#!/bin/bash

# Init
NAVDB=navdata.sqlite3

# Setup new database
#rm -f ${NAVDB}
#sqlite3 ${NAVDB} < create_database.sql

# Import
#time ./import_arinc_424.py -i FAACIFP18 -o ${NAVDB}
#time ./import_navigraph_db.py -i e_dfd_1801.s3db -o ${NAVDB}

# Statistics
#./statistics.py -i ${NAVDB}

# Export
#time ./export_arinc_424.py -i ${NAVDB} -o FAACIFP18.out
time ./export_xplane.py -i ${NAVDB} -o xp
