#!/usr/bin/python3

import sqlite3


# converts latitude/longitude from Degrees/Minutes/Seconds (DMS) to Degrees (DD)
# Examples: N27012432 (27°01'24.32N) to 27.023422222
# Examples: W081444082 (81° 44' 40.82W) to -81.744672222
def convert_dms_to_dd(dms):
    nesw = dms[0:1]
    if nesw == 'N':
        degrees = float(dms[1:3])
        minutes = float(dms[3:5])
        seconds = float(dms[5:7])
        rest = float(dms[7:])
        return degrees + (minutes + ((seconds + rest / 100.0) / 60.0)) / 60.0
    elif nesw == 'E':
        degrees = float(dms[1:4])
        minutes = float(dms[4:6])
        seconds = float(dms[6:8])
        rest = float(dms[8:])
        return degrees + (minutes + ((seconds + rest / 100.0) / 60.0)) / 60.0
    elif nesw == 'S':
        degrees = float(dms[1:3])
        minutes = float(dms[3:5])
        seconds = float(dms[5:7])
        rest = float(dms[7:])
        return -(degrees + (minutes + ((seconds + rest / 100.0) / 60.0)) / 60.0)
    elif nesw == 'W':
        degrees = float(dms[1:4])
        minutes = float(dms[4:6])
        seconds = float(dms[6:8])
        rest = float(dms[8:])
        return -(degrees + (minutes + ((seconds + rest / 100.0) / 60.0)) / 60.0)
    else:
        print('Unknown DMS {}'.format(dms))


# converts 'WFD' into 4474455 (=0x00444657)
def convert_waypoint_type(waypoint_type):
    hexstr = '0x00{2:02x}{1:02x}{0:02x}'.format(ord(waypoint_type[0:1]), ord(waypoint_type[1:2]), ord(waypoint_type[2:3]))
    #print('{0:s} -> {1:d}'.format(waypoint_type, int(hexstr, 16)))
    return int(hexstr, 16)


# converts 'D ' to 3
def convert_fix_type(section_code, subsection_code):
    # 1 ARP (Airport Reference Points)
    # 2 NDB (Non-Directional Beacon)
    # 3 VOR (including VOR-DME and VORTACs)
    # 4 Localizer component of an ILS (Instument Landing System)
    # 5 Localizer component of a localizer-only approach
    # 6 Glideslope component of an ILS
    # 7 Outer markers (OM) for an ILS
    # 8 Middle markers (MM) for an ILS
    # 9 Inner markers (IM) for an ILS
    # 10 Airport Runway Threshold
    # 11 Waypoint / Fix
    # 12 DME without freq, including the DME component of an ILS, VORTAC or VOR-DME (suppress frequency in charts. Use for paired DMEs)
    # 13 DME with freq, Stand-alone DME, or the DME component of an NDB-DME (display frequency in charts. Used for NDBs/DMEs and unpaired DMEs)
    # 14 Final approach path alignment point of an SBAS or GBAS approach path
    # 15 GBAS differential ground station of a GLS
    # 16 Landing threshold point (LTP) or fictitous threshold point of an SBAS/GBAS approach
    # also found these:
    # 16 Seaport
    # 17 Heliport on the ground
    # 20 heliport on a building top
    # 21 nav type for radios
    # 22 FPAP
    # 28 lat/lon into FMS
    if section_code == 'A':  # MORA
        if subsection_code == 'S':  # Grid MORA
            pass
    elif section_code == 'D':  # Navaid
        if subsection_code == ' ':  # VHF Navaid
            return 3
        elif subsection_code == 'B':  # NDB Navaid
            return 2
    elif section_code == 'E':  # Enroute
        if subsection_code == 'A':  # Waypoints
            return 11
        elif subsection_code == 'M':  # Airway Markers
            pass
        elif subsection_code == 'P':  # Holding Patterns
            pass
        elif subsection_code == 'R':  # Airways and Routes
            pass
        elif subsection_code == 'T':  # Preferred Routes
            pass
        elif subsection_code == 'U':  # Airway Restrictions
            pass
        elif subsection_code == 'V':  # Communications
            pass
    elif section_code == 'H':  # Heliport
        if subsection_code == 'A':  # Pads
            pass
        elif subsection_code == 'C':  # Terminal Waypoints
            return 11
        elif subsection_code == 'D':  # SIDs
            pass
        elif subsection_code == 'E':  # STARs
            pass
        elif subsection_code == 'F':  # Approach Procedures
            pass
        elif subsection_code == 'K':  # TAA
            pass
        elif subsection_code == 'S':  # MSA
            pass
        elif subsection_code == 'V':  # Communications
            pass
    elif section_code == 'P':  # Airport
        if subsection_code == 'A':  # Reference Points
            return 1
        elif subsection_code == 'B':  # Gates
            pass
        elif subsection_code == 'C':  # Terminal Waypoints
            return 11
        elif subsection_code == 'D':  # SIDs
            pass
        elif subsection_code == 'E':  # STARs
            pass
        elif subsection_code == 'F':  # Approach Procedures
            pass
        elif subsection_code == 'G':  # Runways
            return 10
        elif subsection_code == 'I':  # Localizer/Glide Slope
            pass
        elif subsection_code == 'K':  # TAA
            pass
        elif subsection_code == 'L':  # MLS
            pass
        elif subsection_code == 'M':  # Localizer Marker
            pass
        elif subsection_code == 'N':  # Terminal NDB
            return 2
        elif subsection_code == 'P':  # Path Point
            pass
        elif subsection_code == 'R':  # Flt Planning ARR/DEP
            pass
        elif subsection_code == 'S':  # MSA
            pass
        elif subsection_code == 'T':  # GLS Station
            pass
        elif subsection_code == 'V':  # Communications
            pass
    elif section_code == 'R':  # Company Routes
        if subsection_code == ' ':  # Company Routes
            pass
        elif subsection_code == 'A':  # Alternate Records
            pass
    elif section_code == 'T':  # Tables
        if subsection_code == 'C':  # Cruising Tables
            pass
        elif subsection_code == 'G':  # Geographical Reference
            pass
    elif section_code == 'U':  # Airspace
        if subsection_code == 'C':  # Controlled Airspace
            pass
        elif subsection_code == 'F':  # FIR/UIR
            pass
        elif subsection_code == 'R':  # Restrictive Airspace
            pass
    print('Unknown section code {} or subsection code {}'.format(section_code, subsection_code))


# converts 'F'/'B'/' ' to 'F'/'B'/'N'
def convert_directional_restriction(dr):
    if dr == 'F':
        return 'F'
    elif dr == 'B':
        return 'B'
    elif dr == ' ':
        return 'N'
    else:
        print('Unknown directional restriction {}'.format(dr))


# converts 'E0120' to '12.000'
def convert_station_declination(station_declination):
    ew = station_declination[0:1]
    if ew == 'E':
        return float(station_declination[1:]) / 10.0
    elif ew == 'W':
        return -float(station_declination[1:]) / 10.0
    else:
        print('Unknown station declination {}'.format(station_declination))


# Sorter for AWY output
def awy_sorter(elem):
    elems = elem.split(' ')
    return(elems[0].strip(), elems[1], elems[2], elems[3], elems[4], elems[5], elems[6], elems[7], elems[8], elems[9], elems[10])


# Export XP-AWY1101-Spec
def export_awy(sql_cursor, output_file='awy.dat'):
    # get routes
    sql_command = \
        'SELECT route_identifier ' \
        'FROM enroute_airways_and_routes ' \
        'GROUP BY route_identifier'
    sql_cursor.execute(sql_command)
    route_identifiers = sql_cursor.fetchall()

    route_legs = {}
    for route_identifier in route_identifiers:
        # get fixes
        sql_command = \
            'SELECT fix_identifier, fix_icao_code, fix_section_code, fix_subsection_code, level, direction_restriction, minimum_altitude_1, minimum_altitude_2, maximum_altitude ' \
            'FROM enroute_airways_and_routes ' \
            'WHERE route_identifier = \'{0:s}\' ' \
            'ORDER BY sequence_number'.format(route_identifier[0])
        sql_cursor.execute(sql_command)
        first_fix = ''
        prev_fix = ''
        for (fix_identifier, fix_icao_code, fix_section_code, fix_subsection_code, level, direction_restriction, minimum_altitude_1, minimum_altitude_2, maximum_altitude) in sql_cursor.fetchall():
            if not fix_section_code:
                fix_section_code = ' ' # TODO D_, DB, EA
            if not fix_subsection_code:
                fix_subsection_code = ' ' # TODO D_, DB, EA
            if not level: # TODO
                level = ' ' # TODO L, H, B
            if not direction_restriction:
                direction_restriction = ' ' # TODO B, F
            if not minimum_altitude_1:
                minimum_altitude_1 = '     ' # TODO
            if not minimum_altitude_2:
                minimum_altitude_2 = '     ' # TODO
            if not maximum_altitude:
                maximum_altitude = '     ' # TODO
            curr_fix = '{0:s}-{1:s}-{2:s}-{3:s}-{4:s}-{5:s}-{6:s}-{7:s}-{8:s}'.format(fix_identifier, fix_icao_code, fix_section_code, fix_subsection_code, level, direction_restriction, minimum_altitude_1, minimum_altitude_2, maximum_altitude)
            if first_fix == '':
                first_fix = curr_fix
            else:
                # add leg from prev fix to curr fix
                route_leg = '{0:s}>{1:s}'.format(prev_fix, curr_fix)
                if route_leg in route_legs:
                    route_legs[route_leg] = '{0:s}-{1:s}'.format(route_legs[route_leg], route_identifier[0].strip())
                else:
                    route_legs[route_leg] = '{0:s}'.format(route_identifier[0].strip())
            prev_fix = curr_fix
        # add leg from last fix to first fix
        route_leg = '{0:s}>{1:s}'.format(prev_fix, first_fix)
        if route_leg in route_legs:
            route_legs[route_leg] = '{0:s}-{1:s}'.format(route_legs[route_leg], route_identifier[0].strip())
        else:
            route_legs[route_leg] = '{0:s}'.format(route_identifier[0].strip())

    outlines = []
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1101 Version - data cycle xxyy, build yyyymmdd, metadata AWYXP1101.\n')
        outfile.write('\n')
        for route_leg in route_legs:
            fix_identifiers = route_leg.split('>')
            (from_fix_identifier, from_fix_icao_code, from_fix_section_code, from_fix_subsection_code, from_level, from_direction_restriction, from_minimum_altitude_1, from_minimum_altitude_2, from_maximum_altitude) = fix_identifiers[0].split('-')
            (to_fix_identifier, to_fix_icao_code, to_fix_section_code, to_fix_subsection_code, to_level, to_direction_restriction, to_minimum_altitude_1, to_minimum_altitude_2, to_maximum_altitude) = fix_identifiers[1].split('-')
            leg_route_identifiers = route_legs[route_leg]
            from_fix_type = convert_fix_type(from_fix_section_code, from_fix_subsection_code)
            to_fix_type = convert_fix_type(to_fix_section_code, to_fix_subsection_code)
            # TODO take from or to?
            if from_level != to_level:
                print(fix_identifiers, "Level different")
            if from_direction_restriction != to_direction_restriction:
                print(fix_identifiers, "Direction restriction different")
            direction_restriction = convert_directional_restriction(to_direction_restriction)
            if from_minimum_altitude_1 != to_minimum_altitude_1:
                print(fix_identifiers, "Minimum altitude 1 different")
            if from_minimum_altitude_2 != to_minimum_altitude_2:
                print(fix_identifiers, "Minimum altitude 2 different")
            if from_maximum_altitude != to_maximum_altitude:
                print(fix_identifiers, "Maximum altitude different")
            minimum_altitude_1 = 0
            if to_minimum_altitude_1 == '     ':
                minimum_altitude_1 = 0  # TODO check
            elif to_minimum_altitude_1 == 'UNKNN':
                minimum_altitude_1 = 0  # TODO check
            else:
                minimum_altitude_1 = int(int(to_minimum_altitude_1) / 100)
            minimum_altitude_2 = 0
            if to_minimum_altitude_2 == '     ':
                minimum_altitude_2 = 0  # TODO check
            elif to_minimum_altitude_2 == 'UNKNN':
                minimum_altitude_2 = 0  # TODO check
            else:
                minimum_altitude_2 = int(int(to_minimum_altitude_2) / 100)
            maximum_altitude = 0
            if to_maximum_altitude == '     ':
                maximum_altitude = 179  # TODO check
            elif to_maximum_altitude == 'UNKNN':
                maximum_altitude = 0  # TODO check
            else:
                maximum_altitude = int(int(to_maximum_altitude) / 100)
            if from_level == ' ':
                outlines.append('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
                    from_fix_identifier.strip(),
                    from_fix_icao_code,
                    from_fix_type,
                    to_fix_identifier.strip(),
                    to_fix_icao_code,
                    to_fix_type,
                    direction_restriction,
                    1,  # Low
                    minimum_altitude_1,
                    maximum_altitude,
                    leg_route_identifiers))
                outlines.append('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
                    from_fix_identifier.strip(),
                    from_fix_icao_code,
                    from_fix_type,
                    to_fix_identifier.strip(),
                    to_fix_icao_code,
                    to_fix_type,
                    direction_restriction,
                    2,  # High
                    minimum_altitude_1,
                    maximum_altitude,
                    leg_route_identifiers))
            elif to_level == 'L':
                outlines.append('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
                    from_fix_identifier.strip(),
                    from_fix_icao_code,
                    from_fix_type,
                    to_fix_identifier.strip(),
                    to_fix_icao_code,
                    to_fix_type,
                    direction_restriction,
                    1,  # Low
                    minimum_altitude_1,
                    maximum_altitude,
                    leg_route_identifiers))
            elif to_level == 'H':
                outlines.append('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
                    from_fix_identifier.strip(),
                    from_fix_icao_code,
                    from_fix_type,
                    to_fix_identifier.strip(),
                    to_fix_icao_code,
                    to_fix_type,
                    direction_restriction,
                    2,  # High
                    minimum_altitude_1,
                    maximum_altitude,
                    leg_route_identifiers))
            elif to_level == 'B':
#                print('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
#                    from_fix_identifier.strip() or '',
#                    from_fix_icao_code or '',
#                    from_fix_type or 0,
#                    to_fix_identifier.strip() or '',
#                    to_fix_icao_code or '',
#                    to_fix_type or 0,
#                    direction_restriction or '',
#                    level or 0,
#                    minimum_altitude_1 or 0,
#                    maximum_altitude or 0,
#                    leg_route_identifiers or ''))
                if not from_fix_identifier:
                    print('no from_fix_identifier')
                if not from_fix_icao_code:
                    print('no from_fix_icao_coder')
                if not from_fix_type:
                    print('no from_fix_type')
                if not to_fix_identifier:
                    print('no to_fix_identifier')
                if not to_fix_icao_code:
                    print('no to_fix_icao_coder')
                if not to_fix_type:
                    print('no to_fix_type')
                if not direction_restriction:
                    print('no direction_restriction')
                if not minimum_altitude_1:
                    print('no minimum_altitude_1')
                if not maximum_altitude:
                    print('no maximum_altitude')
                if not leg_route_identifiers:
                    print('no leg_route_identifiers')
                outlines.append('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
                    from_fix_identifier.strip(),
                    from_fix_icao_code,
                    from_fix_type,
                    to_fix_identifier.strip(),
                    to_fix_icao_code,
                    to_fix_type,
                    direction_restriction,
                    1,  # Low
                    minimum_altitude_1,
                    maximum_altitude,
                    leg_route_identifiers))
                outlines.append('{0:>5s} {1:2s} {2:2d} {3:>5s} {4:2s} {5:2d} {6:1s} {7:1d} {8:3d} {9:3d} {10:s}\n'.format(
                    from_fix_identifier.strip(),
                    from_fix_icao_code,
                    from_fix_type,
                    to_fix_identifier.strip(),
                    to_fix_icao_code,
                    to_fix_type,
                    direction_restriction,
                    2,  # High
                    minimum_altitude_1,
                    maximum_altitude,
                    leg_route_identifiers))
            # TODO use from or to for directional_restriction, level, altitudes?
            # TODO use minimum_altitude_1 or _2?
        for outline in sorted(outlines, key=awy_sorter):
            outfile.write(outline)
        outfile.write('99\n')


# Export XP-CIFP1101-Spec
def export_cifp(sql_cursor, output_file='cifp.dat'):
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1101 Version - data cycle yycc, build yyyymmdd, metadata CIFPXP1101.\n')
        outfile.write('\n')
        # TODO
        outfile.write('99\n')


# Export XP-FIX1101-Spec
def export_fix(sql_cursor, output_file='fix.dat'):
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1101 Version - data cycle yycc, build yyyymmdd, metadata FIXXP1101.\n')
        outfile.write('\n')

        sql_command = \
            'SELECT waypoint_latitude, waypoint_longitude, waypoint_identifier, region_code, waypoint_icao_code, waypoint_type ' \
            'FROM enroute_waypoints ' \
            'ORDER by waypoint_identifier'
        sql_cursor.execute(sql_command)
        for (waypoint_latitude, waypoint_longitude, waypoint_identifier, region_code, waypoint_icao_code, waypoint_type) in sql_cursor.fetchall():
            outfile.write('{0:13.9f} {1:14.9f} {2:>6s} {3:4s} {4:2s} {5:7d}\n'.format(
                convert_dms_to_dd(waypoint_latitude),
                convert_dms_to_dd(waypoint_longitude),
                waypoint_identifier,
                region_code,
                waypoint_icao_code,
                convert_waypoint_type(waypoint_type)))

        sql_command = \
            'SELECT waypoint_latitude, waypoint_longitude, waypoint_identifier, region_code, waypoint_icao_code, waypoint_type ' \
            'FROM airport_terminal_waypoints ' \
            'ORDER by region_code, waypoint_identifier'
        sql_cursor.execute(sql_command)
        for (waypoint_latitude, waypoint_longitude, waypoint_identifier, region_code, waypoint_icao_code, waypoint_type) in sql_cursor.fetchall():
            outfile.write('{0:13.9f} {1:14.9f} {2:>6s} {3:4s} {4:2s} {5:7d}\n'.format(
                convert_dms_to_dd(waypoint_latitude),
                convert_dms_to_dd(waypoint_longitude),
                waypoint_identifier,
                region_code,
                waypoint_icao_code,
                convert_waypoint_type(waypoint_type)))

        outfile.write('99\n')


# Export XP-HOLD1140-Spec
def export_hold(sql_cursor, output_file='hold.dat'):
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1140 Version - data cycle yycc, build yyyymmdd, metadata HoldXP1140.\n')
        outfile.write('\n')

        sql_command = \
            'SELECT region_code, region_icao_code, fix_identifier, fix_icao_code, fix_section_code, fix_subsection_code, inbound_holding_course, turn_direction, leg_length, leg_time, minimum_altitude, maximum_altitude, holding_speed ' \
            'FROM enroute_holding_patterns ' \
            'ORDER by region_code'
        sql_cursor.execute(sql_command)
        for (region_code, region_icao_code, fix_identifier, fix_icao_code, fix_section_code, fix_subsection_code, inbound_holding_course, turn_direction, leg_length, leg_time, minimum_altitude, maximum_altitude, holding_speed) in sql_cursor.fetchall():
            if leg_length.strip() == '':
                leg_length = '0'
            if leg_time.strip() == '':
                leg_time = '0'
            if minimum_altitude.strip() == '':
                minimum_altitude = '0'
            if maximum_altitude.strip() == '':
                maximum_altitude = '0'
            if holding_speed.strip() == '':
                holding_speed = '0'
            outfile.write('{0: >5s} {1:2s} {2:4s} {3:2d} {4:8.1f} {5:8.1f} {6:8.1f} {7:1s} {8:8d} {9:8d} {10:8d}\n'.format(
                fix_identifier.strip(),
                fix_icao_code.strip(),
                region_code.strip(),
                convert_fix_type(fix_section_code, fix_subsection_code),
                float(inbound_holding_course) / 10.0,
                float(leg_time) / 10.0,
                float(leg_length) / 10.0,
                turn_direction,
                int(minimum_altitude),
                int(maximum_altitude),
                int(holding_speed)))

        outfile.write('99\n')


# Export XP-MORA1150-Spec
def export_mora(sql_cursor, output_file='mora.dat'):
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1150 Version - data cycle yycc, build yyyymmdd, metadata MORAXP1150.\n')
        outfile.write('\n')

        latitudes_longitudes = []
        for starting_latitude in list(range(0, 90, 1)) + list(range(-1, -90, -1)):  # +00..+89, -01..-89
            for starting_longitude in list(range(0, 151, 30)) + list(range(-30, -181, -30)):  # +000..+150, -030..-180
                latitudes_longitudes.append([starting_latitude, starting_longitude])
        latitudes_longitudes.append([-90, 0])  # -90, +000

        for (starting_latitude, starting_longitude) in latitudes_longitudes:
            sql_command = \
                'SELECT mora_01, mora_02, mora_03, mora_04, mora_05, mora_06, mora_07, mora_08, mora_09, mora_10, mora_11, mora_12, mora_13, mora_14, mora_15, mora_16, mora_17, mora_18, mora_19, mora_20, mora_21, mora_22, mora_23, mora_24, mora_25, mora_26, mora_27, mora_28, mora_29, mora_30 ' \
                'FROM mora_grid_mora ' \
                'WHERE starting_latitude=\'{0:3s}\' AND starting_longitude=\'{1:4s}\''.format(
                    '{0:+03d}'.format(starting_latitude).replace('+', 'N').replace('-', 'S'),
                    '{0:+04d}'.format(starting_longitude).replace('+', 'E').replace('-', 'W'))
            sql_cursor.execute(sql_command)
            moras = sql_cursor.fetchall()
            if len(moras) == 0:
                outfile.write('{0:+03d} {1:+04d} {2:3s} {3:3s} {4:3s} {5:3s} {6:3s} {7:3s} {8:3s} {9:3s} {10:3s} {11:3s} {12:3s} {13:3s} {14:3s} {15:3s} {16:3s} {17:3s} {18:3s} {19:3s} {20:3s} {21:3s} {22:3s} {23:3s} {24:3s} {25:3s} {26:3s} {27:3s} {28:3s} {29:3s} {30:3s} {31:3s}\n'.format(
                    starting_latitude,
                    starting_longitude,
                    '010', '010', '010', '010', '010', '010', '010', '010', '010', '010',
                    '010', '010', '010', '010', '010', '010', '010', '010', '010', '010',
                    '010', '010', '010', '010', '010', '010', '010', '010', '010', '010'))
            elif len(moras) == 1:
                mora = moras[0]
                outfile.write('{0:+03d} {1:+04d} {2:3s} {3:3s} {4:3s} {5:3s} {6:3s} {7:3s} {8:3s} {9:3s} {10:3s} {11:3s} {12:3s} {13:3s} {14:3s} {15:3s} {16:3s} {17:3s} {18:3s} {19:3s} {20:3s} {21:3s} {22:3s} {23:3s} {24:3s} {25:3s} {26:3s} {27:3s} {28:3s} {29:3s} {30:3s} {31:3s}\n'.format(
                    starting_latitude,
                    starting_longitude,
                    mora[0].replace('UNK', '010'),
                    mora[1].replace('UNK', '010'),
                    mora[2].replace('UNK', '010'),
                    mora[3].replace('UNK', '010'),
                    mora[4].replace('UNK', '010'),
                    mora[5].replace('UNK', '010'),
                    mora[6].replace('UNK', '010'),
                    mora[7].replace('UNK', '010'),
                    mora[8].replace('UNK', '010'),
                    mora[9].replace('UNK', '010'),
                    mora[10].replace('UNK', '010'),
                    mora[11].replace('UNK', '010'),
                    mora[12].replace('UNK', '010'),
                    mora[13].replace('UNK', '010'),
                    mora[14].replace('UNK', '010'),
                    mora[15].replace('UNK', '010'),
                    mora[16].replace('UNK', '010'),
                    mora[17].replace('UNK', '010'),
                    mora[18].replace('UNK', '010'),
                    mora[19].replace('UNK', '010'),
                    mora[20].replace('UNK', '010'),
                    mora[21].replace('UNK', '010'),
                    mora[22].replace('UNK', '010'),
                    mora[23].replace('UNK', '010'),
                    mora[24].replace('UNK', '010'),
                    mora[25].replace('UNK', '010'),
                    mora[26].replace('UNK', '010'),
                    mora[27].replace('UNK', '010'),
                    mora[28].replace('UNK', '010'),
                    mora[29].replace('UNK', '010')))
            else:
                print('More than one MORA returned')
        outfile.write('99\n')


# Export XP-MSA1150-Spec
def export_msa(sql_cursor, output_file='msa.dat'):
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1150 Version - data cycle yycc, build yyyymmdd, metadata MSAXP1150.\n')
        outfile.write('\n')
        sql_command = \
            'SELECT airport_identifier as identifier, airport_icao_code as icao_code, msa_center_identifier, msa_center_icao_code, msa_center_section_code, msa_center_subsection_code, sector_bearing_1, sector_altitude_1, sector_radius_1, sector_bearing_2, sector_altitude_2, sector_radius_2, sector_bearing_3, sector_altitude_3, sector_radius_3, sector_bearing_4, sector_altitude_4, sector_radius_4, sector_bearing_5, sector_altitude_5, sector_radius_5, sector_bearing_6, sector_altitude_6, sector_radius_6, sector_bearing_7, sector_altitude_7, sector_radius_7, magnetic_true_indicator ' \
            'FROM airport_msa ' \
            'UNION ' \
            'SELECT heliport_identifier as identifier, heliport_icao_code as icao_code, msa_center_identifier, msa_center_icao_code, msa_center_section_code, msa_center_subsection_code, sector_bearing_1, sector_altitude_1, sector_radius_1, sector_bearing_2, sector_altitude_2, sector_radius_2, sector_bearing_3, sector_altitude_3, sector_radius_3, sector_bearing_4, sector_altitude_4, sector_radius_4, sector_bearing_5, sector_altitude_5, sector_radius_5, sector_bearing_6, sector_altitude_6, sector_radius_6, sector_bearing_7, sector_altitude_7, sector_radius_7, magnetic_true_indicator ' \
            'FROM heliport_msa'
        sql_cursor.execute(sql_command)

        for (identifier, icao_code, msa_center_identifier, msa_center_icao_code, msa_center_section_code, msa_center_subsection_code, sector_bearing_1, sector_altitude_1, sector_radius_1, sector_bearing_2, sector_altitude_2, sector_radius_2, sector_bearing_3, sector_altitude_3, sector_radius_3, sector_bearing_4, sector_altitude_4, sector_radius_4, sector_bearing_5, sector_altitude_5, sector_radius_5, sector_bearing_6, sector_altitude_6, sector_radius_6, sector_bearing_7, sector_altitude_7, sector_radius_7, magnetic_true_indicator) in sql_cursor.fetchall():
            outfile.write('{0:2d} {1: >5s} {2:2s} {3: >4s} {4:1s}'.format(
                convert_fix_type(msa_center_section_code, msa_center_subsection_code),  # Type of Fix, Navaid, Airport or Runway Threshold
                msa_center_identifier.strip(),  # Identifier
                msa_center_icao_code,  # ICAO code
                identifier.strip(),  # Terminal region
                magnetic_true_indicator))  # Sector Bearing Mag/True Indicator
            if (sector_bearing_1 != '      ') and (sector_altitude_1 != '   ') or (sector_radius_1 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_1[0:3],      # Sector Bearing
                    sector_altitude_1,          # Sector Altitude
                    sector_radius_1))           # Radius Limit
            if (sector_bearing_2 != '      ') and (sector_altitude_2 != '   ') or (sector_radius_2 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_2[0:3],      # Sector Bearing
                    sector_altitude_2,          # Sector Altitude
                    sector_radius_2))           # Radius Limit
            if (sector_bearing_3 != '      ') and (sector_altitude_3 != '   ') or (sector_radius_3 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_3[0:3],      # Sector Bearing
                    sector_altitude_3,          # Sector Altitude
                    sector_radius_3))           # Radius Limit
            if (sector_bearing_4 != '      ') and (sector_altitude_4 != '   ') or (sector_radius_4 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_4[0:3],      # Sector Bearing
                    sector_altitude_4,          # Sector Altitude
                    sector_radius_4))           # Radius Limit
            if (sector_bearing_5 != '      ') and (sector_altitude_5 != '   ') or (sector_radius_5 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_5[0:3],      # Sector Bearing
                    sector_altitude_5,          # Sector Altitude
                    sector_radius_5))           # Radius Limit
            if (sector_bearing_6 != '      ') and (sector_altitude_6 != '   ') or (sector_radius_6 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_6[0:3],      # Sector Bearing
                    sector_altitude_6,          # Sector Altitude
                    sector_radius_6))           # Radius Limit
            if (sector_bearing_7 != '      ') and (sector_altitude_7 != '   ') or (sector_radius_7 != '  '):
                outfile.write(' {0:3s} {1:3s} {2:2s}'.format(
                    sector_bearing_7[0:3],      # Sector Bearing
                    sector_altitude_7,          # Sector Altitude
                    sector_radius_7))           # Radius Limit
            outfile.write(' 000 000  0\n')  # Terminator
        outfile.write('99\n')


# Export XP-NAV1150-Spec
def export_nav(sql_cursor, output_file='nav.dat'):
    with open(output_file, 'w') as outfile:
        outfile.write('I\n')
        outfile.write('1150 Version - data cycle yycc, build 20200623, metadata NavXP1150.\n')
        outfile.write('\n')

        # 2 NDBs
        sql_command = \
            'SELECT airport_identifier, airport_icao_code, ndb_identifier, ndb_icao_code, ndb_frequency, ndb_class, ndb_latitude, ndb_longitude, magnetic_variation, ndb_name ' \
            'FROM navaid_ndb_navaids'
        sql_cursor.execute(sql_command)
        for (airport_identifier, airport_icao_code, ndb_identifier, ndb_icao_code, ndb_frequency, ndb_class, ndb_latitude, ndb_longitude, magnetic_variation, ndb_name) in sql_cursor.fetchall():
            range_power = 0
            if ndb_class[2:3] == 'H':
                range_power = 75  # 2000 Watts or More
            elif ndb_class[2:3] == ' ':
                range_power = 50  # 50 to 1999 Watts
            elif ndb_class[2:3] == 'M':
                range_power = 25  # 25 or Less Than 50
            elif ndb_class[2:3] == 'L':
                range_power = 15  # Less Than 25
            else:
                print('Unknown NDB range class {}'.format(ndb_class[2:3]))
            if airport_identifier == '    ':
                airport_identifier = 'ENRT'
            outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                2,  # NDB
                convert_dms_to_dd(ndb_latitude),  # latitude
                convert_dms_to_dd(ndb_longitude),  # longitude
                0,  # elevation
                ndb_frequency[1:4],  # frequency
                range_power,  # class
                0.0,  # variation TODO use magnetic_variation?
                ndb_identifier.strip(),  # identifier
                airport_identifier.strip(),  # region identifier
                ndb_icao_code,  # ICAO code
                ndb_name.strip() + ' NDB'))  # name

        # 3 VORs
        # 12 paired DMEs
        # 13 unpaired DMEs
        sql_command = \
            'SELECT airport_identifier, airport_icao_code, vor_identifier, vor_icao_code, vor_frequency, navaid_class, vor_latitude, vor_longitude, dme_identifier, dme_latitude, dme_longitude, station_declination, dme_elevation, figure_of_merit, vor_name ' \
            'FROM navaid_vhf_navaids'
        sql_cursor.execute(sql_command)
        for (airport_identifier, airport_icao_code, vor_identifier, vor_icao_code, vor_frequency, navaid_class, vor_latitude, vor_longitude, dme_identifier, dme_latitude, dme_longitude, station_declination, dme_elevation, figure_of_merit, vor_name) in sql_cursor.fetchall():
            range_power = 0
            if navaid_class[2:3] == 'T':
                range_power = 25
            elif navaid_class[2:3] == 'L':
                range_power = 40
            elif navaid_class[2:3] == 'H':
                range_power = 130
            elif navaid_class[2:3] == 'U':
                range_power = 125
            elif navaid_class[2:3] == 'C':
                range_power = 125  # TODO
            else:
                print('Unknown VOR range class {}'.format(navaid_class[2:3]))
            if figure_of_merit == '0':
                range_power = 25  # <=25NM
            elif figure_of_merit == '1':
                range_power = 40  # <=40NM
            elif figure_of_merit == '2':
                range_power = 130  # <=130NM
            elif figure_of_merit == '3':
                range_power = 130  # >150NM
            elif figure_of_merit == '7':  # not included in a civil international NOTAM system
                pass  # TODO
            elif figure_of_merit == '9':  # Out of Service
                pass  # TODO
            else:
                print('Unknown figure of merit {}'.format(figure_of_merit))
            if airport_identifier == '    ':
                airport_identifier = 'ENRT'
            if dme_elevation == '     ':
                dme_elevation = '00000'
            row_code = 0
            latitude = '         '
            longitude = '          '
            suffix = ''
            if navaid_class[0:2] == 'V ':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    3,  # VOR incl. VOR-DME and VORTAC
                    convert_dms_to_dd(vor_latitude),  # latitude
                    convert_dms_to_dd(vor_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    vor_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VOR'))  # name
            elif navaid_class[0:2] == 'VT':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    3,  # VOR incl. VOR-DME and VORTAC
                    convert_dms_to_dd(vor_latitude),  # latitude
                    convert_dms_to_dd(vor_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    vor_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VOR'))  # name
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    12,  # paired DME
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    0.0,  # DME bias TODO
                    dme_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VORTAC DME'))  # name
            elif navaid_class[0:2] == ' T':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    3,  # VOR incl. VOR-DME and VORTAC
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    vor_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' TACAN'))  # name
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    12,  # paired DME
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    0.0,  # DME bias TODO
                    dme_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' TACAN DME'))  # name
            elif navaid_class[0:2] == 'VD':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    3,  # VOR incl. VOR-DME and VORTAC
                    convert_dms_to_dd(vor_latitude),  # latitude
                    convert_dms_to_dd(vor_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    vor_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VOR/DME'))  # name
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    12,  # paired DME
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    0.0,  # DME bias TODO
                    dme_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VOR/DME'))  # name
            elif navaid_class[0:2] == ' D':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    3,  # VOR incl. VOR-DME and VORTAC
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    vor_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VOR/DME'))  # name
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    12,  # paired DME
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    0.0,  # DME bias TODO
                    dme_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' VOR/DME'))  # name
            elif navaid_class[0:2] == ' M':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    3,  # VOR incl. VOR-DME and VORTAC
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    vor_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' TACAN'))  # name
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    12,  # paired DME
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    0.0,  # DME bias TODO
                    dme_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' TACAN DME'))  # name
            elif navaid_class[0:2] == ' I':
                outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5d} {6:10.3f} {7: >4s} {8: >4s} {9:2s} {10:s}\n'.format(
                    12,  # paired DME
                    convert_dms_to_dd(dme_latitude),  # latitude
                    convert_dms_to_dd(dme_longitude),  # longitude
                    int(dme_elevation),  # elevation
                    vor_frequency,  # frequency
                    range_power,  # class
                    convert_station_declination(station_declination),  # variation
                    dme_identifier.strip(),  # identifier
                    airport_identifier.strip(),  # region identifier
                    vor_icao_code,  # ICAO code
                    vor_name.strip() + ' DME-ILS'))  # name
            else:
                print('Unknown navaid class {}'.format(navaid_class[0:2]))

        # 4 ILS LOC
        # 5 stand-alone LOC
        sql_command = ''

        # 6 Glideslope
        # comes from airport_localizer_glide_slope?

        # 7 Outer Markers
        # 8 Middle Makrers
        # 9 Inner Markers

        # 12 paired DMEs
        # 13 unpaired DMEs

        # 14 Final Approach Course Alignment Point (FPAP) of SBAS or GBAS
        # 16 Landing Threshold Point (LTP) or Fictitious Threshold Point (FTP) of SBAS or GBAS
        sql_command = \
            'SELECT airport_reference_points.airport_identifier, airport_reference_points.airport_icao_code, airport_reference_points.airport_reference_point_latitude, airport_reference_points.airport_reference_point_longitude, airport_reference_points.magnetic_variation, airport_runways.runway_identifier, airport_runways.runway_magnetic_bearing, airport_runways.runway_latitude, airport_runways.runway_longitude, airport_runways.landing_threshold_elevation, airport_sbas_path_points.approach_procedure_identifier, airport_sbas_path_points.landing_threshold_point_latitude, airport_sbas_path_points.landing_threshold_point_longitude, airport_sbas_path_points.glide_path_angle, airport_sbas_path_points.flight_path_alignment_point_latitude, airport_sbas_path_points.flight_path_alignment_point_longitude, airport_sbas_path_points.path_point_tch, airport_sbas_path_points.e_approach_type_identifier, airport_sbas_path_points.e_gnss_channel_number ' \
            'FROM airport_reference_points ' \
            'LEFT JOIN airport_runways ' \
            'ON airport_runways.airport_identifier = airport_reference_points.airport_identifier ' \
            'AND airport_runways.airport_icao_code = airport_reference_points.airport_icao_code ' \
            'LEFT JOIN airport_sbas_path_points ' \
            'ON airport_sbas_path_points.airport_identifier = airport_runways.airport_identifier ' \
            'AND airport_sbas_path_points.airport_icao_code = airport_runways.airport_icao_code ' \
            'AND airport_sbas_path_points.runway_identifier_or_final_approach_course = airport_runways.runway_identifier ' \
            'WHERE approach_procedure_identifier IS NOT NULL'
        for (airport_identifier, airport_icao_code, airport_reference_point_latitude, airport_reference_point_longitude, magnetic_variation, runway_identifier, runway_magnetic_heading, runway_latitude, runway_longitude, landing_threshold_elevation, approach_procedure_identifier, landing_threshold_point_latitude, landing_threshold_point_longitude, glide_path_angle, flight_path_alignment_point_latitude, flight_path_alignment_point_longitude, path_point_tch, e_approach_type_identifier, e_gnss_channel_number) in sql_cursor.execute(sql_command):
            outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5.1f} {6:10.3f} {7:5s} {8:4s} {9:2s} {10:3s} {11:3s}\n'.format(
                14,  # FPAP
                convert_dms_to_dd(runway_latitude),  # TODO
                convert_dms_to_dd(runway_longitude),  # TODO
                int(landing_threshold_elevation),
                e_gnss_channel_number,
                0.0,
                float(runway_magnetic_heading) / 10.0 + convert_station_declination(magnetic_variation),
                approach_procedure_identifier.strip(),
                airport_identifier,
                airport_icao_code,
                runway_identifier[2:],
                e_approach_type_identifier.strip()))
            outfile.write('{0:2d} {1:13.9f} {2:14.9f} {3:8d} {4: >8s} {5:5.1f} {6:10.3f} {7:5s} {8:4s} {9:2s} {10:3s} {11:4s}\n'.format(
                16,  # LTP/FTP
                convert_dms_to_dd(runway_latitude),
                convert_dms_to_dd(runway_longitude),
                int(landing_threshold_elevation),
                e_gnss_channel_number,
                float(path_point_tch) / 10.0,
                float(glide_path_angle) * 1000.0 + float(runway_magnetic_heading) / 10.0 + convert_station_declination(magnetic_variation),
                approach_procedure_identifier.strip(),
                airport_identifier,
                airport_icao_code,
                runway_identifier[2:],
                'WAAS'))

        outfile.write('99\n')


def main():
    import argparse
    import os
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Navigation database file', default='navdata.sqlite3')
    parser.add_argument('-o', '--output', help='XPlane file output path', default='.')
    args = parser.parse_args()

    # open database connection
    sql_connection = sqlite3.connect(args.input)
    sql_cursor = sql_connection.cursor()

    # import data
    print("Exporting AWY...")
    export_awy(sql_cursor, os.path.join(args.output, 'awy.dat'))
    print("Exporting CIFP...")
    export_cifp(sql_cursor, os.path.join(args.output, 'cifp.dat'))
    print("Exporting FIX...")
    export_fix(sql_cursor, os.path.join(args.output, 'fix.dat'))
    print("Exporting HOLD...")
    export_hold(sql_cursor, os.path.join(args.output, 'hold.dat'))
    print("Exporting MORA...")
    export_mora(sql_cursor, os.path.join(args.output, 'mora.dat'))
    print("Exporting MSA...")
    export_msa(sql_cursor, os.path.join(args.output, 'msa.dat'))
    print("Exporting NAV...")
    export_nav(sql_cursor, os.path.join(args.output, 'nav.dat'))

    # close database connection
    sql_connection.commit()
    sql_connection.close()


if __name__ == '__main__':
    # execute only if run as a script
    main()
