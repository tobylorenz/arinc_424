#!/usr/bin/python3

import sqlite3


# converts Degrees (DD) to Degrees/Minutes/Seconds (DMS)
# Examples: 27.023422222 to N27012432 (27°01'24.32N)
# Examples: -81.744672222 to W081444082 (81° 44' 40.82W)
def convert_dd_to_dms(dd, pm='NS'):
    if not isinstance(dd, float):
        if pm == 'NS':
            return '         '
        else:
            return '          '

    # sign
    nesw = pm[0]
    if dd < 0:
        dd = -dd
        nesw = pm[1]
    # degrees
    degrees = int(dd / 1)
    degrees_rest = float(dd % 1)
    # minutes
    degrees_rest *= 60.0
    minutes = int(degrees_rest / 1)
    minutes_rest = degrees_rest % 1
    # seconds
    seconds = minutes_rest * 60.0
    #seconds = int(minutes_rest / 1)
    #seconds_rest = minutes_rest % 1
    # rest
    #seconds_rest *= 100.0
    dms = None
    if pm == 'NS':
        dms = '{0: <1s}{1:02d}{2:02d}{3:04.0f}'.format(nesw, degrees, minutes, seconds * 100.0)
    else:
        dms = '{0: <1s}{1:03d}{2:02d}{3:04.0f}'.format(nesw, degrees, minutes, seconds * 100.0)
    if (dms == 'S999592400') or (dms == 'W999592400'):
        print('Extreme conversion from {0:f} to {1:s}'.format(dd, dms))
    return dms


# convert communication frequency according to frequency units
def convert_communication_frequency(communication_frequency, frequency_units):
    if frequency_units == 'H':
        # 17955 kHz -> 1795500
        #  8965 kHz -> 0896500
        return '{0:07.0f}'.format(communication_frequency * 100.0)
    elif frequency_units == 'V':
        # 118.50   MHz -> 0118500
        # 131.275  MHz -> 0131275
        return '{0:07.0f}'.format(communication_frequency * 1000.0)
    elif frequency_units == 'U':
        # 267   MHz -> 0026700
        # 287.5 MHz -> 0028750
        return '{0:07.0f}'.format(communication_frequency * 100.0)
    elif frequency_units == 'C':
        # 132.0583 MHz -> 132.060 MHz -> 0132060
        return '{0:07.0f}'.format(round(communication_frequency, 2) * 1000.0)
    else:
        print('Unknown frequency unit {}'.format(frequency_units))


# convert magnetic variation
def convert_magnetic_variation(magnetic_variation):
    if magnetic_variation < 0:
        return 'W{0:04.0f}'.format(-magnetic_variation * 10.0)
    else:
        return 'E{0:04.0f}'.format(magnetic_variation * 10.0)


# convert range to figure_of_merit
def convert_range(range):
    if range == 0:
        return '7' # TODO 7 or 9
    elif range == 25:
        return '0'
    elif range == 40:
        return '1'
    elif range == 130:
        return '2'
    elif range == 250:
        return '3'
    else:
        print('Unknown range {0:d}'.format(range))


# search navaid by identifier, latitude, longitude, icao_code and return icao_code, section_code, subsection_code
def search_navaid(sql_cursor_out, identifier, latitude, longitude, sections=['D ', 'DB', 'DT', 'EA', 'PA', 'PC', 'PG', 'PI', 'PN', 'PT'], icao_code=None, airport_identifier=None):
    if not (identifier and latitude and longitude):
        return (None, None, None)

    sql_command_where = 'identifier=\'{0:s}\''.format(identifier.strip(' '))
    if latitude.strip(' ') and longitude.strip(' '):
        sql_command_where += ' AND latitude=\'{0: <9s}\' AND longitude=\'{1: <10s}\''.format(latitude, longitude)
    if icao_code:
        sql_command_where += ' AND TRIM(icao_code)=\'{0:s}\''.format(icao_code.strip(' '))

    results = []
    for section in sections:
        sql_command = None
        if section == 'D ':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(vor_identifier) as identifier, vor_icao_code as icao_code, vor_latitude as latitude, vor_longitude as longitude ' \
                'FROM navaid_vhf_navaids'
        elif section == 'DB':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(ndb_identifier) as identifier, ndb_icao_code as icao_code, ndb_latitude as latitude, ndb_longitude as longitude ' \
                'FROM navaid_ndb_navaids'
        elif section == 'DT':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(tacan_identifier) as identifier, tacan_icao_code as icao_code, tacan_latitude as latitude, tacan_longitude as longitude ' \
                'FROM navaid_tacan_only_navaids'
        elif section == 'EA':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(waypoint_identifier) as identifier, waypoint_icao_code as icao_code, waypoint_latitude as latitude, waypoint_longitude as longitude ' \
                'FROM enroute_waypoints'
        elif section == 'PA':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(airport_identifier) as identifier, airport_icao_code as icao_code, airport_reference_point_latitude as latitude, airport_reference_point_longitude as longitude ' \
                'FROM airport_reference_points'
        elif section == 'PC':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(waypoint_identifier) as identifier, waypoint_icao_code as icao_code, waypoint_latitude as latitude, waypoint_longitude as longitude ' \
                'FROM airport_terminal_waypoints'
        elif section == 'PG':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(runway_identifier) as identifier, airport_icao_code as icao_code, runway_latitude as latitude, runway_longitude as longitude ' \
                'FROM airport_runways'
        elif section == 'PI':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(localizer_identifier) as identifier, airport_icao_code as icao_code, localizer_latitude as latitude, localizer_longitude as longitude ' \
                'FROM airport_localizers_and_glideslopes'
        elif section == 'PN':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(ndb_identifier) as identifier, ndb_icao_code as icao_code, ndb_latitude as latitude, ndb_longitude as longitude ' \
                'FROM airport_terminal_ndb_navaids'
        elif section == 'PT':
            sql_command = \
                'SELECT section_code, subsection_code, TRIM(gls_ref_path_identifier) as identifier, airport_or_heliport_icao_code as icao_code, station_latitude as latitude, station_longitude as longitude ' \
                'FROM airport_gls_stations'
            if airport_identifier:
                sql_command_where += ' AND TRIM(airport_or_heliport_identifier)=\'{0: <4s}\''.format(airport_identifier.strip(' '))
        else:
            print('Undefined section {0:s}'.format(section))

        sql_cursor_out.execute(sql_command + ' WHERE ' + sql_command_where)
        single_results = sql_cursor_out.fetchall()
        results += single_results

    if len(results) == 0:
        print('   Searching for {0:s}'.format(sql_command_where))
        print('      No navaid found in ({0:s})'.format('/'.join(sections)))
        return (None, None, None)
    elif len(results) == 1:
        (found_section_code, found_subsection_code, found_identifier, found_icao_code, found_latitude, found_longitude) = results[0]
        return (found_icao_code, found_section_code, found_subsection_code)
    elif len(results) == 2:
        (found_section_code_1, found_subsection_code_1, found_identifier_1, found_icao_code_1, found_latitude_1, found_longitude_1) = results[0]
        (found_section_code_2, found_subsection_code_2, found_identifier_2, found_icao_code_2, found_latitude_2, found_longitude_2) = results[1]
        if (found_section_code_1 == 'D') and (found_subsection_code_1 == ' ') and (found_section_code_2 == 'D') and (found_subsection_code_2 == 'B'):
            # Prefer (D ) over (DB)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'D') and (found_subsection_code_1 == ' ') and (found_section_code_2 == 'E') and (found_subsection_code_2 == 'A'):
            # Prefer (D ) over (EA)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'D') and (found_subsection_code_1 == ' ') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'C'):
            # Prefer (D ) over (PC)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'D') and (found_subsection_code_1 == ' ') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'I'):
            # Prefer (D ) over (PI)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'D') and (found_subsection_code_1 == ' ') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'N'):
            # Prefer (D ) over (PN)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'D') and (found_subsection_code_1 == 'B') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'C'):
            # Prefer (DB) over (PC)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'D') and (found_subsection_code_1 == 'B') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'N'):
            # Prefer (DB) over (PN)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'E') and (found_subsection_code_1 == 'A') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'A'):
            # Prefer (EA) over (PA)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'P') and (found_subsection_code_1 == 'A') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'C'):
            # Prefer (PA) over (PC)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'P') and (found_subsection_code_1 == 'C') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'G'):
            # Prefer (PC) over (PG)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        elif (found_section_code_1 == 'P') and (found_subsection_code_1 == 'C') and (found_section_code_2 == 'P') and (found_subsection_code_2 == 'N'):
            # Prefer (PC) over (PN)
            return (found_icao_code_1, found_section_code_1, found_subsection_code_1)
        else:
            print('   No preference defined for {0:1s}{1:1s} and {2:1s}{3:1s} ({4:s})'.format(found_section_code_1, found_subsection_code_1, found_section_code_2, found_subsection_code_2, sql_command_where))
            return (None, None, None)
    else: # >2
        print('   Searching for {0:s}'.format(sql_command_where))
        print('      More than one navaid found:')
        for (found_section_code, found_subsection_code, found_identifier, found_icao_code, found_latitude, found_longitude) in results:
            print('     section_code=\'{0:s}\' and subsection_code=\'{1:s}\''.format(found_section_code, found_subsection_code))
        return (None, None, None)


# import tbl_airport_communication into airport_communications
def import_tbl_airport_communication(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airport_identifier, communication_type, communication_frequency, frequency_units, service_indicator, callsign, latitude, longitude ' \
        'FROM tbl_airport_communication'
    for (area_code, icao_code, airport_identifier, communication_type, communication_frequency, frequency_units, service_indicator, callsign, latitude, longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'V'
        # = line[13:15]
        #sql_columns['communication_class'] = line[15:19]
        #sql_columns['sequence_number'] = line[19:21]
        # Primary
        sql_columns['communication_types'] = '{0: <3s}'.format(communication_type)
        sql_columns['transmit_frequency'] = convert_communication_frequency(communication_frequency, frequency_units)
        sql_columns['receive_frequency'] = convert_communication_frequency(communication_frequency, frequency_units)
        sql_columns['frequency_units'] = '{0: <1s}'.format(frequency_units)
        #sql_columns['radar_units'] = line[40:41]
        #sql_columns['h24_indicator'] = line[41:42]
        sql_columns['call_signs'] = '{0: <25s}'.format(callsign or '')
        #sql_columns['multi_sector_indicator'] = line[67:68]
        #sql_columns['sectorization'] = line[68:74]
        #sql_columns['sector_facility_identifier'] = line[74:78]
        #sql_columns['sector_facility_icao_code'] = line[78:80]
        #sql_columns['sector_facility_section_code'] = line[80:81]
        #sql_columns['sector_facility_subsection_code'] = line[81:82]
        #sql_columns['altitude_description_code'] = line[82:83]
        #sql_columns['communication_altitude_1'] = line[83:86]
        #sql_columns['communication_altitude_2'] = line[86:89]
        #sql_columns['distance_description_code'] = line[89:90]
        #sql_columns['communications_distance'] = line[90:92]
        sql_columns['transmitter_latitude'] = convert_dd_to_dms(latitude, 'NS')
        sql_columns['transmitter_longitude'] = convert_dd_to_dms(longitude, 'EW')
        sql_columns['service_indicator'] = '{0: <3s}'.format(service_indicator or '')
        #sql_columns['modulation'] = line[114:115]
        #sql_columns['signal_emission'] = line[115:116]
        # = line[116:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_communications',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_airport_msa into airport_msa
def import_tbl_airport_msa(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airport_identifier, msa_center, msa_center_latitude, msa_center_longitude, magnetic_true_indicator, radius_limit, sector_bearing_1, sector_altitude_1, sector_bearing_2, sector_altitude_2, sector_bearing_3, sector_altitude_3, sector_bearing_4, sector_altitude_4, sector_bearing_5, sector_altitude_5 ' \
        'FROM tbl_airport_msa'
    for (area_code, icao_code, airport_identifier, msa_center, msa_center_latitude, msa_center_longitude, magnetic_true_indicator, radius_limit, sector_bearing_1, sector_altitude_1, sector_bearing_2, sector_altitude_2, sector_bearing_3, sector_altitude_3, sector_bearing_4, sector_altitude_4, sector_bearing_5, sector_altitude_5) in sql_cursor_in.execute(sql_command):
        (found_msa_center_icao_code, found_msa_center_section_code, found_msa_center_subsection_code) = search_navaid(
            sql_cursor_out,
            msa_center,
            convert_dd_to_dms(msa_center_latitude, 'NS'),
            convert_dd_to_dms(msa_center_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PA', 'PC', 'PG', 'PN'])
        if not found_msa_center_section_code:
            print('MSA center {0:s} not found'.format(msa_center))
            continue

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'S'
        sql_columns['msa_center_identifier'] = '{0: <5s}'.format(msa_center)
        sql_columns['msa_center_icao_code'] = '{0: <2s}'.format(found_msa_center_icao_code or '')
        sql_columns['msa_center_section_code'] = '{0: <1s}'.format(found_msa_center_section_code or '')
        sql_columns['msa_center_subsection_code'] = '{0: <1s}'.format(found_msa_center_subsection_code or '')
        #sql_columns['multiple_code'] = line[22:23]
        # = line[23:38]
        # Primary
        # = line[39:42]
        sql_columns['sector_bearing_1'] = '{0:03d}'.format(sector_bearing_1)
        sql_columns['sector_altitude_1'] = '{0:03d}'.format(sector_altitude_1)
        if radius_limit:
            sql_columns['sector_radius_1'] = '{0:02d}'.format(radius_limit)
        else:
            sql_columns['sector_radius_1'] = '  '
        if sector_bearing_2:
            sql_columns['sector_bearing_1'] += '{0:03d}'.format(sector_bearing_2)
            sql_columns['sector_bearing_2'] = '{0:03d}'.format(sector_bearing_2)
            sql_columns['sector_altitude_2'] = '{0:03d}'.format(sector_altitude_2)
            if radius_limit:
                sql_columns['sector_radius_2'] = '{0:02d}'.format(radius_limit)
            else:
                sql_columns['sector_radius_2'] = '  '
            if sector_bearing_3:
                sql_columns['sector_bearing_2'] += '{0:03d}'.format(sector_bearing_3)
                sql_columns['sector_bearing_3'] = '{0:03d}'.format(sector_bearing_3)
                sql_columns['sector_altitude_3'] = '{0:03d}'.format(sector_altitude_3)
                if radius_limit:
                    sql_columns['sector_radius_3'] = '{0:02d}'.format(radius_limit)
                else:
                    sql_columns['sector_radius_3'] = '  '
                if sector_bearing_4:
                    sql_columns['sector_bearing_3'] += '{0:03d}'.format(sector_bearing_4)
                    sql_columns['sector_bearing_4'] = '{0:03d}'.format(sector_bearing_4)
                    sql_columns['sector_altitude_4'] = '{0:03d}'.format(sector_altitude_4)
                    if radius_limit:
                        sql_columns['sector_radius_4'] = '{0:02d}'.format(radius_limit)
                    else:
                        sql_columns['sector_radius_4'] = '  '
                    if sector_bearing_5:
                        sql_columns['sector_bearing_4'] += '{0:03d}'.format(sector_bearing_5)
                        sql_columns['sector_bearing_5'] = '{0:03d}'.format(sector_bearing_5)
                        sql_columns['sector_altitude_5'] = '{0:03d}'.format(sector_altitude_5)
                        if radius_limit:
                            sql_columns['sector_radius_5'] = '{0:02d}'.format(radius_limit)
                        else:
                            sql_columns['sector_radius_5'] = '  '
                        sql_columns['sector_bearing_5'] += '{0:03d}'.format(sector_bearing_1)
                    else:
                        sql_columns['sector_bearing_4'] += '{0:03d}'.format(sector_bearing_1)
                        sql_columns['sector_bearing_5'] = '      '
                        sql_columns['sector_altitude_5'] = '   '
                        sql_columns['sector_radius_5'] = '  '
                else:
                    sql_columns['sector_bearing_3'] += '{0:03d}'.format(sector_bearing_1)
                    sql_columns['sector_bearing_4'] = '      '
                    sql_columns['sector_altitude_4'] = '   '
                    sql_columns['sector_radius_4'] = '  '
                    sql_columns['sector_bearing_5'] = '      '
                    sql_columns['sector_altitude_5'] = '   '
                    sql_columns['sector_radius_5'] = '  '
            else:
                sql_columns['sector_bearing_2'] += '{0:03d}'.format(sector_bearing_1)
                sql_columns['sector_bearing_3'] = '      '
                sql_columns['sector_altitude_3'] = '   '
                sql_columns['sector_radius_3'] = '  '
                sql_columns['sector_bearing_4'] = '      '
                sql_columns['sector_altitude_4'] = '   '
                sql_columns['sector_radius_4'] = '  '
                sql_columns['sector_bearing_5'] = '      '
                sql_columns['sector_altitude_5'] = '   '
                sql_columns['sector_radius_5'] = '  '
        else:
            sql_columns['sector_bearing_1'] += '{0:03d}'.format(sector_bearing_1)
            sql_columns['sector_bearing_2'] = '      '
            sql_columns['sector_altitude_2'] = '   '
            sql_columns['sector_radius_2'] = '  '
            sql_columns['sector_bearing_3'] = '      '
            sql_columns['sector_altitude_3'] = '   '
            sql_columns['sector_radius_3'] = '  '
            sql_columns['sector_bearing_4'] = '      '
            sql_columns['sector_altitude_4'] = '   '
            sql_columns['sector_radius_4'] = '  '
            sql_columns['sector_bearing_5'] = '      '
            sql_columns['sector_altitude_5'] = '   '
            sql_columns['sector_radius_5'] = '  '
        sql_columns['sector_bearing_6'] = '      '
        sql_columns['sector_altitude_6'] = '   '
        sql_columns['sector_radius_6'] = '  '
        sql_columns['sector_bearing_7'] = '      '
        sql_columns['sector_altitude_7'] = '   '
        sql_columns['sector_radius_7'] = '  '
        sql_columns['magnetic_true_indicator'] = '{0: <1s}'.format(magnetic_true_indicator)
        # = line[120:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_msa',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_airports into airport_reference_points
def import_tbl_airports(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airport_identifier, airport_name, airport_ref_latitude, airport_ref_longitude, ifr_capability, longest_runway_surface_code, elevation, transition_altitude, transition_level, speed_limit, speed_limit_altitude ' \
        'FROM tbl_airports'
    for (area_code, icao_code, airport_identifier, airport_name, airport_ref_latitude, airport_ref_longitude, ifr_capability, longest_runway_surface_code, elevation, transition_altitude, transition_level, speed_limit, speed_limit_altitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'A'
        #sql_columns['ata_iata_designator'] = line[13:16]
        # = line[16:18]
        # = line[18:21]
        # Primary
        if speed_limit_altitude:
            sql_columns['speed_limit_altitude'] = '{0:05d}'.format(speed_limit_altitude)
        else:
            sql_columns['speed_limit_altitude'] = '     '
        #sql_columns['longest_runway'] = line[27:30]
        sql_columns['ifr_capability'] = '{0: <1s}'.format(ifr_capability or '')
        sql_columns['longest_runway_surface_code'] = '{0: <1s}'.format(longest_runway_surface_code)
        sql_columns['airport_reference_point_latitude'] = convert_dd_to_dms(airport_ref_latitude, 'NS')
        sql_columns['airport_reference_point_longitude'] = convert_dd_to_dms(airport_ref_longitude, 'EW')
        #sql_columns['magnetic_variation'] = line[51:56]
        sql_columns['airport_elevation'] = '{0:05d}'.format(elevation)
        if speed_limit:
            sql_columns['speed_limit'] = '{0:03d}'.format(speed_limit)
        else:
            sql_columns['speed_limit'] = '   '
        #sql_columns['recommended_navaid_identifier'] = line[64:68]
        #sql_columns['recommended_navaid_icao_code'] = line[68:70]
        if transition_altitude:
            sql_columns['transition_altitude'] = '{0:05d}'.format(transition_altitude)
        else:
            sql_columns['transition_altitude'] = '     '
        if transition_level:
            sql_columns['transition_level'] = '{0:05d}'.format(transition_level)
        else:
            sql_columns['transition_level'] = '     '
        #sql_columns['public_military_indicator'] = line[80:81]
        #sql_columns['time_zone'] = line[81:84]
        #sql_columns['daylight_indicator'] = line[84:85]
        #sql_columns['magnetic_true_indicator'] = line[85:86]
        #sql_columns['datum_code'] = line[86:89]
        # = line[89:93]
        sql_columns['airport_name'] = '{0: <30s}'.format(airport_name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_reference_points',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_controlled_airspace into airspace_controlled_airspaces
def import_tbl_controlled_airspace(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airspace_center, controlled_airspace_name, airspace_type, airspace_classification, seqno, flightlevel, boundary_via, latitude, longitude, arc_origin_latitude, arc_origin_longitude, arc_distance, arc_bearing, unit_indicator_lower_limit, lower_limit, unit_indicator_upper_limit, upper_limit ' \
        'FROM tbl_controlled_airspace'
    for (area_code, icao_code, airspace_center, controlled_airspace_name, airspace_type, airspace_classification, seqno, flightlevel, boundary_via, latitude, longitude, arc_origin_latitude, arc_origin_longitude, arc_distance, arc_bearing, unit_indicator_lower_limit, lower_limit, unit_indicator_upper_limit, upper_limit) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'U'
        sql_columns['subsection_code'] = 'C'
        sql_columns['airspace_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['airspace_type'] = '{0: <1s}'.format(airspace_type)
        sql_columns['airspace_center_identifier'] = '{0: <5s}'.format(airspace_center)
        sql_columns['airspace_section_code'] = 'P'
        sql_columns['airspace_subsection_code'] = 'A'
        sql_columns['airspace_classification'] = '{0: <1s}'.format(airspace_classification or '')
        # = line[17:19]
        #sql_columns['multiple_code'] = line[19:20]
        sql_columns['sequence_number'] = '{0:04d}'.format(seqno)
        # Primary
        sql_columns['level'] = '{0: <1s}'.format(flightlevel or '')
        #sql_columns['time_code'] = line[26:27]
        #sql_columns['notam'] = line[27:28]
        # = line[28:30]
        sql_columns['boundary_via'] = '{0: <2s}'.format(boundary_via)
        sql_columns['latitude'] = convert_dd_to_dms(latitude, 'NS')
        sql_columns['longitude'] = convert_dd_to_dms(longitude, 'EW')
        sql_columns['arc_origin_latitude'] = convert_dd_to_dms(arc_origin_latitude, 'NS')
        sql_columns['arc_origin_longitude'] = convert_dd_to_dms(arc_origin_longitude, 'EW')
        if arc_distance:
            sql_columns['arc_distance'] = '{0:04.0f}'.format(arc_distance * 10.0)
        else:
            sql_columns['arc_distance'] = '    '
        if arc_bearing:
            sql_columns['arc_bearing'] = '{0:04.0f}'.format(arc_bearing * 10.0)
        else:
            sql_columns['arc_bearing'] = '    '
        #sql_columns['rnp'] = line[78:81]
        sql_columns['lower_limit'] = '{0: <5s}'.format(lower_limit or '')
        sql_columns['lower_limit_unit_indicator'] = '{0: <1s}'.format(unit_indicator_lower_limit or '')
        sql_columns['upper_limit'] = '{0: <5s}'.format(upper_limit or '')
        sql_columns['upper_limit_unit_indicator'] = '{0: <1s}'.format(unit_indicator_upper_limit or '')
        sql_columns['controlled_airspace_name'] = '{0: <30s}'.format(controlled_airspace_name or '')
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airspace_controlled_airspaces',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_cruising_tables into tables_cruising_tables
def import_tbl_cruising_tables(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT cruise_table_identifier, seqno, course_from, course_to, mag_true, cruise_level_from1, vertical_separation1, cruise_level_to1, cruise_level_from2, vertical_separation2, cruise_level_to2, cruise_level_from3, vertical_separation3, cruise_level_to3, cruise_level_from4, vertical_separation4, cruise_level_to4 ' \
        'FROM tbl_cruising_tables'
    for (cruise_table_identifier, seqno, course_from, course_to, mag_true, cruise_level_from1, vertical_separation1, cruise_level_to1, cruise_level_from2, vertical_separation2, cruise_level_to2, cruise_level_from3, vertical_separation3, cruise_level_to3, cruise_level_from4, vertical_separation4, cruise_level_to4) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        sql_columns['record_type'] = 'S'
        # = line[1:4]
        sql_columns['section_code'] = 'T'
        sql_columns['subsection_code'] = 'C'
        sql_columns['cruise_table_identifier'] = '{0: <2s}'.format(cruise_table_identifier)
        sql_columns['sequence_number'] = '{0:04d}'.format(seqno)
        # = line[9:28]
        sql_columns['course_from'] = '{0:04.0f}'.format(course_from * 10.0)
        sql_columns['course_to'] = '{0:04.0f}'.format(course_to * 10.0)
        #sql_columns['mag_true'] = '{0: <1s}'.format(mag_true)
        # = line[37:39]
        sql_columns['cruise_level_from_1'] = '{0:05d}'.format(cruise_level_from1)
        sql_columns['vertical_separation_1'] = '{0:05d}'.format(vertical_separation1)
        if cruise_level_to1:
            sql_columns['cruise_level_to_1'] = '{0:05d}'.format(cruise_level_to1)
        else:
            sql_columns['cruise_level_to_1'] = '     '
        if cruise_level_from2:
            sql_columns['cruise_level_from_2'] = '{0:05d}'.format(cruise_level_from2)
        else:
            sql_columns['cruise_level_from_2'] = '     '
        if vertical_separation2:
            sql_columns['vertical_separation_2'] = '{0:05d}'.format(vertical_separation2)
        else:
            sql_columns['vertical_separation_2'] = '     '
        if cruise_level_to2:
            sql_columns['cruise_level_to_2'] = '{0:05d}'.format(cruise_level_to2)
        else:
            sql_columns['cruise_level_to_2'] = '     '
        if cruise_level_from3:
            sql_columns['cruise_level_from_3'] = '{0:05d}'.format(cruise_level_from3)
        else:
            sql_columns['cruise_level_from_3'] = '     '
        if vertical_separation3:
            sql_columns['vertical_separation_3'] = '{0:05d}'.format(vertical_separation3)
        else:
            sql_columns['vertical_separation_3'] = '     '
        if cruise_level_to3:
            sql_columns['cruise_level_to_3'] = '{0:05d}'.format(cruise_level_to3)
        else:
            sql_columns['cruise_level_to_3'] = '     '
        if cruise_level_from4:
            sql_columns['cruise_level_from_4'] = '{0:05d}'.format(cruise_level_from4)
        else:
            sql_columns['cruise_level_from_4'] = '     '
        if vertical_separation4:
            sql_columns['vertical_separation_4'] = '{0:05d}'.format(vertical_separation4)
        else:
            sql_columns['vertical_separation_4'] = '     '
        if cruise_level_to4:
            sql_columns['cruise_level_to_4'] = '{0:05d}'.format(cruise_level_to4)
        else:
            sql_columns['cruise_level_to_4'] = '     '
        # = line[99:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'tables_cruising_tables',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_enroute_airway_restriction into enroute_airway_restrictions
def import_tbl_enroute_airway_restriction(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, route_identifier, restriction_identifier, restriction_type, start_waypoint_identifier, start_waypoint_latitude, start_waypoint_longitude, end_waypoint_identifier, end_waypoint_latitude, end_waypoint_longitude, start_date, end_date, units_of_altitude, restriction_altitude1, block_indicator1, restriction_altitude2, block_indicator2, restriction_altitude3, block_indicator3, restriction_altitude4, block_indicator4, restriction_altitude5, block_indicator5, restriction_altitude6, block_indicator6, restriction_altitude7, block_indicator7, restriction_notes ' \
        'FROM tbl_enroute_airway_restriction'
    for (area_code, route_identifier, restriction_identifier, restriction_type, start_waypoint_identifier, start_waypoint_latitude, start_waypoint_longitude, end_waypoint_identifier, end_waypoint_latitude, end_waypoint_longitude, start_date, end_date, units_of_altitude, restriction_altitude1, block_indicator1, restriction_altitude2, block_indicator2, restriction_altitude3, block_indicator3, restriction_altitude4, block_indicator4, restriction_altitude5, block_indicator5, restriction_altitude6, block_indicator6, restriction_altitude7, block_indicator7, restriction_notes) in sql_cursor_in.execute(sql_command):
        (found_start_waypoint_icao_code, found_start_waypoint_section_code, found_start_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            start_waypoint_identifier,
            convert_dd_to_dms(start_waypoint_latitude, 'NS'),
            convert_dd_to_dms(start_waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA'])
        (found_end_waypoint_icao_code, found_end_waypoint_section_code, found_end_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            end_waypoint_identifier,
            convert_dd_to_dms(end_waypoint_latitude, 'NS'),
            convert_dd_to_dms(end_waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA'])

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'E'
        sql_columns['subsection_code'] = 'U'
        sql_columns['route_identifier'] = '{0: <5s}'.format(route_identifier)
        # = line[11:12]
        sql_columns['restriction_identifier'] = '{0:03d}'.format(restriction_identifier)
        sql_columns['restriction_type'] = '{0: <2s}'.format(restriction_type)
        # Altitude Exclusion Primary
        sql_columns['start_fix_identifier'] = '{0: <5s}'.format(start_waypoint_identifier or '')
        sql_columns['start_fix_icao_code'] = '{0: <2s}'.format(found_start_waypoint_icao_code or '')
        sql_columns['start_fix_section_code'] = '{0: <1s}'.format(found_start_waypoint_section_code or '')
        sql_columns['start_fix_subsection_code'] = '{0: <1s}'.format(found_start_waypoint_subsection_code or '')
        sql_columns['end_fix_identifier'] = '{0: <5s}'.format(end_waypoint_identifier)
        sql_columns['end_fix_icao_code'] = '{0: <2s}'.format(found_end_waypoint_icao_code or '')
        sql_columns['end_fix_section_code'] = '{0: <1s}'.format(found_end_waypoint_section_code or '')
        sql_columns['end_fix_subsection_code'] = '{0: <1s}'.format(found_end_waypoint_subsection_code or '')
        # = line[36:37]
        sql_columns['start_date'] = '{0: <7s}'.format(start_date or '')
        sql_columns['end_date'] = '{0: <7s}'.format(end_date or '')
        #sql_columns['time_code'] = line[51:52]
        #sql_columns['time_indicator'] = line[52:53]
        #sql_columns['time_of_operation_1'] = line[53:63]
        #sql_columns['time_of_operation_2'] = line[63:73]
        #sql_columns['time_of_operation_3'] = line[73:83]
        #sql_columns['time_of_operation_4'] = line[83:93]
        #sql_columns['exclusion_indicator'] = line[93:94]
        sql_columns['units_of_altitude'] = '{0: <1s}'.format(units_of_altitude or '')
        if restriction_altitude1:
            sql_columns['restriction_altitude_1'] = '{0:03d}'.format(restriction_altitude1)
        else:
            sql_columns['restriction_altitude_1'] = '   '
        sql_columns['block_indicator_1'] = '{0: <1s}'.format(block_indicator1 or '')
        if restriction_altitude2:
            sql_columns['restriction_altitude_2'] = '{0:03d}'.format(restriction_altitude2)
        else:
            sql_columns['restriction_altitude_2'] = '   '
        sql_columns['block_indicator_2'] = '{0: <1s}'.format(block_indicator2 or '')
        if restriction_altitude3:
            sql_columns['restriction_altitude_3'] = '{0:03d}'.format(restriction_altitude3)
        else:
            sql_columns['restriction_altitude_3'] = '   '
        sql_columns['block_indicator_3'] = '{0: <1s}'.format(block_indicator3 or '')
        if restriction_altitude4:
            sql_columns['restriction_altitude_4'] = '{0:03d}'.format(restriction_altitude4)
        else:
            sql_columns['restriction_altitude_4'] = '   '
        sql_columns['block_indicator_4'] = '{0: <1s}'.format(block_indicator4 or '')
        if restriction_altitude5:
            sql_columns['restriction_altitude_5'] = '{0:03d}'.format(restriction_altitude5)
        else:
            sql_columns['restriction_altitude_5'] = '   '
        sql_columns['block_indicator_5'] = '{0: <1s}'.format(block_indicator5 or '')
        if restriction_altitude6:
            sql_columns['restriction_altitude_6'] = '{0:03d}'.format(restriction_altitude6)
        else:
            sql_columns['restriction_altitude_6'] = '   '
        sql_columns['block_indicator_6'] = '{0: <1s}'.format(block_indicator6 or '')
        if restriction_altitude7:
            sql_columns['restriction_altitude_7'] = '{0:03d}'.format(restriction_altitude7)
        else:
            sql_columns['restriction_altitude_7'] = '   '
        sql_columns['block_indicator_7'] = '{0: <1s}'.format(block_indicator7 or '')
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        # Note Restriction Primary
        # TODO if restriction_notes:
        #    sql_columns['restriction_notes'] = '{0: <69s}'.format(restriction_notes)
        # Note Restriction Continuation
        # Seasonal Closure Primary
        # Cruise Table Replacement Primary
        # Cruise Table Replacement Continuation
        # Altitude Exclusion Continuation
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'enroute_airway_restrictions',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_enroute_airways into enroute_airways_and_routes
def import_tbl_enroute_airways(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, route_identifier, seqno, icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, route_type, flightlevel, direction_restriction, crusing_table_identifier, minimum_altitude1, minimum_altitude2, maximum_altitude, outbound_course, inbound_course, inbound_distance ' \
        'FROM tbl_enroute_airways'
    for (area_code, route_identifier, seqno, icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, route_type, flightlevel, direction_restriction, crusing_table_identifier, minimum_altitude1, minimum_altitude2, maximum_altitude, outbound_course, inbound_course, inbound_distance) in sql_cursor_in.execute(sql_command):
        (found_waypoint_icao_code, found_waypoint_section_code, found_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            waypoint_identifier,
            convert_dd_to_dms(waypoint_latitude, 'NS'),
            convert_dd_to_dms(waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA'])

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'E'
        sql_columns['subsection_code'] = 'R'
        # = line[6:13]
        sql_columns['route_identifier'] = '{0: <5s}'.format(route_identifier)
        # = line[18:19]
        # = line[19:25]
        sql_columns['sequence_number']  = '{0:04d}'.format(seqno)
        sql_columns['fix_identifier'] = '{0: <5s}'.format(waypoint_identifier)
        sql_columns['fix_icao_code'] = '{0: <2s}'.format(icao_code)
        #sql_columns['fix_icao_code'] = '{0: <2s}'.format(found_waypoint_icao_code or '')
        if icao_code and found_waypoint_icao_code and (icao_code != found_waypoint_icao_code.strip(' ')):
            print('waypoint icao codes differ: {0:s} != {1:s}'.format(icao_code, found_waypoint_icao_code))
        sql_columns['fix_section_code'] = '{0: <1s}'.format(found_waypoint_section_code or '')
        sql_columns['fix_subsection_code'] = '{0: <1s}'.format(found_waypoint_subsection_code or '')
        # Primary
        sql_columns['waypoint_description_code'] = '{0: <4s}'.format(waypoint_description_code)
        #sql_columns['boundary_code'] = line[43:44]
        sql_columns['route_type'] = '{0: <1s}'.format(route_type)
        sql_columns['level'] = '{0: <1s}'.format(flightlevel)
        sql_columns['direction_restriction'] = '{0: <1s}'.format(direction_restriction or '')
        sql_columns['cruise_table_indicator'] = '{0: <2s}'.format(crusing_table_identifier or '')
        #sql_columns['eu_indicator'] = line[49:50]
        #sql_columns['recommended_navaid_identifier'] = line[50:54]
        #sql_columns['recommended_navaid_icao_code'] = line[54:56]
        #sql_columns['rnp'] = line[56:59]
        # = line[59:62]
        #sql_columns['theta'] = line[62:66]
        #sql_columns['rho'] = line[66:70]
        if outbound_course:
            sql_columns['outbound_magnetic_course'] = '{0:04.0f}'.format(outbound_course * 10.0)
        else:
            sql_columns['outbound_magnetic_course'] = '    '
        sql_columns['route_distance_from'] = '{0:04.0f}'.format(inbound_distance * 10.0)
        if inbound_course:
            sql_columns['inbound_magnetic_course'] = '{0:04.0f}'.format(inbound_course * 10.0)
        else:
            sql_columns['inbound_magnetic_course'] = '    '
        # = line[82:83]
        if minimum_altitude1:
            sql_columns['minimum_altitude_1'] = '{0:05d}'.format(minimum_altitude1)
        else:
            sql_columns['minimum_altitude_1'] = '     '
        if minimum_altitude2:
            sql_columns['minimum_altitude_2'] = '{0:05d}'.format(minimum_altitude2)
        else:
            sql_columns['minimum_altitude_2'] = '     '
        if maximum_altitude:
            sql_columns['maximum_altitude'] = '{0:05d}'.format(maximum_altitude)
        else:
            sql_columns['maximum_altitude'] = '     '
        #sql_columns['fix_radius_transition_indicator'] = line[98:101]
        #sql_columns['vertical_scale_factor'] = line[101:104]
        #sql_columns['rvsm_minimum_level'] = line[104:107]
        #sql_columns['vsf_rvsm_maximum_level'] = line[107:110]
        # = line[110:114]
        # = line[114:120]
        #sql_columns['route_qualifier_1'] = line[120:121]
        #sql_columns['route_qualifier_2'] = line[121:122]
        #sql_columns['route_qualifier_3'] = line[122:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'enroute_airways_and_routes',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_enroute_communication into enroute_communications
def import_tbl_enroute_communication(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, fir_rdo_ident, fir_uir_indicator, communication_type, communication_frequency, frequency_units, service_indicator, remote_name, callsign, latitude, longitude ' \
        'FROM tbl_enroute_communication'
    for (area_code, fir_rdo_ident, fir_uir_indicator, communication_type, communication_frequency, frequency_units, service_indicator, remote_name, callsign, latitude, longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'E'
        sql_columns['subsection_code'] = 'V'
        sql_columns['fir_rdo_identifier'] = '{0: <4s}'.format(fir_rdo_ident)
        #sql_columns['fir_uir_address'] = line[10:14]
        sql_columns['indicator'] = '{0: <1s}'.format(fir_uir_indicator or '')
        #sql_columns['communication_class'] = line[15:19]
        #sql_columns['sequence_number'] = line[19:21]
        # Primary
        sql_columns['communications_type'] = '{0: <3s}'.format(communication_type)
        sql_columns['transmit_frequency'] = convert_communication_frequency(communication_frequency, frequency_units)
        sql_columns['receive_frequency'] = convert_communication_frequency(communication_frequency, frequency_units)
        sql_columns['frequency_units'] = '{0: <1s}'.format(frequency_units)
        #sql_columns['radar_service'] = line[40:41]
        #sql_columns['h24_indicator'] = line[41:42]
        sql_columns['call_sign'] = '{0: <25s}'.format(callsign or '')
        #sql_columns['position_narrative'] = line[67:92]
        sql_columns['latitude'] = convert_dd_to_dms(latitude, 'NS')
        sql_columns['longitude'] = convert_dd_to_dms(longitude, 'EW')
        sql_columns['service_indicator'] = '{0: <3s}'.format(service_indicator or '')
        #sql_columns['modulation'] = line[114:115]
        #sql_columns['signal_emission'] = line[115:116]
        #sql_columns['altitude_description'] = line[116:117]
        #sql_columns['communication_altitude_1'] = line[117:120]
        #sql_columns['communication_altitude_2'] = line[120:123]
        # TODO missing: remote_name --> e_remote_facility_*
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        # Continuation
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'enroute_communications',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_enroute_ndbnavaids into navaid_ndb_navaids
def import_tbl_enroute_ndbnavaids(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, ndb_identifier, ndb_name, ndb_frequency, navaid_class, ndb_latitude, ndb_longitude ' \
        'FROM tbl_enroute_ndbnavaids'
    for (area_code, icao_code, ndb_identifier, ndb_name, ndb_frequency, navaid_class, ndb_latitude, ndb_longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'D'
        sql_columns['subsection_code'] = 'B'
        sql_columns['airport_identifier'] = '    '
        sql_columns['airport_icao_code'] = '  '
        # = line[12:13]
        sql_columns['ndb_identifier'] = '{0: <4s}'.format(ndb_identifier)
        # = line[17:19]
        sql_columns['ndb_icao_code'] = '{0: <2s}'.format(icao_code)
        # Primary
        sql_columns['ndb_frequency'] = '{0:05.0f}'.format(ndb_frequency * 10.0)
        sql_columns['ndb_class'] = '{0: <5s}'.format(navaid_class)
        sql_columns['ndb_latitude'] = convert_dd_to_dms(ndb_latitude, 'NS')
        sql_columns['ndb_longitude'] = convert_dd_to_dms(ndb_longitude, 'EW')
        # = line[51:74]
        #sql_columns['magnetic_variation'] = line[74:79]
        # = line[79:85]
        # = line[85:90]
        #sql_columns['datum_code'] = line[90:93]
        sql_columns['ndb_name'] = '{0: <30s}'.format(ndb_name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'navaid_ndb_navaids',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_enroute_waypoints into enroute_waypoints
def import_tbl_enroute_waypoints(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, waypoint_identifier, waypoint_name, waypoint_type, waypoint_usage, waypoint_latitude, waypoint_longitude ' \
        'FROM tbl_enroute_waypoints'
    for (area_code, icao_code, waypoint_identifier, waypoint_name, waypoint_type, waypoint_usage, waypoint_latitude, waypoint_longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'E'
        sql_columns['subsection_code'] = 'A'
        sql_columns['region_code'] = 'ENRT'
        sql_columns['region_icao_code'] = '  '
        # = line[12:13]
        sql_columns['waypoint_identifier'] = '{0: <5s}'.format(waypoint_identifier)
        # = line[18:19]
        sql_columns['waypoint_icao_code'] = '{0: <2s}'.format(icao_code)
        # Primary
        # = line[22:26]
        sql_columns['waypoint_type'] = '{0: <3s}'.format(waypoint_type)
        # = line[29:30]
        sql_columns['waypoint_usage'] = '{0: <2s}'.format(waypoint_usage or '')
        # = line[31:32]
        sql_columns['waypoint_latitude'] = convert_dd_to_dms(waypoint_latitude, 'NS')
        sql_columns['waypoint_longitude'] = convert_dd_to_dms(waypoint_longitude, 'EW')
        # = line[51:74]
        #sql_columns['dynamic_magnetic_variation'] = line[74:79]
        # = line[79:84]
        #sql_columns['datum_code'] = line[84:87]
        # = line[87:95]
        #sql_columns['name_format_indicator'] = line[95:98]
        sql_columns['waypoint_name_description'] = '{0: <25s}'.format(waypoint_name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'enroute_waypoints',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_fir_uir into airspace_fir_uir
def import_tbl_fir_uir(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, fir_uir_identifier, fir_uir_name, fir_uir_indicator, seqno, boundary_via, adjacent_fir_identifier, adjacent_uir_identifier, reporting_units_speed, reporting_units_altitude, fir_uir_latitude, fir_uir_longitude, arc_origin_latitude, arc_origin_longitude, arc_distance, arc_bearing, fir_upper_limit, uir_lower_limit, uir_upper_limit, cruise_table_identifier ' \
        'FROM tbl_fir_uir'
    for (area_code, fir_uir_identifier, fir_uir_name, fir_uir_indicator, seqno, boundary_via, adjacent_fir_identifier, adjacent_uir_identifier, reporting_units_speed, reporting_units_altitude, fir_uir_latitude, fir_uir_longitude, arc_origin_latitude, arc_origin_longitude, arc_distance, arc_bearing, fir_upper_limit, uir_lower_limit, uir_upper_limit, cruise_table_identifier) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'U'
        sql_columns['subsection_code'] = 'F'
        sql_columns['fir_uir_identifier'] = '{0: <4s}'.format(fir_uir_identifier)
        #sql_columns['fir_uir_address'] = line[10:14]
        sql_columns['fir_uir_indicator'] = '{0: <1s}'.format(fir_uir_indicator)
        sql_columns['sequence_number'] = '{0:04d}'.format(seqno)
        # Primary
        sql_columns['adjacent_fir_identifier'] = '{0: <4s}'.format(adjacent_fir_identifier or '')
        sql_columns['adjacent_uir_identifier'] = '{0: <4s}'.format(adjacent_uir_identifier or '')
        if reporting_units_speed:
            sql_columns['reporting_units_speed'] = '{0:01d}'.format(reporting_units_speed)
        else:
            sql_columns['reporting_units_speed'] = ' '
        if reporting_units_altitude:
            sql_columns['reporting_units_altitude'] = '{0:01d}'.format(reporting_units_altitude)
        else:
            sql_columns['reporting_units_altitude'] = ' '
        #sql_columns['entry_report'] = line[30:31]
        # = line[31:32]
        sql_columns['boundary_via'] = '{0: <2s}'.format(boundary_via)
        sql_columns['fir_uir_latitude'] = convert_dd_to_dms(fir_uir_latitude, 'NS')
        sql_columns['fir_uir_longitude'] = convert_dd_to_dms(fir_uir_longitude, 'EW')
        sql_columns['arc_origin_latitude'] = convert_dd_to_dms(arc_origin_latitude, 'NS')
        sql_columns['arc_origin_longitude'] = convert_dd_to_dms(arc_origin_longitude, 'EW')
        if arc_distance:
            sql_columns['arc_distance'] = '{0:04.0f}'.format(arc_distance * 10.0)
        else:
            sql_columns['arc_distance'] = '    '
        if arc_bearing:
            sql_columns['arc_bearing'] = '{0:04.0f}'.format(arc_bearing * 10.0)
        else:
            sql_columns['arc_bearing'] = '    '
        sql_columns['fir_upper_limit'] = '{0: <5s}'.format(fir_upper_limit or '')
        sql_columns['uir_lower_limit'] = '{0: <5s}'.format(uir_lower_limit or '')
        sql_columns['uir_upper_limit'] = '{0: <5s}'.format(uir_upper_limit or '')
        sql_columns['cruise_table_ind'] = '{0: <2s}'.format(cruise_table_identifier or '')
        # = line[97:98]
        sql_columns['fir_uir_name'] = '{0: <25s}'.format(fir_uir_name or '')
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airspace_fir_uir',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_gate into airport_gates
def import_tbl_gate(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, icao_code, gate_identifier, gate_latitude, gate_longitude, name ' \
        'FROM tbl_gate'
    for (area_code, airport_identifier, icao_code, gate_identifier, gate_latitude, gate_longitude, name) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'B'
        sql_columns['gate_identifier'] = '{0: <5s}'.format(gate_identifier)
        # = line[18:21]
        # Primary
        # = line[22:32]
        sql_columns['gate_latitude'] = convert_dd_to_dms(gate_latitude, 'NS')
        sql_columns['gate_longitude'] = convert_dd_to_dms(gate_longitude, 'EW')
        # = line[51:98]
        sql_columns['name'] = '{0: <25s}'.format(name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_gates',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_gls into airport_gls_stations
def import_tbl_gls(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, icao_code, gls_ref_path_identifier, gls_category, gls_channel, runway_identifier, gls_approach_bearing, station_latitude, station_longitude, gls_station_ident, gls_approach_slope, magentic_variation, station_elevation, station_type ' \
        'FROM tbl_gls'
    for (area_code, airport_identifier, icao_code, gls_ref_path_identifier, gls_category, gls_channel, runway_identifier, gls_approach_bearing, station_latitude, station_longitude, gls_station_ident, gls_approach_slope, magentic_variation, station_elevation, station_type) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_or_heliport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_or_heliport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'T'
        sql_columns['gls_ref_path_identifier'] = '{0: <4s}'.format(gls_ref_path_identifier)
        sql_columns['gls_category'] = '{0: <1s}'.format(gls_category)
        # = line[18:21]
        # Primary
        sql_columns['gbas_sbas_channel'] = '{0:05d}'.format(gls_channel)
        sql_columns['runway_or_helipad_identifier'] = '{0: <5s}'.format(runway_identifier)
        # = line[32:51]
        sql_columns['gls_approach_bearing'] = '{0:04.0f}'.format(gls_approach_bearing * 10.0)
        sql_columns['station_latitude'] = convert_dd_to_dms(station_latitude, 'NS')
        sql_columns['station_longitude'] = convert_dd_to_dms(station_longitude, 'EW')
        sql_columns['gls_station_identifier'] = '{0: <4s}'.format(gls_station_ident)
        # = line[78:83]
        #sql_columns['service_volume_radius'] = line[83:85]
        #sql_columns['tdma_slots'] = line[85:87]
        sql_columns['gls_approach_slope'] = '{0:03.0f}'.format(gls_approach_slope * 100.0)
        sql_columns['magnetic_variation'] = convert_magnetic_variation(magentic_variation)
        # = line[95:97]
        sql_columns['station_elevation'] = '{0:05d}'.format(station_elevation)
        #sql_columns['datum_code'] = line[102:105]
        sql_columns['station_type'] = '{0: <3s}'.format(station_type or '')
        # = line[108:110]
        #sql_columns['station_elevation_wgs_84'] = line[110:115]
        #sql_columns['glide_path_tch'] = line[115:118]
        # = line[118:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_gls_stations',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_grid_mora into mora_grid_mora
def import_tbl_grid_mora(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT starting_latitude, starting_longitude, mora01, mora02, mora03, mora04, mora05, mora06, mora07, mora08, mora09, mora10, mora11, mora12, mora13, mora14, mora15, mora16, mora17, mora18, mora19, mora20, mora21, mora22, mora23, mora24, mora25, mora26, mora27, mora28, mora29, mora30 ' \
        'FROM tbl_grid_mora'
    for (starting_latitude, starting_longitude, mora01, mora02, mora03, mora04, mora05, mora06, mora07, mora08, mora09, mora10, mora11, mora12, mora13, mora14, mora15, mora16, mora17, mora18, mora19, mora20, mora21, mora22, mora23, mora24, mora25, mora26, mora27, mora28, mora29, mora30) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        sql_columns['record_type'] = 'S'
        # = line[1:4]
        sql_columns['section_code'] = 'A'
        sql_columns['subsection_code'] = 'S'
        # = line[6:13]
        if starting_latitude < 0:
            sql_columns['starting_latitude'] = 'S{0:02d}'.format(-starting_latitude)
        else:
            sql_columns['starting_latitude'] = 'N{0:02d}'.format(starting_latitude)
        if starting_longitude < 0:
            sql_columns['starting_longitude'] = 'W{0:03d}'.format(-starting_longitude)
        else:
            sql_columns['starting_longitude'] = 'E{0:03d}'.format(starting_longitude)
        # = line[20:30]
        sql_columns['mora_01'] = '{0:<3s}'.format(mora01)
        sql_columns['mora_02'] = '{0:<3s}'.format(mora02)
        sql_columns['mora_03'] = '{0:<3s}'.format(mora03)
        sql_columns['mora_04'] = '{0:<3s}'.format(mora04)
        sql_columns['mora_05'] = '{0:<3s}'.format(mora05)
        sql_columns['mora_06'] = '{0:<3s}'.format(mora06)
        sql_columns['mora_07'] = '{0:<3s}'.format(mora07)
        sql_columns['mora_08'] = '{0:<3s}'.format(mora08)
        sql_columns['mora_09'] = '{0:<3s}'.format(mora09)
        sql_columns['mora_10'] = '{0:<3s}'.format(mora10)
        sql_columns['mora_11'] = '{0:<3s}'.format(mora11)
        sql_columns['mora_12'] = '{0:<3s}'.format(mora12)
        sql_columns['mora_13'] = '{0:<3s}'.format(mora13)
        sql_columns['mora_14'] = '{0:<3s}'.format(mora14)
        sql_columns['mora_15'] = '{0:<3s}'.format(mora15)
        sql_columns['mora_16'] = '{0:<3s}'.format(mora16)
        sql_columns['mora_17'] = '{0:<3s}'.format(mora17)
        sql_columns['mora_18'] = '{0:<3s}'.format(mora18)
        sql_columns['mora_19'] = '{0:<3s}'.format(mora19)
        sql_columns['mora_20'] = '{0:<3s}'.format(mora20)
        sql_columns['mora_21'] = '{0:<3s}'.format(mora21)
        sql_columns['mora_22'] = '{0:<3s}'.format(mora22)
        sql_columns['mora_23'] = '{0:<3s}'.format(mora23)
        sql_columns['mora_24'] = '{0:<3s}'.format(mora24)
        sql_columns['mora_25'] = '{0:<3s}'.format(mora25)
        sql_columns['mora_26'] = '{0:<3s}'.format(mora26)
        sql_columns['mora_27'] = '{0:<3s}'.format(mora27)
        sql_columns['mora_28'] = '{0:<3s}'.format(mora28)
        sql_columns['mora_29'] = '{0:<3s}'.format(mora29)
        sql_columns['mora_30'] = '{0:<3s}'.format(mora30)
        # = line[120:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'mora_grid_mora',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_header into header
def import_tbl_header(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT version, arincversion, record_set, current_airac, effective_fromto, previous_airac, previous_fromto ' \
        'FROM tbl_header'
    for (version, arincversion, record_set, current_airac, effective_fromto, previous_airac, previous_fromto) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        sql_columns['header_identifier'] = 'HDR'
        #sql_columns['header_number'] = line[3:5]
        # Header 01
        #sql_columns['file_name'] = line[5:20]
        #sql_columns['version_number'] = line[20:23]
        #sql_columns['production_test_flag'] = line[23:24]
        #sql_columns['record_length'] = line[24:28]
        #sql_columns['record_count'] = line[28:35]
        #sql_columns['cycle_date'] = line[35:39]
        # = line[39:41]
        #sql_columns['creation_date'] = line[41:52]
        #sql_columns['creation_time'] = line[52:60]
        # = line[60:61]
        #sql_columns['data_supplier_identifier'] = line[61:77]
        #sql_columns['target_customer_identifier'] = line[77:93]
        #sql_columns['database_part_number'] = line[93:113]
        # = line[113:124]
        #sql_columns['file_crc'] = line[124:132]
        # Header 02..
        #sql_columns['effective_date'] = line[5:16]
        #sql_columns['expiration_date'] = line[16:27]
        # = line[27:28]
        #sql_columns['supplier_text_field'] = line[28:58]
        #sql_columns['descriptive_text'] = line[58:88]
        # = line[88:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
        'header',
        ', '.join([str(key) for key in sql_columns.keys()]),
        ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_holdings into enroute_holding_patterns
def import_tbl_holdings(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, region_code, icao_code, waypoint_identifier, holding_name, waypoint_latitude, waypoint_longitude, duplicate_identifier, inbound_holding_course, turn_direction, leg_length, leg_time, minimum_altitude, maximum_altitude, holding_speed ' \
        'FROM tbl_holdings'
    for (area_code, region_code, icao_code, waypoint_identifier, holding_name, waypoint_latitude, waypoint_longitude, duplicate_identifier, inbound_holding_course, turn_direction, leg_length, leg_time, minimum_altitude, maximum_altitude, holding_speed) in sql_cursor_in.execute(sql_command):
        (found_waypoint_icao_code, found_waypoint_section_code, found_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            waypoint_identifier,
            convert_dd_to_dms(waypoint_latitude, 'NS'),
            convert_dd_to_dms(waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PC', 'PN'])

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'E'
        sql_columns['subsection_code'] = 'P'
        sql_columns['region_code'] = '{0: <4s}'.format(region_code)
        sql_columns['region_icao_code'] = '{0: <2s}'.format(icao_code or '')
        # = line[12:27]
        sql_columns['duplicate_identifier'] = '{0:02d}'.format(duplicate_identifier)
        sql_columns['fix_identifier'] = '{0: <5s}'.format(waypoint_identifier)
        sql_columns['fix_icao_code'] = '{0: <2s}'.format(found_waypoint_icao_code or '')
        sql_columns['fix_section_code'] = '{0: <1s}'.format(found_waypoint_section_code or '')
        sql_columns['fix_subsection_code'] = '{0: <1s}'.format(found_waypoint_subsection_code or '')
        # Primary
        sql_columns['inbound_holding_course'] = '{0:04.0f}'.format(inbound_holding_course * 10.0)
        sql_columns['turn_direction'] = '{0: <1s}'.format(turn_direction)
        if leg_length:
            sql_columns['leg_length'] = '{0:03.0f}'.format(leg_length * 10.0)
        else:
            sql_columns['leg_length'] = '   '
        if leg_time:
            sql_columns['leg_time'] = '{0:02.0f}'.format(leg_time * 10.0)
        else:
            sql_columns['leg_time'] = '  '
        if minimum_altitude:
            sql_columns['minimum_altitude'] = '{0:05d}'.format(minimum_altitude)
        else:
            sql_columns['minimum_altitude'] = '     '
        if maximum_altitude:
            sql_columns['maximum_altitude'] = '{0:05d}'.format(maximum_altitude)
        else:
            sql_columns['maximum_altitude'] = '     '
        if holding_speed:
            sql_columns['holding_speed'] = '{0:03d}'.format(holding_speed)
        else:
            sql_columns['holding_speed'] = '   '
        #sql_columns['rnp'] = line[62:65]
        #sql_columns['arc_radius'] = line[65:71]
        #sql_columns['vertical_scale_factor'] = line[71:74]
        #sql_specific_columns['rvsm_minimum_level'] = line[74:77]
        #sql_specific_columns['rvsm_maximum_level'] = line[77:80]
        #sql_specific_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:98]
        sql_columns['name'] = '{0: <25s}'.format(holding_name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'enroute_holding_patterns',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_iaps into airport_approach_procedures
def import_tbl_iaps(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, procedure_identifier, route_type, transition_identifier, seqno, waypoint_icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, turn_direction, rnp, path_termination, recommanded_navaid, recommanded_navaid_latitude, recommanded_navaid_longitude, arc_radius, theta, rho, magnetic_course, route_distance_holding_distance_time, altitude_description, altitude1, altitude2, transition_altitude, speed_limit_description, speed_limit, vertical_angle, center_waypoint, center_waypoint_latitude, center_waypoint_longitude ' \
        'FROM tbl_iaps'
    for (area_code, airport_identifier, procedure_identifier, route_type, transition_identifier, seqno, waypoint_icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, turn_direction, rnp, path_termination, recommanded_navaid, recommanded_navaid_latitude, recommanded_navaid_longitude, arc_radius, theta, rho, magnetic_course, route_distance_holding_distance_time, altitude_description, altitude1, altitude2, transition_altitude, speed_limit_description, speed_limit, vertical_angle, center_waypoint, center_waypoint_latitude, center_waypoint_longitude) in sql_cursor_in.execute(sql_command):
        (found_waypoint_icao_code, found_waypoint_section_code, found_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            waypoint_identifier,
            convert_dd_to_dms(waypoint_latitude, 'NS'),
            convert_dd_to_dms(waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PA', 'PC', 'PG', 'PN'],
            icao_code=waypoint_icao_code)
        (found_recommended_navaid_icao_code, found_recommended_navaid_section_code, found_recommended_navaid_subsection_code) = search_navaid(
            sql_cursor_out,
            recommanded_navaid,
            convert_dd_to_dms(recommanded_navaid_latitude, 'NS'),
            convert_dd_to_dms(recommanded_navaid_longitude, 'EW'),
            ['D ', 'DB', 'PI', 'PN', 'PT'],
            airport_identifier=airport_identifier)
        (found_center_waypoint_icao_code, found_center_waypoint_section_code, found_center_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            center_waypoint,
            convert_dd_to_dms(center_waypoint_latitude, 'NS'),
            convert_dd_to_dms(center_waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PA', 'PC', 'PG', 'PN'])

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        #sql_columns['airport_icao_code'] = line[10:12]
        sql_columns['subsection_code'] = 'F'
        sql_columns['approach_identifier'] = '{0: <6s}'.format(procedure_identifier)
        sql_columns['route_type'] = '{0: <1s}'.format(route_type)
        sql_columns['transition_identifier'] = '{0: <5s}'.format(transition_identifier or '')
        # sql_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
        sql_columns['sequence_number'] = '{0:03d}'.format(seqno)
        sql_columns['fix_identifier'] = '{0: <5s}'.format(waypoint_identifier or '')
        sql_columns['fix_icao_code'] = '{0: <2s}'.format(waypoint_icao_code or '')
        #sql_columns['fix_icao_code'] = '{0: <2s}'.format(found_waypoint_icao_code or '')
        if waypoint_icao_code and found_waypoint_icao_code and (waypoint_icao_code != found_waypoint_icao_code.strip(' ')):
            print('waypoint icao codes differ for waypoint {0:s}: {1:s} != {2:s}'.format(waypoint_identifier, waypoint_icao_code, found_waypoint_icao_code))
        sql_columns['fix_section_code'] = '{0: <1s}'.format(found_waypoint_section_code or '')
        sql_columns['fix_subsection_code'] = '{0: <1s}'.format(found_waypoint_subsection_code or '')
        # Primary
        sql_columns['waypoint_description_code'] = '{0: <4s}'.format(waypoint_description_code or '')
        sql_columns['turn_direction'] = '{0: <1s}'.format(turn_direction or '')
        if rnp:
            sql_columns['rnp'] = '{0:03.0f}'.format(rnp * 10.0)
        else:
            sql_columns['rnp'] = '   '
        sql_columns['path_and_termination'] = '{0: <2s}'.format(path_termination)
        #sql_columns['turn_direction_valid'] = line[49:50]
        sql_columns['recommended_navaid_identifier'] = '{0: <4s}'.format(recommanded_navaid or '')
        sql_columns['recommended_navaid_icao_code'] = '{0: <2s}'.format(found_recommended_navaid_icao_code or '')
        if arc_radius:
            sql_columns['arc_radius'] = '{0:06.0f}'.format(arc_radius * 1000.0)
        else:
            sql_columns['arc_radius'] = '      '
        if theta:
            sql_columns['theta'] = '{0:04.0f}'.format(theta * 10.0)
        else:
            sql_columns['theta'] = '    '
        if rho:
            sql_columns['rho'] = '{0:04.0f}'.format(rho * 10.0)
        else:
            sql_columns['rho'] = '    '
        if magnetic_course:
            sql_columns['magnetic_course'] = '{0:04.0f}'.format(magnetic_course * 10.0)
        else:
            sql_columns['magnetic_course'] = '    '
        if route_distance_holding_distance_time:
            sql_columns['route_distance_holding_distance_or_time'] = '{0:04.0f}'.format(route_distance_holding_distance_time * 10.0)
        else:
            sql_columns['route_distance_holding_distance_or_time'] = '    '
        sql_columns['recommended_navaid_section_code'] = '{0: <1s}'.format(found_recommended_navaid_section_code or '')
        sql_columns['recommended_navaid_subsection_code'] = '{0: <1s}'.format(found_recommended_navaid_subsection_code or '')
        #sql_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_columns['altitude_description'] = '{0: <1s}'.format(altitude_description or '')
        #sql_columns['atc_indicator'] = line[83:84]
        if altitude1:
            sql_columns['altitude_1'] = '{0:05d}'.format(altitude1)
        else:
            sql_columns['altitude_1'] = '     '
        if altitude2:
            sql_columns['altitude_2'] = '{0:05d}'.format(altitude2)
        else:
            sql_columns['altitude_2'] = '     '
        if transition_altitude:
            sql_columns['transition_altitude'] = '{0:05d}'.format(transition_altitude)
        else:
            sql_columns['transition_altitude'] = '     '
        if speed_limit:
            sql_columns['speed_limit'] = '{0:03d}'.format(speed_limit)
        else:
            sql_columns['speed_limit'] = '     '
        if vertical_angle:
            sql_columns['vertical_angle'] = '{0:04.0f}'.format(vertical_angle * 100.0)
        else:
            sql_columns['vertical_angle'] = '    '
        sql_columns['center_fix_or_taa_procedure_turn_indicator'] = '{0: <5s}'.format(center_waypoint or '')
        #sql_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_columns['taa_icao_code'] = '{0: <2s}'.format(found_center_waypoint_icao_code or '') # TODO check if taa == center waypoint
        sql_columns['taa_section_code'] = '{0: <1s}'.format(found_center_waypoint_section_code or '') # TODO check if taa == center waypoint
        sql_columns['taa_subsection_code'] = '{0: <1s}'.format(found_center_waypoint_subsection_code or '') # TODO check if taa == center waypoint
        #sql_columns['gnss_fms_indication'] = line[116:117]
        sql_columns['speed_limit_description'] = '{0: <1s}'.format(speed_limit_description or '')
        #sql_columns['route_qualifier_1'] = line[118:119]
        #sql_columns['route_qualifier_2'] = line[119:120]
        #sql_columns['route_qualifier_3'] = line[120:121]
        #sql_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_approach_procedures',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_localizer_marker into airport_localizer_markers
def import_tbl_localizer_marker(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airport_identifier, runway_identifier, llz_identifier, marker_type, marker_latitude, marker_longitude ' \
        'FROM tbl_localizer_marker'
    for (area_code, icao_code, airport_identifier, runway_identifier, llz_identifier, marker_type, marker_latitude, marker_longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'M'
        sql_columns['localizer_identifier'] = '{0: <4s}'.format(llz_identifier)
        sql_columns['marker_type'] = '{0: <3s}'.format(marker_type)
        # = line[20:21]
        # Primary
        #sql_columns['locator_frequency'] = line[22:27]
        sql_columns['runway_helipad_identifier'] = '{0: <5s}'.format(runway_identifier)
        sql_columns['marker_latitude'] = convert_dd_to_dms(marker_latitude, 'NS')
        sql_columns['marker_longitude'] = convert_dd_to_dms(marker_longitude, 'EW')
        #sql_columns['minor_axis_bearing'] = line[51:55]
        #sql_columns['locator_latitude'] = line[55:64]
        #sql_columns['locator_longitude'] = line[64:74]
        #sql_columns['locator_class'] = line[74:79]
        #sql_columns['locator_facility_characteristics'] = line[79:84]
        #sql_columns['locator_identifier'] = line[84:88]
        # = line[88:90]
        #sql_columns['magnetic_variation'] = line[90:95]
        # = line[95:97]
        #sql_columns['facility_elevation'] = line[97:102]
        # = line[102:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_localizer_markers',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_localizers_glideslopes into airport_localizers_and_glideslopes
def import_tbl_localizers_glideslopes(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airport_identifier, runway_identifier, llz_identifier, llz_latitude, llz_longitude, llz_frequency, llz_bearing, ils_mls_gls_category, gs_latitude, gs_longitude, gs_angle, gs_elevation, station_declination ' \
        'FROM tbl_localizers_glideslopes'
    for (area_code, icao_code, airport_identifier, runway_identifier, llz_identifier, llz_latitude, llz_longitude, llz_frequency, llz_bearing, ils_mls_gls_category, gs_latitude, gs_longitude, gs_angle, gs_elevation, station_declination) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier_or_heliport'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'I'
        sql_columns['localizer_identifier'] = '{0: <4s}'.format(llz_identifier)
        sql_columns['ils_category'] = '{0: <1s}'.format(ils_mls_gls_category)
        # = line[18:21]
        # Primary
        sql_columns['localizer_frequency'] = '{0:05.0f}'.format(llz_frequency * 100.0)
        sql_columns['runway_identifier'] = '{0: <5s}'.format(runway_identifier)
        sql_columns['localizer_latitude'] = convert_dd_to_dms(llz_latitude, 'NS')
        sql_columns['localizer_longitude'] = convert_dd_to_dms(llz_longitude, 'EW')
        sql_columns['localizer_bearing'] = '{0:04.0f}'.format(llz_bearing * 10.0)
        sql_columns['glideslope_latitude'] = convert_dd_to_dms(gs_latitude, 'NS')
        sql_columns['glideslope_longitude'] = convert_dd_to_dms(gs_longitude, 'EW')
        #sql_columns['localizer_position'] = line[74:78]
        #sql_columns['localizer_position_reference'] = line[78:79]
        #sql_columns['glideslope_position'] = line[79:83]
        #sql_columns['localizer_width'] = line[83:87]
        if gs_angle:
            sql_columns['glideslope_angle'] = '{0:03.0f}'.format(gs_angle * 100.0)
        else:
            sql_columns['glideslope_angle'] = '   '
        if station_declination < 0.0:
            sql_columns['station_declination'] = 'W{0:04.0f}'.format(-station_declination * 10.0)
        else:
            sql_columns['station_declination'] = 'E{0:04.0f}'.format(station_declination * 10.0)
        #sql_columns['glideslope_height_at_landing_threshold'] = line[95:97]
        if gs_elevation:
            sql_columns['glideslope_elevation'] = '{0:05d}'.format(gs_elevation)
        else:
            sql_columns['glideslope_elevation'] = '     '
        #sql_columns['supporting_facility_id'] = line[102:106]
        #sql_columns['supporting_facility_icao_code'] = line[106:108]
        #sql_columns['supporting_facility_section_code'] = line[108:109]
        #sql_columns['supporting_facility_subsection_code'] = line[109:110]
        #sql_columns['glideslope_height_at_landing_threshold'] = line[110:113]
        # = line[113:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_localizers_and_glideslopes',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_restrictive_airspace into airspace_restrictive_airspaces
def import_tbl_restrictive_airspace(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, restrictive_airspace_designation, restrictive_airspace_name, restrictive_type, multiple_code, seqno, boundary_via, flightlevel, latitude, longitude, arc_origin_latitude, arc_origin_longitude, arc_distance, arc_bearing, unit_indicator_lower_limit, lower_limit, unit_indicator_upper_limit, upper_limit ' \
        'FROM tbl_restrictive_airspace'
    for (area_code, icao_code, restrictive_airspace_designation, restrictive_airspace_name, restrictive_type, multiple_code, seqno, boundary_via, flightlevel, latitude, longitude, arc_origin_latitude, arc_origin_longitude, arc_distance, arc_bearing, unit_indicator_lower_limit, lower_limit, unit_indicator_upper_limit, upper_limit) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'U'
        sql_columns['subsection_code'] = 'R'
        sql_columns['restrictive_airspace_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['restrictive_type'] = '{0: <1s}'.format(restrictive_type)
        sql_columns['restrictive_airspace_designation'] = '{0: <10s}'.format(restrictive_airspace_designation)
        sql_columns['multiple_code'] = '{0: <1s}'.format(multiple_code or '')
        sql_columns['sequence_number'] = '{0:04d}'.format(seqno)
        # Primary
        sql_columns['level'] = '{0: <1s}'.format(flightlevel or '')
        #sql_columns['time_code'] = line[26:27]
        #sql_columns['notam'] = line[27:28]
        # = line[28:30]
        sql_columns['boundary_via'] = '{0: <2s}'.format(boundary_via)
        sql_columns['latitude'] = convert_dd_to_dms(latitude, 'NS')
        sql_columns['longitude'] = convert_dd_to_dms(longitude, 'EW')
        sql_columns['arc_origin_latitude'] = convert_dd_to_dms(arc_origin_latitude, 'NS')
        sql_columns['arc_origin_longitude'] = convert_dd_to_dms(arc_origin_longitude, 'EW')
        if arc_distance:
            sql_columns['arc_distance'] = '{0:04.0f}'.format(arc_distance * 10.0)
        else:
            sql_columns['arc_distance'] = '    '
        if arc_bearing:
            sql_columns['arc_bearing'] = '{0:04.0f}'.format(arc_bearing * 10.0)
        else:
            sql_columns['arc_bearing'] = '    '
        # = line[78:81]
        sql_columns['lower_limit'] = '{0: <5s}'.format(lower_limit or '')
        sql_columns['lower_limit_unit_indicator'] = '{0: <1s}'.format(unit_indicator_lower_limit or '')
        sql_columns['upper_limit'] = '{0: <5s}'.format(upper_limit or '')
        sql_columns['upper_limit_unit_indicator'] = '{0: <1s}'.format(unit_indicator_upper_limit or '')
        sql_columns['restrictive_airspace_name'] = '{0: <30s}'.format(restrictive_airspace_name or '')
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airspace_restrictive_airspaces',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_runways into airport_runways
def import_tbl_runways(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, icao_code, airport_identifier, runway_identifier, runway_latitude, runway_longitude, runway_gradient, runway_magnetic_bearing, runway_true_bearing, landing_threshold_elevation, displaced_threshold_distance, threshold_crossing_height, runway_length, runway_width, llz_identifier, llz_mls_gls_category ' \
        'FROM tbl_runways'
    for (area_code, icao_code, airport_identifier, runway_identifier, runway_latitude, runway_longitude, runway_gradient, runway_magnetic_bearing, runway_true_bearing, landing_threshold_elevation, displaced_threshold_distance, threshold_crossing_height, runway_length, runway_width, llz_identifier, llz_mls_gls_category) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        sql_columns['airport_icao_code'] = '{0: <2s}'.format(icao_code)
        sql_columns['subsection_code'] = 'G'
        sql_columns['runway_identifier'] = '{0: <5s}'.format(runway_identifier)
        # = line[18:21]
        # Primary
        sql_columns['runway_length'] = '{0:05d}'.format(runway_length)
        sql_columns['runway_magnetic_bearing'] = '{0:04.0f}'.format(runway_magnetic_bearing * 10.0)
        # = line[31:32]
        sql_columns['runway_latitude'] = convert_dd_to_dms(runway_latitude, 'NS')
        sql_columns['runway_longitude'] = convert_dd_to_dms(runway_longitude, 'EW')
        if runway_gradient:
            if runway_gradient > 9.0:
                sql_columns['runway_gradient'] = '+9000'
            elif runway_gradient < -9.0:
                sql_columns['runway_gradient'] = '-9000'
            else:
                sql_columns['runway_gradient'] = '{0:+04.0f}'.format(runway_gradient * 1000.0)
        # = line[56:60]
        #sql_columns['ltp_ellipsoid_height'] = line[60:66]
        sql_columns['landing_threshold_elevation'] = '{0:05d}'.format(landing_threshold_elevation)
        sql_columns['displaced_threshold_distance'] = '{0:04d}'.format(displaced_threshold_distance)
        #sql_columns['threshold_crossing_height_old'] = line[75:77]
        sql_columns['runway_width'] = '{0:3}'.format(runway_width)
        #sql_columns['tch_value_indicator'] = line[80:81]
        #sql_specific_columns['localizer_mls_gls_ref_path_identifier'] = line[81:85]
        #sql_specific_columns['localizer_mls_gls_category_class'] = line[85:86]
        # TODO missing llz_identifier llz_mls_gls_category
        #sql_columns['stopway'] = line[86:90]
        #sql_specific_columns['second_localizer_mls_gls_ref_path_identifier'] = line[90:94]
        #sql_specific_columns['second_localizer_mls_gls_category_class'] = line[94:95]
        sql_columns['threshold_crossing_height'] = '{0:03d}'.format(threshold_crossing_height)
        #sql_columns['runway_accuracy_compliance_flag'] = line[98:99]
        #sql_columns['landing_threshold_elevation_accuracy_compliance_flag'] = line[99:100]
        # = line[100:101]
        #sql_columns['runway_description'] = line[101:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        # Continuation
        # Simulation Continuation
        sql_columns['s_runway_true_bearing'] = '{0:05.0f}'.format(runway_true_bearing * 100.0)
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_runways',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_sids into airport_sids
def import_tbl_sids(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, procedure_identifier, route_type, transition_identifier, seqno, waypoint_icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, turn_direction, rnp, path_termination, recommanded_navaid, recommanded_navaid_latitude, recommanded_navaid_longitude, arc_radius, theta, rho, magnetic_course, route_distance_holding_distance_time, altitude_description, altitude1, altitude2, transition_altitude, speed_limit_description, speed_limit, vertical_angle, center_waypoint, center_waypoint_latitude, center_waypoint_longitude ' \
        'FROM tbl_sids'
    for (area_code, airport_identifier, procedure_identifier, route_type, transition_identifier, seqno, waypoint_icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, turn_direction, rnp, path_termination, recommanded_navaid, recommanded_navaid_latitude, recommanded_navaid_longitude, arc_radius, theta, rho, magnetic_course, route_distance_holding_distance_time, altitude_description, altitude1, altitude2, transition_altitude, speed_limit_description, speed_limit, vertical_angle, center_waypoint, center_waypoint_latitude, center_waypoint_longitude) in sql_cursor_in.execute(sql_command):
        (found_waypoint_icao_code, found_waypoint_section_code, found_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            waypoint_identifier,
            convert_dd_to_dms(waypoint_latitude, 'NS'),
            convert_dd_to_dms(waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PC', 'PG', 'PN'],
            icao_code=waypoint_icao_code)
        (found_recommended_navaid_icao_code, found_recommended_navaid_section_code, found_recommended_navaid_subsection_code) = search_navaid(
            sql_cursor_out,
            recommanded_navaid,
            convert_dd_to_dms(recommanded_navaid_latitude, 'NS'),
            convert_dd_to_dms(recommanded_navaid_longitude, 'EW'),
            ['D ', 'DB'])
        (found_center_waypoint_icao_code, found_center_waypoint_section_code, found_center_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            center_waypoint,
            convert_dd_to_dms(center_waypoint_latitude, 'NS'),
            convert_dd_to_dms(center_waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PA', 'PC', 'PG', 'PN'])

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        #sql_columns['airport_icao_code'] = line[10:12]
        sql_columns['subsection_code'] = 'D'
        sql_columns['sid_identifier'] = '{0: <6s}'.format(procedure_identifier)
        sql_columns['route_type'] = '{0: <1s}'.format(route_type)
        sql_columns['transition_identifier'] = '{0: <5s}'.format(transition_identifier or '')
        #sql_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
        sql_columns['sequence_number'] = '{0:03d}'.format(seqno)
        sql_columns['fix_identifier'] = '{0: <5s}'.format(waypoint_identifier or '')
        sql_columns['fix_icao_code'] = '{0: <2s}'.format(waypoint_icao_code or '')
        #sql_columns['fix_icao_code'] = '{0: <2s}'.format(found_waypoint_icao_code or '')
        if waypoint_icao_code and found_waypoint_icao_code and (waypoint_icao_code != found_waypoint_icao_code.strip(' ')):
            print('waypoint icao codes differ for waypoint {0:s}: {1:s} != {2:s}'.format(waypoint_identifier, waypoint_icao_code, found_waypoint_icao_code))
        sql_columns['fix_section_code'] = '{0: <1s}'.format(found_waypoint_section_code or '')
        sql_columns['fix_subsection_code'] = '{0: <1s}'.format(found_waypoint_subsection_code or '')
        # Primary
        sql_columns['waypoint_description_code'] = '{0: <4s}'.format(waypoint_description_code or '')
        sql_columns['turn_direction'] = '{0: <1s}'.format(turn_direction or '')
        if rnp:
            sql_columns['rnp'] = '{0:03.0f}'.format(rnp * 10.0)
        else:
            sql_columns['rnp'] = '   '
        sql_columns['path_and_termination'] = '{0: <2s}'.format(path_termination)
        #sql_columns['turn_direction_valid'] = line[49:50]
        sql_columns['recommended_navaid_identifier'] = '{0: <4s}'.format(recommanded_navaid or '')
        sql_columns['recommended_navaid_icao_code'] = '{0: <2s}'.format(found_recommended_navaid_icao_code or '')
        if arc_radius:
            sql_columns['arc_radius'] = '{0:06.0f}'.format(arc_radius * 1000.0)
        else:
            sql_columns['arc_radius'] = '      '
        if theta:
            sql_columns['theta'] = '{0:04.0f}'.format(theta * 10.0)
        else:
            sql_columns['theta'] = '    '
        if rho:
            sql_columns['rho'] = '{0:04.0f}'.format(rho * 10.0)
        else:
            sql_columns['rho'] = '    '
        if magnetic_course:
            sql_columns['magnetic_course'] = '{0:04.0f}'.format(magnetic_course * 10.0)
        else:
            sql_columns['magnetic_course'] = '    '
        if route_distance_holding_distance_time:
            sql_columns['route_distance_holding_distance_or_time'] = '{0:04.0f}'.format(route_distance_holding_distance_time * 10.0)
        else:
            sql_columns['route_distance_holding_distance_or_time'] = '    '
        sql_columns['recommended_navaid_section_code'] = '{0: <1s}'.format(found_recommended_navaid_section_code or '')
        sql_columns['recommended_navaid_subsection_code'] = '{0: <1s}'.format(found_recommended_navaid_subsection_code or '')
        #sql_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_columns['altitude_description'] = '{0: <1s}'.format(altitude_description or '')
        #sql_columns['atc_indicator'] = line[83:84]
        if altitude1:
            sql_columns['altitude_1'] = '{0:05d}'.format(altitude1)
        else:
            sql_columns['altitude_1'] = '     '
        if altitude2:
            sql_columns['altitude_2'] = '{0:05d}'.format(altitude2)
        else:
            sql_columns['altitude_2'] = '     '
        if transition_altitude:
            sql_columns['transition_altitude'] = '{0:05d}'.format(transition_altitude)
        else:
            sql_columns['transition_altitude'] = '     '
        if speed_limit:
            sql_columns['speed_limit'] = '{0:03d}'.format(speed_limit)
        else:
            sql_columns['speed_limit'] = '   '
        if vertical_angle:
            sql_columns['vertical_angle'] = '{0:04.0f}'.format(vertical_angle * 100.0)
        else:
            sql_columns['vertical_angle'] = '    '
        sql_columns['center_fix_or_taa_procedure_turn_indicator'] = '{0: <5s}'.format(center_waypoint or '')
        #sql_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_columns['taa_icao_code'] = '{0: <2s}'.format(found_center_waypoint_icao_code or '') # TODO check if taa == center waypoint
        sql_columns['taa_section_code'] = '{0: <1s}'.format(found_center_waypoint_section_code or '') # TODO check if taa == center waypoint
        sql_columns['taa_subsection_code'] = '{0: <1s}'.format(found_center_waypoint_subsection_code or '') # TODO check if taa == center waypoint
        #sql_columns['gnss_fms_indication'] = line[116:117]
        sql_columns['speed_limit_description'] = '{0: <1s}'.format(speed_limit_description or '')
        #sql_columns['route_qualifier_1'] = line[118:119]
        #sql_columns['route_qualifier_2'] = line[119:120]
        #sql_columns['route_qualifier_3'] = line[120:123]
        #sql_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_sids',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_stars into airport_stars
def import_tbl_stars(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, procedure_identifier, route_type, transition_identifier, seqno, waypoint_icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, turn_direction, rnp, path_termination, recommanded_navaid, recommanded_navaid_latitude, recommanded_navaid_longitude, arc_radius, theta, rho, magnetic_course, route_distance_holding_distance_time, altitude_description, altitude1, altitude2, transition_altitude, speed_limit_description, speed_limit, vertical_angle, center_waypoint, center_waypoint_latitude, center_waypoint_longitude ' \
        'FROM tbl_stars'
    for (area_code, airport_identifier, procedure_identifier, route_type, transition_identifier, seqno, waypoint_icao_code, waypoint_identifier, waypoint_latitude, waypoint_longitude, waypoint_description_code, turn_direction, rnp, path_termination, recommanded_navaid, recommanded_navaid_latitude, recommanded_navaid_longitude, arc_radius, theta, rho, magnetic_course, route_distance_holding_distance_time, altitude_description, altitude1, altitude2, transition_altitude, speed_limit_description, speed_limit, vertical_angle, center_waypoint, center_waypoint_latitude, center_waypoint_longitude) in sql_cursor_in.execute(sql_command):
        (found_waypoint_icao_code, found_waypoint_section_code, found_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            waypoint_identifier,
            convert_dd_to_dms(waypoint_latitude, 'NS'),
            convert_dd_to_dms(waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PA', 'PC', 'PN'],
            icao_code=waypoint_icao_code)
        (found_recommended_navaid_icao_code, found_recommended_navaid_section_code, found_recommended_navaid_subsection_code) = search_navaid(
            sql_cursor_out,
            recommanded_navaid,
            convert_dd_to_dms(recommanded_navaid_latitude, 'NS'),
            convert_dd_to_dms(recommanded_navaid_longitude, 'EW'),
            ['D '])
        (found_center_waypoint_icao_code, found_center_waypoint_section_code, found_center_waypoint_subsection_code) = search_navaid(
            sql_cursor_out,
            center_waypoint,
            convert_dd_to_dms(center_waypoint_latitude, 'NS'),
            convert_dd_to_dms(center_waypoint_longitude, 'EW'),
            ['D ', 'DB', 'EA', 'PA', 'PC', 'PG', 'PN'])

        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        #sql_columns['airport_icao_code'] = line[10:12]
        sql_columns['subsection_code'] = 'E'
        sql_columns['star_identifier'] = '{0: <6s}'.format(procedure_identifier)
        sql_columns['route_type'] = '{0: <1s}'.format(route_type)
        sql_columns['transition_identifier'] = '{0: <5s}'.format(transition_identifier or '')
        #sql_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
        sql_columns['sequence_number'] = '{0:03d}'.format(seqno)
        sql_columns['fix_identifier'] = '{0: <5s}'.format(waypoint_identifier or '')
        sql_columns['fix_icao_code'] = '{0: <2s}'.format(waypoint_icao_code or '')
        #sql_columns['fix_icao_code'] = '{0: <2s}'.format(found_waypoint_icao_code or '')
        if waypoint_icao_code and found_waypoint_icao_code and (waypoint_icao_code != found_waypoint_icao_code.strip(' ')):
            print('waypoint icao codes differ for waypoint {0:s}: {1:s} != {2:s}'.format(waypoint_identifier, waypoint_icao_code, found_waypoint_icao_code))
        sql_columns['fix_section_code'] = '{0: <1s}'.format(found_waypoint_section_code or '')
        sql_columns['fix_subsection_code'] = '{0: <1s}'.format(found_waypoint_subsection_code or '')
        # Primary
        sql_columns['waypoint_description_code'] = '{0: <4s}'.format(waypoint_description_code or '')
        sql_columns['turn_direction'] = '{0: <1s}'.format(turn_direction or '')
        if rnp:
            sql_columns['rnp'] = '{0:03.0f}'.format(rnp * 10.0)
        else:
            sql_columns['rnp'] = '   '
        sql_columns['path_and_termination'] = '{0: <2s}'.format(path_termination or '')
        #sql_columns['turn_direction_valid'] = line[49:50]
        sql_columns['recommended_navaid_identifier'] = '{0: <4s}'.format(recommanded_navaid or '')
        sql_columns['recommended_navaid_icao_code'] = '{0: <2s}'.format(found_recommended_navaid_icao_code or '')
        if arc_radius:
            sql_columns['arc_radius'] = '{0:06.0f}'.format(arc_radius * 1000.0)
        else:
            sql_columns['arc_radius'] = '      '
        if theta:
            sql_columns['theta'] = '{0:04.0f}'.format(theta * 10.0)
        else:
            sql_columns['theta'] = '    '
        if rho:
            sql_columns['rho'] = '{0:04.0f}'.format(rho * 10.0)
        else:
            sql_columns['rho'] = '    '
        if magnetic_course:
            sql_columns['magnetic_course'] = '{0:04.0f}'.format(magnetic_course * 10.0)
        else:
            sql_columns['magnetic_course'] = '    '
        if route_distance_holding_distance_time:
            sql_columns['route_distance_holding_distance_or_time'] = '{0:04.0f}'.format(route_distance_holding_distance_time * 10.0)
        else:
            sql_columns['route_distance_holding_distance_or_time'] = '    '
        sql_columns['recommended_navaid_section_code'] = '{0: <1s}'.format(found_recommended_navaid_section_code or '')
        sql_columns['recommended_navaid_subsection_code'] = '{0: <1s}'.format(found_recommended_navaid_subsection_code or '')
        #sql_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:32]
        sql_columns['altitude_description'] = '{0: <1s}'.format(altitude_description or '')
        #sql_columns['atc_indicator'] = line[83:84]
        if altitude1:
            sql_columns['altitude_1'] = '{0:05d}'.format(altitude1)
        else:
            sql_columns['altitude_1'] = '     '
        if altitude2:
            sql_columns['altitude_2'] = '{0:05d}'.format(altitude2)
        else:
            sql_columns['altitude_2'] = '     '
        if transition_altitude:
            sql_columns['transition_altitude'] = '{0:05d}'.format(transition_altitude)
        else:
            sql_columns['transition_altitude'] = '     '
        if speed_limit:
            sql_columns['speed_limit'] = '{0:03d}'.format(speed_limit)
        else:
            sql_columns['speed_limit'] = '   '
        if vertical_angle:
            sql_columns['vertical_angle'] = '{0:04.0f}'.format(vertical_angle * 100.0)
        else:
            sql_columns['vertical_angle'] = '    '
        sql_columns['center_fix_or_taa_procedure_turn_indicator'] = '{0: <5s}'.format(center_waypoint or '')
        #sql_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_columns['taa_icao_code'] = '{0: <2s}'.format(found_center_waypoint_icao_code or '') # TODO check if taa == center waypoint
        sql_columns['taa_section_code'] = '{0: <1s}'.format(found_center_waypoint_section_code or '') # TODO check if taa == center waypoint
        sql_columns['taa_subsection_code'] = '{0: <1s}'.format(found_center_waypoint_subsection_code or '') # TODO check if taa == center waypoint
        #sql_columns['gnss_fms_indication'] = line[116:117]
        sql_columns['speed_limit_description'] = '{0: <1s}'.format(speed_limit_description or '')
        #sql_columns['route_qualifier_1'] = line[118:119]
        #sql_columns['route_qualifier_2'] = line[119:120]
        #sql_columns['route_qualifier_3'] = line[120:121]
        #sql_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_stars',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_terminal_ndbnavaids into airport_terminal_ndb_navaids
def import_tbl_terminal_ndbnavaids(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, icao_code, ndb_identifier, ndb_name, ndb_frequency, navaid_class, ndb_latitude, ndb_longitude ' \
        'FROM tbl_terminal_ndbnavaids'
    for (area_code, airport_identifier, icao_code, ndb_identifier, ndb_name, ndb_frequency, navaid_class, ndb_latitude, ndb_longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        sql_columns['subsection_code'] = 'N'
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier)
        #sql_columns['airport_icao_code'] = '{0: <2s}'.format()
        # = line[12:13]
        sql_columns['ndb_identifier'] = '{0: <4s}'.format(ndb_identifier)
        # = line[17:19]
        sql_columns['ndb_icao_code'] = '{0: <2s}'.format(icao_code)
        # Primary
        sql_columns['ndb_frequency'] = '{0:05.0f}'.format(ndb_frequency * 10.0)
        sql_columns['ndb_class'] = '{0: <5s}'.format(navaid_class)
        sql_columns['ndb_latitude'] = convert_dd_to_dms(ndb_latitude, 'NS')
        sql_columns['ndb_longitude'] = convert_dd_to_dms(ndb_longitude, 'EW')
        # = line[51:74]
        #sql_columns['magnetic_variation'] = line[74:79]
        # = line[79:85]
        # = line[85:90]
        #sql_columns['datum_code'] = line[90:93]
        sql_columns['ndb_name'] = '{0: <30s}'.format(ndb_name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_terminal_ndb_navaids',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_terminal_waypoints into airport_terminal_waypoints
def import_tbl_terminal_waypoints(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, region_code, icao_code, waypoint_identifier, waypoint_name, waypoint_type, waypoint_latitude, waypoint_longitude ' \
        'FROM tbl_terminal_waypoints'
    for (area_code, region_code, icao_code, waypoint_identifier, waypoint_name, waypoint_type, waypoint_latitude, waypoint_longitude) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'P'
        # = line[5:6]
        sql_columns['region_code'] = '{0: <4s}'.format(region_code)
        # TODO sql_columns['region_icao_code'] = '{0: <2s}'.format()
        sql_columns['subsection_code'] = 'C'
        sql_columns['waypoint_identifier'] = '{0: <5s}'.format(waypoint_identifier)
        # = line[18:19]
        sql_columns['waypoint_icao_code'] = '{0: <2s}'.format(icao_code)
        # Primary
        # = line[22:26]
        sql_columns['waypoint_type'] = '{0: <3s}'.format(waypoint_type)
        # = line[29:30]
        #sql_columns['waypoint_usage'] = line[29:31]
        # = line[31:32]
        sql_columns['waypoint_latitude'] = convert_dd_to_dms(waypoint_latitude, 'NS')
        sql_columns['waypoint_longitude'] = convert_dd_to_dms(waypoint_longitude, 'EW')
        # = line[51:74]
        #sql_columns['dynamic_magnetic_variation'] = line[74:79]
        # = line[79:84]
        #sql_columns['datum_code'] = line[84:87]
        # = line[87:95]
        #sql_columns['name_format_indicator'] = line[95:98]
        sql_columns['waypoint_name_description'] = '{0: <25s}'.format(waypoint_name)
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'airport_terminal_waypoints',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import tbl_vhfnavaids into navaid_vhf_navaids
def import_tbl_vhfnavaids(sql_cursor_out, sql_cursor_in):
    sql_command = \
        'SELECT area_code, airport_identifier, icao_code, vor_identifier, vor_name, vor_frequency, navaid_class, vor_latitude, vor_longitude, dme_ident, dme_latitude, dme_longitude, dme_elevation, ilsdme_bias, range, station_declination ' \
        'FROM tbl_vhfnavaids'
    for (area_code, airport_identifier, icao_code, vor_identifier, vor_name, vor_frequency, navaid_class, vor_latitude, vor_longitude, dme_ident, dme_latitude, dme_longitude, dme_elevation, ilsdme_bias, range, station_declination) in sql_cursor_in.execute(sql_command):
        sql_columns = {}
        # Common
        sql_columns['record_type'] = 'S'
        sql_columns['customer_area_code'] = '{0: <3s}'.format(area_code)
        sql_columns['section_code'] = 'D'
        sql_columns['subsection_code'] = ' '
        sql_columns['airport_identifier'] = '{0: <4s}'.format(airport_identifier or '')
        #sql_columns['airport_icao_code'] = '{0: <2s}'.format()
        # = line[12:13]
        sql_columns['vor_identifier'] = '{0: <4s}'.format(vor_identifier)
        # = line[17:19]
        sql_columns['vor_icao_code'] = '{0: <2s}'.format(icao_code)
        # Primary
        sql_columns['vor_frequency'] = '{0:05.0f}'.format(vor_frequency * 100.0)
        sql_columns['navaid_class'] = '{0: <5s}'.format(navaid_class)
        sql_columns['vor_latitude'] = convert_dd_to_dms(vor_latitude, 'NS')
        sql_columns['vor_longitude'] = convert_dd_to_dms(vor_longitude, 'EW')
        sql_columns['dme_identifier'] = '{0: <4s}'.format(dme_ident or '')
        sql_columns['dme_latitude'] = convert_dd_to_dms(dme_latitude, 'NS')
        sql_columns['dme_longitude'] = convert_dd_to_dms(dme_longitude, 'EW')
        if station_declination < 0.0:
            sql_columns['station_declination'] = 'W{0:04.0f}'.format(-station_declination * 10.0)
        else:
            sql_columns['station_declination'] = 'E{0:04.0f}'.format(station_declination * 10.0)
        sql_columns['dme_elevation'] = '{0:05d}'.format(dme_elevation)
        sql_columns['figure_of_merit'] = convert_range(range)
        if ilsdme_bias:
            sql_columns['ils_dme_bias'] = '{0:02.0f}'.format(ilsdme_bias * 10.0)
        else:
            sql_columns['ils_dme_bias'] = '  '
        #sql_columns['frequency_protection'] = line[87:90]
        #sql_columns['datum_code'] = line[90:93]
        sql_columns['vor_name'] = '{0: <30s}'.format(vor_name)
        #sql_columns['vfr_checkpoint_flag'] = line[118:119]
        # = line[119:121]
        #sql_columns['route_inappropriate_dme'] = line[121:122]
        #sql_columns['dme_operational_service_volume'] = line[122:123]
        #sql_columns['file_record_number'] = line[123:128]
        #sql_columns['cycle_date'] = line[128:132]
        sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
            'navaid_vhf_navaids',
            ', '.join([str(key) for key in sql_columns.keys()]),
            ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
        sql_cursor_out.execute(sql_command)


# import navigraph db
def import_navigraph_db(sql_cursor_out, sql_cursor_in):
    print("Importing airport communication...")
    import_tbl_airport_communication(sql_cursor_out, sql_cursor_in)
    print("Importing airports...")
    import_tbl_airports(sql_cursor_out, sql_cursor_in)
    print("Importing controlled airspace...")
    import_tbl_controlled_airspace(sql_cursor_out, sql_cursor_in)
    print("Importing cruising tables...")
    import_tbl_cruising_tables(sql_cursor_out, sql_cursor_in)
    print("Importing enroute communication...")
    import_tbl_enroute_communication(sql_cursor_out, sql_cursor_in)
    print("Importing enroute ndbnavaids...")
    import_tbl_enroute_ndbnavaids(sql_cursor_out, sql_cursor_in)
    print("Importing enroute waypoints...")
    import_tbl_enroute_waypoints(sql_cursor_out, sql_cursor_in)
    print("Importing fir uir...")
    import_tbl_fir_uir(sql_cursor_out, sql_cursor_in)
    print("Importing gate...")
    import_tbl_gate(sql_cursor_out, sql_cursor_in)
    print("Importing gls...")
    import_tbl_gls(sql_cursor_out, sql_cursor_in)
    print("Importing grid mora...")
    import_tbl_grid_mora(sql_cursor_out, sql_cursor_in)
    print("Importing header...")
    # TODO import_tbl_header(sql_cursor_out, sql_cursor_in)
    print("Importing localizer marker...")
    import_tbl_localizer_marker(sql_cursor_out, sql_cursor_in)
    print("Importing localizers glideslopes...")
    import_tbl_localizers_glideslopes(sql_cursor_out, sql_cursor_in)
    print("Importing restrictive airspace...")
    import_tbl_restrictive_airspace(sql_cursor_out, sql_cursor_in)
    print("Importing runways...")
    import_tbl_runways(sql_cursor_out, sql_cursor_in)
    print("Importing terminal ndbnavaids...")
    import_tbl_terminal_ndbnavaids(sql_cursor_out, sql_cursor_in)
    print("Importing terminal waypoints...")
    import_tbl_terminal_waypoints(sql_cursor_out, sql_cursor_in)
    print("Importing vhfnavaids...")
    import_tbl_vhfnavaids(sql_cursor_out, sql_cursor_in)

    # following require navaids
    print("Importing airport msa...")
    import_tbl_airport_msa(sql_cursor_out, sql_cursor_in)
    print("Importing enroute airway restrictions...")
    import_tbl_enroute_airway_restriction(sql_cursor_out, sql_cursor_in)
    print("Importing enroute airways...")
    import_tbl_enroute_airways(sql_cursor_out, sql_cursor_in)
    print("Importing holdings...")
    import_tbl_holdings(sql_cursor_out, sql_cursor_in)
    print("Importing iaps...")
    import_tbl_iaps(sql_cursor_out, sql_cursor_in)
    print("Importing sids...")
    import_tbl_sids(sql_cursor_out, sql_cursor_in)
    print("Importing stars...")
    import_tbl_stars(sql_cursor_out, sql_cursor_in)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Navigraph database file', default='e_dfd_1801.s3db')
    parser.add_argument('-o', '--output', help='Navigation database file', default='navdata.sqlite3')
    args = parser.parse_args()

    # open database connections
    sql_connection_out = sqlite3.connect(args.output)
    sql_connection_in = sqlite3.connect(args.input)
    sql_cursor_out = sql_connection_out.cursor()
    sql_cursor_in = sql_connection_in.cursor()

    # import data
    import_navigraph_db(sql_cursor_out, sql_cursor_in)

    # close database connections
    sql_connection_out.commit()
    sql_connection_in.commit()
    sql_connection_out.close()
    sql_connection_in.close()


if __name__ == '__main__':
    # execute only if run as a script
    main()
