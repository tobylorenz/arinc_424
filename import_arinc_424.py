#!/usr/bin/python3

import sqlite3


# primary record
def sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns):
    sql_columns = sql_common_columns | sql_specific_columns
    sql_command = 'INSERT OR REPLACE INTO {0} ({1}) VALUES ({2});'.format(
        sql_table,
        ', '.join([str(key) for key in sql_columns.keys()]),
        ', '.join(['\'' + str(value).replace('\'', '\'\'') + '\'' for value in sql_columns.values()]))
    sql_cursor.execute(sql_command)


# continuation records
def sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns):
    sql_command = 'UPDATE {0} SET {1} WHERE {2};'.format(
        sql_table,
        ', '.join([str(key) + ' = \'' + str(value).replace('\'', '\'\'') + '\'' for key, value in sql_specific_columns.items()]),
        ' AND '.join([str(key) + ' = \'' + str(value).replace('\'', '\'\'') + '\'' for key, value in sql_common_columns.items()]))
    sql_cursor.execute(sql_command)


# Navaid (D) - VHF NAVAID ( )
def import_arinc_424_d_(sql_cursor, line):
    sql_table = 'navaid_vhf_navaids'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    # = line[12:13]
    sql_common_columns['vor_identifier'] = line[13:17]
    # = line[17:19]
    sql_common_columns['vor_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['vor_frequency'] = line[22:27]
        sql_specific_columns['navaid_class'] = line[27:32]
        sql_specific_columns['vor_latitude'] = line[32:41]
        sql_specific_columns['vor_longitude'] = line[41:51]
        sql_specific_columns['dme_identifier'] = line[51:55]
        sql_specific_columns['dme_latitude'] = line[55:64]
        sql_specific_columns['dme_longitude'] = line[64:74]
        sql_specific_columns['station_declination'] = line[74:79]
        sql_specific_columns['dme_elevation'] = line[79:84]
        sql_specific_columns['figure_of_merit'] = line[84:85]
        sql_specific_columns['ils_dme_bias'] = line[85:87]
        sql_specific_columns['frequency_protection'] = line[87:90]
        sql_specific_columns['datum_code'] = line[90:93]
        sql_specific_columns['vor_name'] = line[93:118]
        sql_specific_columns['vfr_checkpoint_flag'] = line[118:119]
        # = line[119:121]
        sql_specific_columns['route_inappropriate_dme'] = line[121:122]
        sql_specific_columns['dme_operational_service_volume'] = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'S':
            # = line[23:27]
            sql_specific_columns['s_facility_characteristics'] = line[27:32]
            # = linee[32:74]
            sql_specific_columns['s_magnetic_variation'] = line[74:79]
            sql_specific_columns['s_facility_elevation'] = line[79:84]
            # = line[84:123]
            sql_specific_columns['s_file_record_number'] = line[123:128]
            sql_specific_columns['s_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            # = line[43:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'L':
            sql_specific_columns['l_navaid_limitation_code'] = line[23:24]
            sql_specific_columns['l_component_affected_indicator'] = line[24:25]
            sql_specific_columns['l_sequence_number'] = line[25:27]
            sql_specific_columns['l_sector_from_sector_to_1'] = line[27:29]
            sql_specific_columns['l_distance_description_1'] = line[29:30]
            sql_specific_columns['l_distance_limitation_1'] = line[30:36]
            sql_specific_columns['l_altitude_description_1'] = line[36:37]
            sql_specific_columns['l_altitude_limitation_1'] = line[37:43]
            sql_specific_columns['l_sector_from_sector_to_2'] = line[43:45]
            sql_specific_columns['l_distance_description_2'] = line[45:46]
            sql_specific_columns['l_distance_limitation_2'] = line[46:52]
            sql_specific_columns['l_altitude_description_2'] = line[52:53]
            sql_specific_columns['l_altitude_limitation_2'] = line[53:59]
            sql_specific_columns['l_sector_from_sector_to_3'] = line[59:61]
            sql_specific_columns['l_distance_description_3'] = line[61:62]
            sql_specific_columns['l_distance_limitation_3'] = line[62:68]
            sql_specific_columns['l_altitude_description_3'] = line[68:69]
            sql_specific_columns['l_altitude_limitation_3'] = line[69:75]
            sql_specific_columns['l_sector_from_sector_to_4'] = line[75:77]
            sql_specific_columns['l_distance_description_4'] = line[77:78]
            sql_specific_columns['l_distance_limitation_4'] = line[78:84]
            sql_specific_columns['l_altitude_description_4'] = line[84:85]
            sql_specific_columns['l_altitude_limitation_4'] = line[85:91]
            sql_specific_columns['l_sector_from_sector_to_5'] = line[91:93]
            sql_specific_columns['l_distance_description_5'] = line[93:94]
            sql_specific_columns['l_distance_limitation_5'] = line[94:100]
            sql_specific_columns['l_altitude_description_5'] = line[100:101]
            sql_specific_columns['l_altitude_limitation_5'] = line[101:107]
            sql_specific_columns['l_sequence_end_indicator'] = line[107:108]
            # = line[108:123]
            sql_specific_columns['l_file_record_number'] = line[123:128]
            sql_specific_columns['l_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Navaid (D) - NDB NAVAID (B)
def import_arinc_424_db(sql_cursor, line):
    sql_table = 'navaid_ndb_navaids'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    # = line[12:13]
    sql_common_columns['ndb_identifier'] = line[13:17]
    # = line[17:19]
    sql_common_columns['ndb_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['ndb_frequency'] = line[22:27]
        sql_specific_columns['ndb_class'] = line[27:32]
        sql_specific_columns['ndb_latitude'] = line[32:41]
        sql_specific_columns['ndb_longitude'] = line[41:51]
        # = line[51:74]
        sql_specific_columns['magnetic_variation'] = line[74:79]
        # = line[79:85]
        # = line[85:90]
        sql_specific_columns['datum_code'] = line[90:93]
        sql_specific_columns['ndb_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'S':
            # = line[23:27]
            sql_specific_columns['s_facility_characteristics'] = line[27:32]
            # = line[32:79]
            sql_specific_columns['s_facility_elevation'] = line[79:84]
            # = line[84:123]
            sql_specific_columns['s_file_record_number'] = line[123:128]
            sql_specific_columns['s_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            # = line[43:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Terminal NDB NAVAID (N)
def import_arinc_424_pn(sql_cursor, line):
    sql_table = 'airport_terminal_ndb_navaids'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    # = line[12:13]
    sql_common_columns['ndb_identifier'] = line[13:17]
    # = line[17:19]
    sql_common_columns['ndb_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['ndb_frequency'] = line[22:27]
        sql_specific_columns['ndb_class'] = line[27:32]
        sql_specific_columns['ndb_latitude'] = line[32:41]
        sql_specific_columns['ndb_longitude'] = line[41:51]
        # = line[51:74]
        sql_specific_columns['magnetic_variation'] = line[74:79]
        # = line[79:85]
        # = line[85:90]
        sql_specific_columns['datum_code'] = line[90:93]
        sql_specific_columns['ndb_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'S':
            # = line[23:27]
            sql_specific_columns['s_facility_characteristics'] = line[27:32]
            # = line[32:79]
            sql_specific_columns['s_facility_elevation'] = line[79:84]
            # = line[84:123]
            sql_specific_columns['s_file_record_number'] = line[123:128]
            sql_specific_columns['s_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            # = line[43:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Waypoint (A)
def import_arinc_424_ea(sql_cursor, line):
    sql_table = 'enroute_waypoints'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['region_code'] = line[6:10]
    sql_common_columns['region_icao_code'] = line[10:12]
    # = line[12:13]
    sql_common_columns['waypoint_identifier'] = line[13:18]
    # = line[18:19]
    sql_common_columns['waypoint_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[22:26]
        sql_specific_columns['waypoint_type'] = line[26:29]
        # = line[29:30]
        sql_specific_columns['waypoint_usage'] = line[30:31]
        # = line[31:32]
        sql_specific_columns['waypoint_latitude'] = line[32:41]
        sql_specific_columns['waypoint_longitude'] = line[41:51]
        # = line[51:74]
        sql_specific_columns['dynamic_magnetic_variation'] = line[74:79]
        # = line[79:84]
        sql_specific_columns['datum_code'] = line[84:87]
        # = line[87:95]
        sql_specific_columns['name_format_indicator'] = line[95:98]
        sql_specific_columns['waypoint_name_description'] = line[98:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            sql_specific_columns['p_fir_fra_entry_point'] = line[43:44]
            sql_specific_columns['p_fir_fra_exit_point'] = line[44:45]
            sql_specific_columns['p_fra_arrival_transition_point'] = line[45:46]
            sql_specific_columns['p_fra_departure_transition_point'] = line[46:47]
            sql_specific_columns['p_fra_intermediate_point'] = line[47:48]
            sql_specific_columns['p_fra_terminal_holding_point'] = line[48:49]
            # = line[49:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Terminal Waypoint (C)
def import_arinc_424_pc(sql_cursor, line):
    sql_table = 'airport_terminal_waypoints'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['region_code'] = line[6:10]
    sql_common_columns['region_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['waypoint_identifier'] = line[13:18]
    # = line[18:19]
    sql_common_columns['waypoint_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[22:26]
        sql_specific_columns['waypoint_type'] = line[26:29]
        # = line[29:30]
        sql_specific_columns['waypoint_usage'] = line[30:31]
        # = line[31:32]
        sql_specific_columns['waypoint_latitude'] = line[32:41]
        sql_specific_columns['waypoint_longitude'] = line[41:51]
        # = line[51:74]
        sql_specific_columns['dynamic_magnetic_variation'] = line[74:79]
        # = line[79:84]
        sql_specific_columns['datum_code'] = line[84:87]
        # = line[87:95]
        sql_specific_columns['name_format_indicator'] = line[95:98]
        sql_specific_columns['waypoint_name_description'] = line[98:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            sql_specific_columns['p_fir_fra_entry_point'] = line[43:44]
            sql_specific_columns['p_fir_fra_exit_point'] = line[44:45]
            sql_specific_columns['p_fra_arrival_transition_point'] = line[45:46]
            sql_specific_columns['p_fra_departure_transition_point'] = line[46:47]
            sql_specific_columns['p_fra_intermediate_point'] = line[47:48]
            sql_specific_columns['p_fra_terminal_holding_point'] = line[48:49]
            # = line[49:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Holding Pattern (P)
def import_arinc_424_ep(sql_cursor, line):
    sql_table = 'enroute_holding_patterns'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['region_code'] = line[6:10]
    sql_common_columns['region_icao_code'] = line[10:12]
    # = line[12:27]
    sql_common_columns['duplicate_identifier'] = line[27:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['inbound_holding_course'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['leg_length'] = line[44:47]
        sql_specific_columns['leg_time'] = line[47:49]
        sql_specific_columns['minimum_altitude'] = line[49:54]
        sql_specific_columns['maximum_altitude'] = line[54:59]
        sql_specific_columns['holding_speed'] = line[59:62]
        sql_specific_columns['rnp'] = line[62:65]
        sql_specific_columns['arc_radius'] = line[65:71]
        sql_specific_columns['vertical_scale_factor'] = line[71:74]
        sql_specific_columns['rvsm_minimum_level'] = line[74:77]
        sql_specific_columns['rvsm_maximum_level'] = line[77:80]
        sql_specific_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:98]
        sql_specific_columns['name'] = line[98:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[40:109]
            # = line[109:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Airways (R)
def import_arinc_424_er(sql_cursor, line):
    sql_table = 'enroute_airways_and_routes'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    # = line[6:13]
    sql_common_columns['route_identifier'] = line[13:18]
    # = line[18:19]
    # = line[19:25]
    sql_common_columns['sequence_number'] = line[25:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['boundary_code'] = line[43:44]
        sql_specific_columns['route_type'] = line[44:45]
        sql_specific_columns['level'] = line[45:46]
        sql_specific_columns['direction_restriction'] = line[46:47]
        sql_specific_columns['cruise_table_indicator'] = line[47:49]
        sql_specific_columns['eu_indicator'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['rnp'] = line[56:59]
        # = line[59:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['outbound_magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_from'] = line[74:78]
        sql_specific_columns['inbound_magnetic_course'] = line[78:82]
        # = line[82:83]
        sql_specific_columns['minimum_altitude_1'] = line[83:88]
        sql_specific_columns['minimum_altitude_2'] = line[88:93]
        sql_specific_columns['maximum_altitude'] = line[93:98]
        sql_specific_columns['fix_radius_transition_indicator'] = line[98:101]
        sql_specific_columns['vertical_scale_factor'] = line[101:104]
        sql_specific_columns['rvsm_minimum_level'] = line[104:107]
        sql_specific_columns['vsf_rvsm_maximum_level'] = line[107:110]
        # = line[110:114]
        # = line[114:120]
        sql_specific_columns['route_qualifier_1'] = line[120:121]
        sql_specific_columns['route_qualifier_2'] = line[121:122]
        sql_specific_columns['route_qualifier_3'] = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[40:109]
            # = line[109:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:66]
            sql_specific_columns['p_restricted_airspace_icao_code_1'] = line[66:68]
            sql_specific_columns['p_restricted_airspace_type_1'] = line[68:69]
            sql_specific_columns['p_restricted_airspace_designation_1'] = line[69:79]
            sql_specific_columns['p_restricted_airspace_multiple_code_1'] = line[79:80]
            sql_specific_columns['p_restricted_airspace_icao_code_2'] = line[80:82]
            sql_specific_columns['p_restricted_airspace_type_2'] = line[82:83]
            sql_specific_columns['p_restricted_airspace_designation_2'] = line[83:93]
            sql_specific_columns['p_restricted_airspace_multiple_code_2'] = line[93:94]
            sql_specific_columns['p_restricted_airspace_icao_code_3'] = line[94:96]
            sql_specific_columns['p_restricted_airspace_type_3'] = line[96:97]
            sql_specific_columns['p_restricted_airspace_designation_3'] = line[97:107]
            sql_specific_columns['p_restricted_airspace_multiple_code_3'] = line[107:108]
            sql_specific_columns['p_restricted_airspace_icao_code_4'] = line[108:110]
            sql_specific_columns['p_restricted_airspace_type_4'] = line[110:111]
            sql_specific_columns['p_restricted_airspace_designation_4'] = line[111:121]
            sql_specific_columns['p_restricted_airspace_multiple_code_4'] = line[121:122]
            sql_specific_columns['p_restricted_airspace_link_continuation'] = line[122:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Reference Point (A)
def import_arinc_424_pa(sql_cursor, line):
    sql_table = 'airport_reference_points'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['ata_iata_designator'] = line[13:16]
    # = line[16:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['speed_limit_altitude'] = line[22:27]
        sql_specific_columns['longest_runway'] = line[27:30]
        sql_specific_columns['ifr_capability'] = line[30:31]
        sql_specific_columns['longest_runway_surface_code'] = line[31:32]
        sql_specific_columns['airport_reference_point_latitude'] = line[32:41]
        sql_specific_columns['airport_reference_point_longitude'] = line[41:51]
        sql_specific_columns['magnetic_variation'] = line[51:56]
        sql_specific_columns['airport_elevation'] = line[56:61]
        sql_specific_columns['speed_limit'] = line[61:64]
        sql_specific_columns['recommended_navaid_identifier'] = line[64:68]
        sql_specific_columns['recommended_navaid_icao_code'] = line[68:70]
        sql_specific_columns['transition_altitude'] = line[70:75]
        sql_specific_columns['transition_level'] = line[75:80]
        sql_specific_columns['public_military_indicator'] = line[80:81]
        sql_specific_columns['time_zone'] = line[81:84]
        sql_specific_columns['daylight_indicator'] = line[84:85]
        sql_specific_columns['magnetic_true_indicator'] = line[85:86]
        sql_specific_columns['datum_code'] = line[86:89]
        # = line[89:93]
        sql_specific_columns['airport_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:66]
            sql_specific_columns['p_controlled_airspace_indicator'] = line[66:67]
            sql_specific_columns['p_controlled_airspace_airport_identifier'] = line[67:71]
            sql_specific_columns['p_controlled_airspace_airport_icao_code'] = line[71:73]
            # = line[73:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Gates (B)
def import_arinc_424_pb(sql_cursor, line):
    sql_table = 'airport_gates'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['gate_identifier'] = line[13:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[22:32]
        sql_specific_columns['gate_latitude'] = line[32:41]
        sql_specific_columns['gate_longitude'] = line[41:51]
        # = line[51:98]
        sql_specific_columns['name'] = line[98:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - SIDs (D)
def import_arinc_424_pd(sql_cursor, line):
    sql_table = 'airport_sids'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['sid_identifier'] = line[13:19]
    sql_common_columns['route_type'] = line[19:20]
    sql_common_columns['transition_identifier'] = line[20:25]
    sql_common_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
    sql_common_columns['sequence_number'] = line[26:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['rnp'] = line[44:47]
        sql_specific_columns['path_and_termination'] = line[47:49]
        sql_specific_columns['turn_direction_valid'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['arc_radius'] = line[56:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_holding_distance_or_time'] = line[74:78]
        sql_specific_columns['recommended_navaid_section_code'] = line[78:79]
        sql_specific_columns['recommended_navaid_subsection_code'] = line[79:80]
        sql_specific_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_specific_columns['altitude_description'] = line[82:83]
        sql_specific_columns['atc_indicator'] = line[83:84]
        sql_specific_columns['altitude_1'] = line[84:89]
        sql_specific_columns['altitude_2'] = line[89:94]
        sql_specific_columns['transition_altitude'] = line[94:99]
        sql_specific_columns['speed_limit'] = line[99:102]
        sql_specific_columns['vertical_angle'] = line[102:106]
        sql_specific_columns['center_fix_or_taa_procedure_turn_indicator'] = line[106:111]
        sql_specific_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_specific_columns['taa_icao_code'] = line[112:114]
        sql_specific_columns['taa_section_code'] = line[114:115]
        sql_specific_columns['taa_subsection_code'] = line[115:116]
        sql_specific_columns['gnss_fms_indication'] = line[116:117]
        sql_specific_columns['speed_limit_description'] = line[117:118]
        sql_specific_columns['route_qualifier_1'] = line[118:119]
        sql_specific_columns['route_qualifier_2'] = line[119:120]
        sql_specific_columns['route_qualifier_3'] = line[120:121]
        sql_specific_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            sql_specific_columns['e_procedure_tch'] = line[40:43]
            # = line[43:60]
            sql_specific_columns['e_procedure_design_mag_var'] = line[60:65]
            sql_specific_columns['e_procedure_design_mag_var_indicator'] = line[65:66]
            sql_specific_columns['e_procedure_referenced_fix_identifier_1'] = line[66:71]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_1'] = line[71:73]
            sql_specific_columns['e_procedure_referenced_fix_section_code_1'] = line[73:74]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_1'] = line[74:75]
            sql_specific_columns['e_procedure_referenced_fix_identifier_2'] = line[75:80]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_2'] = line[80:82]
            sql_specific_columns['e_procedure_referenced_fix_section_code_2'] = line[82:83]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_2'] = line[83:84]
            sql_specific_columns['e_procedure_referenced_fix_identifier_3'] = line[84:89]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_3'] = line[89:91]
            sql_specific_columns['e_procedure_referenced_fix_section_code_3'] = line[91:92]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_3'] = line[92:93]
            sql_specific_columns['e_procedure_referenced_fix_identifier_4'] = line[93:98]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_4'] = line[98:100]
            sql_specific_columns['e_procedure_referenced_fix_section_code_4'] = line[100:101]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_4'] = line[101:102]
            sql_specific_columns['e_cat_a_radii'] = line[102:104]
            sql_specific_columns['e_cat_b_radii'] = line[104:106]
            sql_specific_columns['e_cat_c_radii'] = line[106:108]
            sql_specific_columns['e_cat_d_radii'] = line[108:110]
            sql_specific_columns['e_special_indicator'] = line[110:111]
            # = line[111:115]
            sql_specific_columns['e_vertical_scale_factor'] = line[115:118]
            sql_specific_columns['e_route_qualifier_1'] = line[118:119]
            sql_specific_columns['e_route_qualifier_2'] = line[119:120]
            sql_specific_columns['e_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:74]
            sql_specific_columns[''] = line[74:78]
            # = line[78:118]
            sql_specific_columns[''] = line[118:119]
            sql_specific_columns[''] = line[119:120]
            sql_specific_columns[''] = line[120:121]
            # = line[121:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'W':
            sql_specific_columns['w_fas_block_provided_authorized'] = line[40:41]
            sql_specific_columns['w_fas_block_provided_lev_of_service_name'] = line[41:51]
            sql_specific_columns['w_lnav_vnav_authorized'] = line[51:52]
            sql_specific_columns['w_lnav_vnav_level_of_service_name'] = line[52:62]
            sql_specific_columns['w_lnav_authorized'] = line[62:63]
            sql_specific_columns['w_lnav_level_of_service_name'] = line[63:73]
            sql_specific_columns['w_remote_altimeter_flag'] = line[73:74]
            # = line[74:88]
            sql_specific_columns['w_rnp_authorized_1'] = line[88:89]
            sql_specific_columns['w_rnp_level_of_service_value_1'] = line[89:92]
            sql_specific_columns['w_rnp_authorized_2'] = line[92:93]
            sql_specific_columns['w_rnp_level_of_service_value_2'] = line[93:96]
            sql_specific_columns['w_rnp_authorized_3'] = line[96:97]
            sql_specific_columns['w_rnp_level_of_service_value_3'] = line[97:100]
            sql_specific_columns['w_rnp_authorized_4'] = line[100:101]
            sql_specific_columns['w_rnp_level_of_service_value_4'] = line[101:104]
            # = line[104:118]
            sql_specific_columns['w_route_qualifier_1'] = line[118:119]
            sql_specific_columns['w_route_qualifier_2'] = line[119:120]
            sql_specific_columns['w_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['w_file_record_number'] = line[123:128]
            sql_specific_columns['w_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - STAR (E)
def import_arinc_424_pe(sql_cursor, line):
    sql_table = 'airport_stars'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['star_identifier'] = line[13:19]
    sql_common_columns['route_type'] = line[19:20]
    sql_common_columns['transition_identifier'] = line[20:25]
    sql_common_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
    sql_common_columns['sequence_number'] = line[26:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['rnp'] = line[44:47]
        sql_specific_columns['path_and_termination'] = line[47:49]
        sql_specific_columns['turn_direction_valid'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['arc_radius'] = line[56:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_holding_distance_or_time'] = line[74:78]
        sql_specific_columns['recommended_navaid_section_code'] = line[78:79]
        sql_specific_columns['recommended_navaid_subsection_code'] = line[79:80]
        sql_specific_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_specific_columns['altitude_description'] = line[82:83]
        sql_specific_columns['atc_indicator'] = line[83:84]
        sql_specific_columns['altitude_1'] = line[84:89]
        sql_specific_columns['altitude_2'] = line[89:94]
        sql_specific_columns['transition_altitude'] = line[94:99]
        sql_specific_columns['speed_limit'] = line[99:102]
        sql_specific_columns['vertical_angle'] = line[102:106]
        sql_specific_columns['center_fix_or_taa_procedure_turn_indicator'] = line[106:111]
        sql_specific_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_specific_columns['taa_icao_code'] = line[112:114]
        sql_specific_columns['taa_section_code'] = line[114:115]
        sql_specific_columns['taa_subsection_code'] = line[115:116]
        sql_specific_columns['gnss_fms_indication'] = line[116:117]
        sql_specific_columns['speed_limit_description'] = line[117:118]
        sql_specific_columns['route_qualifier_1'] = line[118:119]
        sql_specific_columns['route_qualifier_2'] = line[119:120]
        sql_specific_columns['route_qualifier_3'] = line[120:121]
        sql_specific_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            sql_specific_columns['e_procedure_tch'] = line[40:43]
            # = line[43:60]
            sql_specific_columns['e_procedure_design_mag_var'] = line[60:65]
            sql_specific_columns['e_procedure_design_mag_var_indicator'] = line[65:66]
            sql_specific_columns['e_procedure_referenced_fix_identifier_1'] = line[66:71]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_1'] = line[71:73]
            sql_specific_columns['e_procedure_referenced_fix_section_code_1'] = line[73:74]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_1'] = line[74:75]
            sql_specific_columns['e_procedure_referenced_fix_identifier_2'] = line[75:80]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_2'] = line[80:82]
            sql_specific_columns['e_procedure_referenced_fix_section_code_2'] = line[82:83]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_2'] = line[83:84]
            sql_specific_columns['e_procedure_referenced_fix_identifier_3'] = line[84:89]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_3'] = line[89:91]
            sql_specific_columns['e_procedure_referenced_fix_section_code_3'] = line[91:92]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_3'] = line[92:93]
            sql_specific_columns['e_procedure_referenced_fix_identifier_4'] = line[93:98]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_4'] = line[98:100]
            sql_specific_columns['e_procedure_referenced_fix_section_code_4'] = line[100:101]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_4'] = line[101:102]
            sql_specific_columns['e_cat_a_radii'] = line[102:104]
            sql_specific_columns['e_cat_b_radii'] = line[104:106]
            sql_specific_columns['e_cat_c_radii'] = line[106:108]
            sql_specific_columns['e_cat_d_radii'] = line[108:110]
            sql_specific_columns['e_special_indicator'] = line[110:111]
            # = line[111:115]
            sql_specific_columns['e_vertical_scale_factor'] = line[115:118]
            sql_specific_columns['e_route_qualifier_1'] = line[118:119]
            sql_specific_columns['e_route_qualifier_2'] = line[119:120]
            sql_specific_columns['e_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:74]
            sql_specific_columns[''] = line[74:78]
            # = line[78:118]
            sql_specific_columns[''] = line[118:119]
            sql_specific_columns[''] = line[119:120]
            sql_specific_columns[''] = line[120:121]
            # = line[121:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'W':
            sql_specific_columns['w_fas_block_provided_authorized'] = line[40:41]
            sql_specific_columns['w_fas_block_provided_lev_of_service_name'] = line[41:51]
            sql_specific_columns['w_lnav_vnav_authorized'] = line[51:52]
            sql_specific_columns['w_lnav_vnav_level_of_service_name'] = line[52:62]
            sql_specific_columns['w_lnav_authorized'] = line[62:63]
            sql_specific_columns['w_lnav_level_of_service_name'] = line[63:73]
            sql_specific_columns['w_remote_altimeter_flag'] = line[73:74]
            # = line[74:88]
            sql_specific_columns['w_rnp_authorized_1'] = line[88:89]
            sql_specific_columns['w_rnp_level_of_service_value_1'] = line[89:92]
            sql_specific_columns['w_rnp_authorized_2'] = line[92:93]
            sql_specific_columns['w_rnp_level_of_service_value_2'] = line[93:96]
            sql_specific_columns['w_rnp_authorized_3'] = line[96:97]
            sql_specific_columns['w_rnp_level_of_service_value_3'] = line[97:100]
            sql_specific_columns['w_rnp_authorized_4'] = line[100:101]
            sql_specific_columns['w_rnp_level_of_service_value_4'] = line[101:104]
            # = line[104:118]
            sql_specific_columns['w_route_qualifier_1'] = line[118:119]
            sql_specific_columns['w_route_qualifier_2'] = line[119:120]
            sql_specific_columns['w_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['w_file_record_number'] = line[123:128]
            sql_specific_columns['w_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Approach Procedures (F)
def import_arinc_424_pf(sql_cursor, line):
    sql_table = 'airport_approach_procedures'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_identifier'] = line[13:19]
    sql_common_columns['route_type'] = line[19:20]
    sql_common_columns['transition_identifier'] = line[20:25]
    sql_common_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
    sql_common_columns['sequence_number'] = line[26:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['rnp'] = line[44:47]
        sql_specific_columns['path_and_termination'] = line[47:49]
        sql_specific_columns['turn_direction_valid'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['arc_radius'] = line[56:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_holding_distance_or_time'] = line[74:78]
        sql_specific_columns['recommended_navaid_section_code'] = line[78:79]
        sql_specific_columns['recommended_navaid_subsection_code'] = line[79:80]
        sql_specific_columns['leg_inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_specific_columns['altitude_description'] = line[82:83]
        sql_specific_columns['atc_indicator'] = line[83:84]
        sql_specific_columns['altitude_1'] = line[84:89]
        sql_specific_columns['altitude_2'] = line[89:94]
        sql_specific_columns['transition_altitude'] = line[94:99]
        sql_specific_columns['speed_limit'] = line[99:102]
        sql_specific_columns['vertical_angle'] = line[102:106]
        sql_specific_columns['center_fix_or_taa_procedure_turn_indicator'] = line[106:111]
        sql_specific_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_specific_columns['taa_icao_code'] = line[112:114]
        sql_specific_columns['taa_section_code'] = line[114:115]
        sql_specific_columns['taa_subsection_code'] = line[115:116]
        sql_specific_columns['gnss_fms_indication'] = line[116:117]
        sql_specific_columns['speed_limit_description'] = line[117:118]
        sql_specific_columns['route_qualifier_1'] = line[118:119]
        sql_specific_columns['route_qualifier_2'] = line[119:120]
        sql_specific_columns['route_qualifier_3'] = line[120:121]
        sql_specific_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            sql_specific_columns['e_procedure_tch'] = line[40:43]
            # = line[43:60]
            sql_specific_columns['e_procedure_design_mag_var'] = line[60:65]
            sql_specific_columns['e_procedure_design_mag_var_indicator'] = line[65:66]
            sql_specific_columns['e_procedure_referenced_fix_identifier_1'] = line[66:71]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_1'] = line[71:73]
            sql_specific_columns['e_procedure_referenced_fix_section_code_1'] = line[73:74]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_1'] = line[74:75]
            sql_specific_columns['e_procedure_referenced_fix_identifier_2'] = line[75:80]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_2'] = line[80:82]
            sql_specific_columns['e_procedure_referenced_fix_section_code_2'] = line[82:83]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_2'] = line[83:84]
            sql_specific_columns['e_procedure_referenced_fix_identifier_3'] = line[84:89]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_3'] = line[89:91]
            sql_specific_columns['e_procedure_referenced_fix_section_code_3'] = line[91:92]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_3'] = line[92:93]
            sql_specific_columns['e_procedure_referenced_fix_identifier_4'] = line[93:98]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_4'] = line[98:100]
            sql_specific_columns['e_procedure_referenced_fix_section_code_4'] = line[100:101]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_4'] = line[101:102]
            sql_specific_columns['e_cat_a_radii'] = line[102:104]
            sql_specific_columns['e_cat_b_radii'] = line[104:106]
            sql_specific_columns['e_cat_c_radii'] = line[106:108]
            sql_specific_columns['e_cat_d_radii'] = line[108:110]
            sql_specific_columns['e_special_indicator'] = line[110:111]
            # = line[111:115]
            sql_specific_columns['e_vertical_scale_factor'] = line[115:118]
            sql_specific_columns['e_route_qualifier_1'] = line[118:119]
            sql_specific_columns['e_route_qualifier_2'] = line[119:120]
            sql_specific_columns['e_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:74]
            sql_specific_columns['p_leg_distance'] = line[74:78]
            # = line[78:118]
            sql_specific_columns['p_route_qualifier_1'] = line[118:119]
            sql_specific_columns['p_route_qualifier_2'] = line[119:120]
            sql_specific_columns['p_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'W':
            sql_specific_columns['w_fas_block_provided_authorized'] = line[40:41]
            sql_specific_columns['w_fas_block_provided_lev_of_service_name'] = line[41:51]
            sql_specific_columns['w_lnav_vnav_authorized'] = line[51:52]
            sql_specific_columns['w_lnav_vnav_level_of_service_name'] = line[52:62]
            sql_specific_columns['w_lnav_authorized'] = line[62:63]
            sql_specific_columns['w_lnav_level_of_service_name'] = line[63:73]
            sql_specific_columns['w_remote_altimeter_flag'] = line[73:74]
            # = line[74:88]
            sql_specific_columns['w_rnp_authorized_1'] = line[88:89]
            sql_specific_columns['w_rnp_level_of_service_value_1'] = line[89:92]
            sql_specific_columns['w_rnp_authorized_2'] = line[92:93]
            sql_specific_columns['w_rnp_level_of_service_value_2'] = line[93:96]
            sql_specific_columns['w_rnp_authorized_3'] = line[96:97]
            sql_specific_columns['w_rnp_level_of_service_value_3'] = line[97:100]
            sql_specific_columns['w_rnp_authorized_4'] = line[100:101]
            sql_specific_columns['w_rnp_level_of_service_value_4'] = line[101:104]
            # = line[104:118]
            sql_specific_columns['w_route_qualifier_1'] = line[118:119]
            sql_specific_columns['w_route_qualifier_2'] = line[119:120]
            sql_specific_columns['w_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['w_file_record_number'] = line[123:128]
            sql_specific_columns['w_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Runway (G)
def import_arinc_424_pg(sql_cursor, line):
    sql_table = 'airport_runways'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['runway_identifier'] = line[13:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['runway_length'] = line[22:27]
        sql_specific_columns['runway_magnetic_bearing'] = line[27:31]
        # = line[31:32]
        sql_specific_columns['runway_latitude'] = line[32:41]
        sql_specific_columns['runway_longitude'] = line[41:51]
        sql_specific_columns['runway_gradient'] = line[51:56]  # TODO
        # = line[56:60] # TODO
        sql_specific_columns['ltp_ellipsoid_height'] = line[60:66]
        sql_specific_columns['landing_threshold_elevation'] = line[66:71]
        sql_specific_columns['displaced_threshold_distance'] = line[71:75]
        sql_specific_columns['threshold_crossing_height_old'] = line[75:77]
        sql_specific_columns['runway_width'] = line[77:80]
        sql_specific_columns['tch_value_indicator'] = line[80:81]
        sql_specific_columns['localizer_mls_gls_ref_path_identifier'] = line[81:85]
        sql_specific_columns['localizer_mls_gls_category_class'] = line[85:86]
        sql_specific_columns['stopway'] = line[86:90]
        sql_specific_columns['second_localizer_mls_gls_ref_path_identifier'] = line[90:94]
        sql_specific_columns['second_localizer_mls_gls_category_class'] = line[94:95]
        sql_specific_columns['threshold_crossing_height'] = line[95:98]
        sql_specific_columns['runway_accuracy_compliance_flag'] = line[98:99]
        sql_specific_columns['landing_threshold_elevation_accuracy_compliance_flag'] = line[99:100]
        # = line[100:101]
        sql_specific_columns['runway_description'] = line[101:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            sql_specific_columns['a_runway_surface_type'] = line[92:96]
            sql_specific_columns['a_runway_surface_code'] = line[96:97]
            sql_specific_columns['a_starter_extension'] = line[97:101]
            sql_specific_columns['a_tora'] = line[101:106]
            sql_specific_columns['a_toda'] = line[106:111]
            sql_specific_columns['a_asda'] = line[111:116]
            sql_specific_columns['a_lda'] = line[116:121]
            sql_specific_columns['a_runway_usage_indicator'] = line[121:122]
            # = line[122:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'S':
            # = line[23:51]
            sql_specific_columns['s_runway_true_bearing'] = line[51:56]
            sql_specific_columns['s_true_bearing_source'] = line[56:57]
            # = line[57:65]
            sql_specific_columns['s_tdze'] = line[65:66]
            sql_specific_columns['s_touchdown_zone_elevation'] = line[66:71]
            # = line[71:123]
            sql_specific_columns['s_file_record_number'] = line[123:128]
            sql_specific_columns['s_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Localizer/Glide Slope (I)
def import_arinc_424_pi(sql_cursor, line):
    sql_table = 'airport_localizers_and_glideslopes'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier_or_heliport'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['localizer_identifier'] = line[13:17]
    sql_common_columns['ils_category'] = line[17:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['localizer_frequency'] = line[22:27]
        sql_specific_columns['runway_identifier'] = line[27:32]
        sql_specific_columns['localizer_latitude'] = line[32:41]
        sql_specific_columns['localizer_longitude'] = line[41:51]
        sql_specific_columns['localizer_bearing'] = line[51:55]
        sql_specific_columns['glideslope_latitude'] = line[55:64]
        sql_specific_columns['glideslope_longitude'] = line[64:74]
        sql_specific_columns['localizer_position'] = line[74:78]
        sql_specific_columns['localizer_position_reference'] = line[78:79]
        sql_specific_columns['glideslope_position'] = line[79:83]
        sql_specific_columns['localizer_width'] = line[83:87]
        sql_specific_columns['glideslope_angle'] = line[87:90]
        sql_specific_columns['station_declination'] = line[90:95]
        sql_specific_columns['glideslope_height_at_landing_threshold_old'] = line[95:97]
        sql_specific_columns['glideslope_elevation'] = line[97:102]
        sql_specific_columns['supporting_facility_identifier'] = line[102:106]
        sql_specific_columns['supporting_facility_icao_code'] = line[106:108]
        sql_specific_columns['supporting_facility_section_code'] = line[108:109]
        sql_specific_columns['supporting_facility_subsection_code'] = line[109:110]
        sql_specific_columns['glideslope_height_at_landing_threshold'] = line[110:113]
        # = line[113:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'S':
            # = line[23:27]
            sql_specific_columns['s_facility_characteristics'] = line[27:32]
            # = line[32:51]
            sql_specific_columns['s_localizer_true_bearing'] = line[51:56]
            sql_specific_columns['s_localizer_bearing_source'] = line[56:57]
            # = line[57:87]
            sql_specific_columns['s_glideslope_beam_width'] = line[87:90]
            sql_specific_columns['s_approach_route_ident_1'] = line[90:96]
            sql_specific_columns['s_approach_route_ident_2'] = line[96:102]
            sql_specific_columns['s_approach_route_ident_3'] = line[102:108]
            sql_specific_columns['s_approach_route_ident_4'] = line[108:114]
            sql_specific_columns['s_approach_route_ident_5'] = line[114:120]
            # = line[120:123]
            sql_specific_columns['s_file_record_number'] = line[123:128]
            sql_specific_columns['s_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Company Routes (R) - Company Routes ( )
def import_arinc_424_r_(sql_cursor, line):
    sql_table = 'company_routes'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['from_airport_fix_identifier'] = line[6:11]
    # = line[11:12]
    sql_common_columns['from_airport_fix_icao_code'] = line[12:14]
    sql_common_columns['from_airport_fix_section_code'] = line[14:15]
    sql_common_columns['from_airport_fix_subsection_code'] = line[15:16]
    sql_common_columns['to_airport_fix_identifier'] = line[16:21]
    # = line[21:22]
    sql_common_columns['to_airport_fix_icao_code'] = line[22:24]
    sql_common_columns['to_airport_fix_section_code'] = line[24:25]
    sql_common_columns['to_airport_fix_subsection_code'] = line[25:26]
    sql_common_columns['company_route_identifier'] = line[26:36]
    sql_common_columns['sequence_number'] = line[36:39]
    sql_common_columns['via'] = line[39:42]
    sql_common_columns['sid_star_app_awy'] = line[42:48]
    sql_common_columns['area_code'] = line[48:51]
    sql_common_columns['to_fix_identifier'] = line[51:57]
    sql_common_columns['to_fix_icao_code'] = line[57:59]
    sql_common_columns['to_fix_section_code'] = line[59:60]
    sql_common_columns['to_fix_subsection_code'] = line[60:61]
    sql_common_columns['runway_trans'] = line[61:66]
    sql_common_columns['enrt_trans'] = line[66:71]
    # = line[71:72]
    sql_common_columns['cruise_altitude'] = line[72:77]
    sql_common_columns['terminal_alternate_airport_identifier'] = line[77:81]
    sql_common_columns['terminal_alternate_airport_icao_code'] = line[81:83]
    sql_common_columns['alternate_distance'] = line[83:87]
    sql_common_columns['cost_index'] = line[87:90]
    sql_common_columns['enroute_alternate_airport'] = line[90:94]
    sql_common_columns['sid_star_app_awy_route_type'] = line[94:95]
    sql_common_columns['ssa_route_type_qualifier_1'] = line[95:96]
    sql_common_columns['ssa_route_type_qualifier_2'] = line[96:97]
    # = line[97:123]
    sql_common_columns['file_record_number'] = line[123:128]
    sql_common_columns['cycle_date'] = line[128:132]
    sql_primary(sql_cursor, sql_table, sql_common_columns, {})


# Airport (P) - Localizer Marker (M)
def import_arinc_424_pm(sql_cursor, line):
    sql_table = 'airport_localizer_markers'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['localizer_identifier'] = line[13:17]
    sql_common_columns['marker_type'] = line[17:20]
    # = line[20:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['locator_frequency'] = line[22:27]
        sql_specific_columns['runway_helipad_identifier'] = line[27:32]
        sql_specific_columns['marker_latitude'] = line[32:41]
        sql_specific_columns['marker_longitude'] = line[41:51]
        sql_specific_columns['minor_axis_bearing'] = line[51:55]
        sql_specific_columns['locator_latitude'] = line[55:64]
        sql_specific_columns['locator_longitude'] = line[64:74]
        sql_specific_columns['locator_class'] = line[74:79]
        sql_specific_columns['locator_facility_characteristics'] = line[79:84]
        sql_specific_columns['locator_identifier'] = line[84:88]
        # = line[88:90]
        sql_specific_columns['magnetic_variation'] = line[90:95]
        # = line[95:97]
        sql_specific_columns['facility_elevation'] = line[97:102]
        # = line[102:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Communications (V)
def import_arinc_424_pv(sql_cursor, line):
    sql_table = 'airport_communications'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    # = line[13:15]
    sql_common_columns['communication_class'] = line[15:19]
    sql_common_columns['sequence_number'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['communication_types'] = line[22:25]
        sql_specific_columns['transmit_frequency'] = line[25:32]
        sql_specific_columns['receive_frequency'] = line[32:39]
        sql_specific_columns['frequency_units'] = line[39:40]
        sql_specific_columns['radar_units'] = line[40:41]
        sql_specific_columns['h24_indicator'] = line[41:42]
        sql_specific_columns['call_signs'] = line[42:67]
        sql_specific_columns['multi_sector_indicator'] = line[67:68]
        sql_specific_columns['sectorization'] = line[68:74]
        sql_specific_columns['sector_facility_identifier'] = line[74:78]
        sql_specific_columns['sector_facility_icao_code'] = line[78:80]
        sql_specific_columns['sector_facility_section_code'] = line[80:81]
        sql_specific_columns['sector_facility_subsection_code'] = line[81:82]
        sql_specific_columns['altitude_description_code'] = line[82:83]
        sql_specific_columns['communication_altitude_1'] = line[83:86]
        sql_specific_columns['communication_altitude_2'] = line[86:89]
        sql_specific_columns['distance_description_code'] = line[89:90]
        sql_specific_columns['communications_distance'] = line[90:92]
        sql_specific_columns['transmitter_latitude'] = line[92:101]
        sql_specific_columns['transmitter_longitude'] = line[101:111]
        sql_specific_columns['service_indicator'] = line[111:114]
        sql_specific_columns['modulation'] = line[114:115]
        sql_specific_columns['signal_emission'] = line[115:116]
        # = line[116:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'E':
            sql_specific_columns['e_remote_facility_identifier'] = line[23:27]
            sql_specific_columns['e_remote_facility_icao_code'] = line[27:29]
            sql_specific_columns['e_remote_facility_section_code'] = line[29:30]
            sql_specific_columns['e_remote_facility_subsection_code'] = line[30:31]
            sql_specific_columns['e_transmitter_site_mag_var'] = line[31:36]
            sql_specific_columns['e_transmitter_site_elevation'] = line[36:41]
            sql_specific_columns['e_additional_sectorization_1'] = line[41:47]
            sql_specific_columns['e_additional_sectorization_1_altitude_description'] = line[47:48]
            sql_specific_columns['e_additional_sectorization_1_altitude_1'] = line[48:51]
            sql_specific_columns['e_additional_sectorization_1_altitude_2'] = line[51:54]
            sql_specific_columns['e_additional_sectorization_2'] = line[54:60]
            sql_specific_columns['e_additional_sectorization_2_altitude_description'] = line[60:61]
            sql_specific_columns['e_additional_sectorization_2_altitude_1'] = line[61:64]
            sql_specific_columns['e_additional_sectorization_2_altitude_2'] = line[64:67]
            # = line[67:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'N':
            sql_specific_columns['n_sectorization_narrative'] = line[23:83]
            # = line[83:123]
            sql_specific_columns['n_file_record_number'] = line[123:128]
            sql_specific_columns['n_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'T':
            # = line[23:69]
            sql_specific_columns['t_time_of_operation_1'] = line[69:79]
            sql_specific_columns['t_time_of_operation_2'] = line[79:89]
            sql_specific_columns['t_time_of_operation_3'] = line[89:99]
            sql_specific_columns['t_time_of_operation_4'] = line[99:109]
            sql_specific_columns['t_time_of_operation_5'] = line[109:119]
            # = line[119:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'U':
            sql_specific_columns['u_time_narrative'] = line[23:123]
            sql_specific_columns['u_file_record_number'] = line[123:128]
            sql_specific_columns['u_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Airway Marker (M)
def import_arinc_424_em(sql_cursor, line):
    sql_table = 'enroute_airway_markers'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    # = line[6:13]
    sql_common_columns['marker_identifier'] = line[13:17]
    # = line[17:19]
    sql_common_columns['marker_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['marker_code'] = line[22:26]
        # = line[26:27]
        sql_specific_columns['marker_shape'] = line[27:28]
        sql_specific_columns['marker_power'] = line[28:29]
        # = line[29:32]
        sql_specific_columns['marker_latitude'] = line[32:41]
        sql_specific_columns['marker_longitude'] = line[41:51]
        sql_specific_columns['minor_axis'] = line[51:55]
        # = line[55:74]
        sql_specific_columns['magnetic_variation'] = line[74:79]
        sql_specific_columns['facility_elevation'] = line[79:84]
        sql_specific_columns['datum_code'] = line[84:87]
        # = line[87:93]
        sql_specific_columns['marker_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'a':
            sql_specific_columns['a_notes'] = line[23:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Tables (T) - Cruising Tables (C)
def import_arinc_424_tc(sql_cursor, line):
    sql_table = 'tables_cruising_tables'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    # = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['cruise_table_identifier'] = line[6:8]
    sql_common_columns['sequence_number'] = line[8:9]
    # = line[9:28]
    sql_common_columns['course_from'] = line[28:32]
    sql_common_columns['course_to'] = line[32:36]
    sql_common_columns['mag_true'] = line[36:37]
    # = line[37:39]
    sql_common_columns['cruise_level_from_1'] = line[39:44]
    sql_common_columns['vertical_separation_1'] = line[44:49]
    sql_common_columns['cruise_level_to_1'] = line[49:54]
    sql_common_columns['cruise_level_from_2'] = line[54:59]
    sql_common_columns['vertical_separation_2'] = line[59:64]
    sql_common_columns['cruise_level_to_2'] = line[64:69]
    sql_common_columns['cruise_level_from_3'] = line[69:74]
    sql_common_columns['vertical_separation_3'] = line[74:79]
    sql_common_columns['cruise_level_to_3'] = line[79:84]
    sql_common_columns['cruise_level_from_4'] = line[84:89]
    sql_common_columns['vertical_separation_4'] = line[89:94]
    sql_common_columns['cruise_level_to_4'] = line[94:99]
    # = line[99:123]
    sql_common_columns['file_record_number'] = line[123:128]
    sql_common_columns['cycle_date'] = line[128:132]
    sql_primary(sql_cursor, sql_table, sql_common_columns, {})


# Airspace (U) - FIR/UIR (F)
def import_arinc_424_uf(sql_cursor, line):
    sql_table = 'airspace_fir_uir'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['fir_uir_identifier'] = line[6:10]
    sql_common_columns['fir_uir_address'] = line[10:14]
    sql_common_columns['fir_uir_indicator'] = line[14:15]
    sql_common_columns['sequence_number'] = line[15:19]
    continuation_record_no = line[19:20]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['adjacent_fir_identifier'] = line[20:24]
        sql_specific_columns['adjacent_uir_identifier'] = line[24:28]
        sql_specific_columns['reporting_units_speed'] = line[28:29]
        sql_specific_columns['reporting_units_altitude'] = line[29:30]
        sql_specific_columns['entry_report'] = line[30:31]
        # = line[31:32]
        sql_specific_columns['boundary_via'] = line[32:34]
        sql_specific_columns['fir_uir_latitude'] = line[34:43]
        sql_specific_columns['fir_uir_longitude'] = line[43:53]
        sql_specific_columns['arc_origin_latitude'] = line[53:62]
        sql_specific_columns['arc_origin_longitude'] = line[62:72]
        sql_specific_columns['arc_distance'] = line[72:76]
        sql_specific_columns['arc_bearing'] = line[76:80]
        sql_specific_columns['fir_upper_limit'] = line[80:85]
        sql_specific_columns['uir_lower_limit'] = line[85:90]
        sql_specific_columns['uir_upper_limit'] = line[90:95]
        sql_specific_columns['cruise_table_ind'] = line[95:97]
        # = line[97:98]
        sql_specific_columns['fir_uir_name'] = line[98:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[21:22]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airspace (U) - Restrictive Airspace (R)
def import_arinc_424_ur(sql_cursor, line):
    sql_table = 'airspace_restrictive_airspaces'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['restrictive_airspace_icao_code'] = line[6:8]
    sql_common_columns['restrictive_type'] = line[8:9]
    sql_common_columns['restrictive_airspace_designation'] = line[9:19]
    sql_common_columns['multiple_code'] = line[19:20]
    sql_common_columns['sequence_number'] = line[20:24]
    continuation_record_no = line[24:25]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['level'] = line[25:26]
        sql_specific_columns['time_code'] = line[26:27]
        sql_specific_columns['notam'] = line[27:28]
        # = line[28:30]
        sql_specific_columns['boundary_via'] = line[30:32]
        sql_specific_columns['latitude'] = line[32:41]
        sql_specific_columns['longitude'] = line[41:51]
        sql_specific_columns['arc_origin_latitude'] = line[51:60]
        sql_specific_columns['arc_origin_longitude'] = line[60:70]
        sql_specific_columns['arc_distance'] = line[70:74]
        sql_specific_columns['arc_bearing'] = line[74:78]
        # = line[78:81]
        sql_specific_columns['lower_limit'] = line[81:86]
        sql_specific_columns['lower_limit_unit_indicator'] = line[86:87]
        sql_specific_columns['upper_limit'] = line[87:92]
        sql_specific_columns['upper_limit_unit_indicator'] = line[92:93]
        sql_specific_columns['restrictive_airspace_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[25:26]
        if application_type == 'T':
            sql_specific_columns = {}
            sql_specific_columns['t_continuation_time_code'] = line[26:27]
            sql_specific_columns['t_continuation_notam'] = line[27:28]
            sql_specific_columns['t_time_indicator'] = line[28:29]
            sql_specific_columns['t_time_of_operations_1'] = line[29:39]
            sql_specific_columns['t_time_of_operations_2'] = line[39:49]
            sql_specific_columns['t_time_of_operations_3'] = line[49:59]
            sql_specific_columns['t_time_of_operations_4'] = line[59:69]
            sql_specific_columns['t_time_of_operations_5'] = line[69:79]
            sql_specific_columns['t_time_of_operations_6'] = line[79:89]
            sql_specific_columns['t_time_of_operations_7'] = line[89:99]
            # = line[99:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'U':
            sql_specific_columns = {}
            sql_specific_columns['u_narrative_time'] = line[26:123]
            sql_specific_columns['u_file_record_number'] = line[123:128]
            sql_specific_columns['u_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'C':
            sql_specific_columns = {}
            # = line[26:99]
            sql_specific_columns['c_controlling_agency'] = line[99:123]
            sql_specific_columns['c_file_record_number'] = line[123:128]
            sql_specific_columns['c_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# MORA (A) - Grid MORA (S)
def import_arinc_424_as(sql_cursor, line):
    sql_table = 'mora_grid_mora'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    # = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    # = line[6:13]
    sql_common_columns['starting_latitude'] = line[13:16]
    sql_common_columns['starting_longitude'] = line[16:20]
    # = line[20:30]
    sql_common_columns['mora_01'] = line[30:33]
    sql_common_columns['mora_02'] = line[33:36]
    sql_common_columns['mora_03'] = line[36:39]
    sql_common_columns['mora_04'] = line[39:42]
    sql_common_columns['mora_05'] = line[42:45]
    sql_common_columns['mora_06'] = line[45:48]
    sql_common_columns['mora_07'] = line[48:51]
    sql_common_columns['mora_08'] = line[51:54]
    sql_common_columns['mora_09'] = line[54:57]
    sql_common_columns['mora_10'] = line[57:60]
    sql_common_columns['mora_11'] = line[60:63]
    sql_common_columns['mora_12'] = line[63:66]
    sql_common_columns['mora_13'] = line[66:69]
    sql_common_columns['mora_14'] = line[69:72]
    sql_common_columns['mora_15'] = line[72:75]
    sql_common_columns['mora_16'] = line[75:78]
    sql_common_columns['mora_17'] = line[78:81]
    sql_common_columns['mora_18'] = line[81:84]
    sql_common_columns['mora_19'] = line[84:87]
    sql_common_columns['mora_20'] = line[87:90]
    sql_common_columns['mora_21'] = line[90:93]
    sql_common_columns['mora_22'] = line[93:96]
    sql_common_columns['mora_23'] = line[96:99]
    sql_common_columns['mora_24'] = line[99:102]
    sql_common_columns['mora_25'] = line[102:105]
    sql_common_columns['mora_26'] = line[105:108]
    sql_common_columns['mora_27'] = line[108:111]
    sql_common_columns['mora_28'] = line[111:114]
    sql_common_columns['mora_29'] = line[114:117]
    sql_common_columns['mora_30'] = line[117:120]
    # = line[120:123]
    sql_common_columns['file_record_number'] = line[123:128]
    sql_common_columns['cycle_date'] = line[128:132]
    sql_primary(sql_cursor, sql_table, sql_common_columns, {})


# Airport (P) - MSA (S)
def import_arinc_424_ps(sql_cursor, line):
    sql_table = 'airport_msa'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['msa_center_identifier'] = line[13:18]
    sql_common_columns['msa_center_icao_code'] = line[18:20]
    sql_common_columns['msa_center_section_code'] = line[20:21]
    sql_common_columns['msa_center_subsection_code'] = line[21:22]
    sql_common_columns['multiple_code'] = line[22:23]
    # = line[23:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[39:42]
        sql_specific_columns['sector_bearing_1'] = line[42:48]
        sql_specific_columns['sector_altitude_1'] = line[48:51]
        sql_specific_columns['sector_radius_1'] = line[51:53]
        sql_specific_columns['sector_bearing_2'] = line[53:59]
        sql_specific_columns['sector_altitude_2'] = line[59:62]
        sql_specific_columns['sector_radius_2'] = line[62:64]
        sql_specific_columns['sector_bearing_3'] = line[64:70]
        sql_specific_columns['sector_altitude_3'] = line[70:73]
        sql_specific_columns['sector_radius_3'] = line[73:75]
        sql_specific_columns['sector_bearing_4'] = line[75:81]
        sql_specific_columns['sector_altitude_4'] = line[81:84]
        sql_specific_columns['sector_radius_4'] = line[84:86]
        sql_specific_columns['sector_bearing_5'] = line[86:92]
        sql_specific_columns['sector_altitude_5'] = line[92:95]
        sql_specific_columns['sector_radius_5'] = line[95:97]
        sql_specific_columns['sector_bearing_6'] = line[97:103]
        sql_specific_columns['sector_altitude_6'] = line[103:106]
        sql_specific_columns['sector_radius_6'] = line[106:108]
        sql_specific_columns['sector_bearing_7'] = line[108:114]
        sql_specific_columns['sector_altitude_7'] = line[114:117]
        sql_specific_columns['sector_radius_7'] = line[117:119]
        sql_specific_columns['magnetic_true_indicator'] = line[119:120]
        # = line[120:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            # = line[40:42]
            sql_specific_columns['e_sector_bearing_1'] = line[42:48]
            sql_specific_columns['e_sector_altitude_1'] = line[48:51]
            sql_specific_columns['e_sector_radius_1'] = line[51:53]
            sql_specific_columns['e_sector_bearing_2'] = line[53:59]
            sql_specific_columns['e_sector_altitude_2'] = line[59:62]
            sql_specific_columns['e_sector_radius_2'] = line[62:64]
            sql_specific_columns['e_sector_bearing_3'] = line[64:70]
            sql_specific_columns['e_sector_altitude_3'] = line[70:73]
            sql_specific_columns['e_sector_radius_3'] = line[73:75]
            sql_specific_columns['e_sector_bearing_4'] = line[75:81]
            sql_specific_columns['e_sector_altitude_4'] = line[81:84]
            sql_specific_columns['e_sector_radius_4'] = line[84:86]
            sql_specific_columns['e_sector_bearing_5'] = line[86:92]
            sql_specific_columns['e_sector_altitude_5'] = line[92:95]
            sql_specific_columns['e_sector_radius_5'] = line[95:97]
            sql_specific_columns['e_sector_bearing_6'] = line[97:103]
            sql_specific_columns['e_sector_altitude_6'] = line[103:106]
            sql_specific_columns['e_sector_radius_6'] = line[106:108]
            sql_specific_columns['e_sector_bearing_7'] = line[108:114]
            sql_specific_columns['e_sector_altitude_7'] = line[114:117]
            sql_specific_columns['e_sector_radius_7'] = line[117:119]
            sql_specific_columns['e_magnetic_true_indicator'] = line[119:120]
            # = line[120:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'A':
            sql_specific_columns['a_notes'] = line[40:109]
            # = line[109:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Airway Restriction (U)
def import_arinc_424_eu(sql_cursor, line):
    sql_table = 'enroute_airway_restrictions'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['route_identifier'] = line[6:11]
    # = line[11:12]
    sql_common_columns['restriction_identifier'] = line[12:15]
    sql_common_columns['restriction_type'] = line[15:17]
    continuation_record_no = line[17:18]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['start_fix_identifier'] = line[18:23]
        sql_specific_columns['start_fix_icao_code'] = line[23:25]
        sql_specific_columns['start_fix_section_code'] = line[25:26]
        sql_specific_columns['start_fix_subsection_code'] = line[26:27]
        sql_specific_columns['end_fix_identifier'] = line[27:32]
        sql_specific_columns['end_fix_icao_code'] = line[32:34]
        sql_specific_columns['end_fix_section_code'] = line[34:35]
        sql_specific_columns['end_fix_subsection_code'] = line[35:36]
        # = line[36:37]
        sql_specific_columns['start_date'] = line[37:44]
        sql_specific_columns['end_date'] = line[44:51]
        sql_specific_columns['time_code'] = line[51:52]
        sql_specific_columns['time_indicator'] = line[52:53]
        sql_specific_columns['time_of_operation_1'] = line[53:63]
        sql_specific_columns['time_of_operation_2'] = line[63:73]
        sql_specific_columns['time_of_operation_3'] = line[73:83]
        sql_specific_columns['time_of_operation_4'] = line[83:93]
        sql_specific_columns['exclusion_indicator'] = line[93:94]
        sql_specific_columns['units_of_altitude'] = line[94:95]
        sql_specific_columns['restriction_altitude_1'] = line[95:98]
        sql_specific_columns['block_indicator_1'] = line[98:99]
        sql_specific_columns['restriction_altitude_2'] = line[99:102]
        sql_specific_columns['block_indicator_2'] = line[102:103]
        sql_specific_columns['restriction_altitude_3'] = line[103:106]
        sql_specific_columns['block_indicator_3'] = line[106:107]
        sql_specific_columns['restriction_altitude_4'] = line[107:110]
        sql_specific_columns['block_indicator_4'] = line[110:111]
        sql_specific_columns['restriction_altitude_5'] = line[111:114]
        sql_specific_columns['block_indicator_5'] = line[114:115]
        sql_specific_columns['restriction_altitude_6'] = line[115:118]
        sql_specific_columns['block_indicator_6'] = line[118:119]
        sql_specific_columns['restriction_altitude_7'] = line[119:122]
        sql_specific_columns['block_indicator_7'] = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        appliction_type == line[18:19]
        # Note Restriction Primary
        # TODO = line[18:23]
        # TODO = line[23:25]
        # TODO = line[25:26]
        # TODO = line[26:27]
        # TODO = line[27:32]
        # TODO = line[32:34]
        # TODO = line[34:35]
        # TODO = line[35:36]
        # TODO = line[36:37]
        # TODO = line[37:44]
        # TODO = line[44:51]
        # TODO = line[51:120]
        # = line[120:123]

        # Note Restriction Continuation
        # = line[19:51]
        # TODO = line[51:120]
        # = line[120:123]

        # Seasonal Closure Primary
        # TODO = line[18:23]
        # TODO = line[23:25]
        # TODO = line[25:26]
        # TODO = line[26:27]
        # TODO = line[27:32]
        # TODO = line[32:34]
        # TODO = line[34:35]
        # TODO = line[35:36]
        # TODO = line[36:37]
        # TODO = line[37:44]
        # TODO = line[44:51]
        # TODO = line[51:52]
        # = line[52:93]
        # TODO = line[93:95]
        # = line[95:123]

        # Cruising Table Replacement Primary
        # TODO = line[18:23]
        # TODO = line[23:25]
        # TODO = line[25:26]
        # TODO = line[26:27]
        # TODO = line[27:32]
        # TODO = line[32:34]
        # TODO = line[34:35]
        # TODO = line[35:36]
        # TODO = line[36:37]
        # TODO = line[37:44]
        # TODO = line[44:51]
        # TODO = line[51:52]
        # = line[52:93]
        # TODO = line[93:95]
        # = line[95:123]

        # Altitude Exclusion Primary Extension Continuation
        # TODO = line[19:93]
        # TODO = line[93:94]
        # TODO = line[94:95]
        # TODO = line[95:98]
        # TODO = line[98:99]
        # TODO = line[99:102]
        # TODO = line[102:103]
        # TODO = line[103:106]
        # TODO = line[106:107]
        # TODO = line[107:110]
        # TODO = line[110:111]
        # TODO = line[111:114]
        # TODO = line[114:115]
        # TODO = line[115:118]
        # TODO = line[118:119]
        # TODO = line[119:122]
        # TODO = line[122:123]

        print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
        print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Airport and Heliport MLS (Azimuth, Elevation and Back Azimuth) (L)
def import_arinc_424_pl(sql_cursor, line):
    sql_table = 'airport_mls'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['mls_identifier'] = line[13:17]
    sql_common_columns['mls_category'] = line[17:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['channel'] = line[22:25]
        # = line[25:27]
        sql_specific_columns['runway_identifier'] = line[27:32]
        sql_specific_columns['azimuth_latitude'] = line[32:41]
        sql_specific_columns['azimuth_longitude'] = line[41:51]
        sql_specific_columns['azimuth_bearing'] = line[51:55]
        sql_specific_columns['elevation_latitude'] = line[55:64]
        sql_specific_columns['elevation_longitude'] = line[64:74]
        sql_specific_columns['azimuth_position'] = line[74:78]
        sql_specific_columns['azimuth_position_reference'] = line[78:79]
        sql_specific_columns['elevation_position'] = line[79:83]
        sql_specific_columns['azimuth_proportional_angle_right'] = line[83:86]
        sql_specific_columns['azimuth_proportional_angle_left'] = line[86:89]
        sql_specific_columns['azimuth_coverage_right'] = line[89:92]
        sql_specific_columns['azimuth_coverage_left'] = line[92:95]
        sql_specific_columns['elevation_angle_span'] = line[95:98]
        sql_specific_columns['magnetic_variation'] = line[98:103]
        sql_specific_columns['el_elevation'] = line[103:108]
        sql_specific_columns['nominal_elevation_angle'] = line[108:112]
        sql_specific_columns['minimum_glide_path_angle'] = line[112:115]
        sql_specific_columns['supporting_facility_identifier'] = line[115:119]
        sql_specific_columns['supporting_facility_icao_code'] = line[119:121]
        sql_specific_columns['supporting_facility_section_code'] = line[121:122]
        sql_specific_columns['supporting_facility_subsection_code'] = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        # TODO
        print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
        print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Communications (V)
def import_arinc_424_ev(sql_cursor, line):
    sql_table = 'enroute_communications'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['fir_rdo_identifier'] = line[6:10]
    sql_common_columns['fir_uir_address'] = line[10:14]
    sql_common_columns['indicator'] = line[14:15]
    sql_common_columns['communication_class'] = line[15:19]
    sql_common_columns['sequence_number'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['communications_type'] = line[22:25]
        sql_specific_columns['transmit_frequency'] = line[25:32]
        sql_specific_columns['receive_frequency'] = line[32:39]
        sql_specific_columns['frequency_units'] = line[39:40]
        sql_specific_columns['radar_service'] = line[40:41]
        sql_specific_columns['h24_indicator'] = line[41:42]
        sql_specific_columns['call_sign'] = line[42:67]
        sql_specific_columns['position_narrative'] = line[67:92]
        sql_specific_columns['latitude'] = line[92:101]
        sql_specific_columns['longitude'] = line[101:111]
        sql_specific_columns['service_indicator'] = line[111:114]
        sql_specific_columns['modulation'] = line[114:115]
        sql_specific_columns['signal_emission'] = line[115:116]
        sql_specific_columns['altitude_description'] = line[116:117]
        sql_specific_columns['communication_altitude_1'] = line[117:120]
        sql_specific_columns['communication_altitude_2'] = line[120:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'E':
            sql_specific_columns['e_remote_facility_identifier'] = line[23:27]
            sql_specific_columns['e_remote_facility_icao_code'] = line[27:29]
            sql_specific_columns['e_remote_facility_section_code'] = line[29:30]
            sql_specific_columns['e_remote_facility_subsection_code'] = line[30:31]
            sql_specific_columns['e_transmitter_site_mag_var'] = line[31:36]
            sql_specific_columns['e_transmitter_site_elevation'] = line[36:41]
            sql_specific_columns['e_assigned_sector_name'] = line[41:66]
            sql_specific_columns['e_time_code'] = line[66:67]
            sql_specific_columns['e_notam'] = line[67:68]
            # = line[68:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'T':
            # = line[23:69]
            sql_specific_columns['t_time_of_operation_1'] = line[69:79]
            sql_specific_columns['t_time_of_operation_2'] = line[79:89]
            sql_specific_columns['t_time_of_operation_3'] = line[89:99]
            sql_specific_columns['t_time_of_operation_4'] = line[99:109]
            sql_specific_columns['t_time_of_operation_5'] = line[109:119]
            # = line[119:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'U':
            sql_specific_columns['u_time_narrative'] = line[23:123]
            sql_specific_columns['u_file_record_number'] = line[123:128]
            sql_specific_columns['u_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Preferred Route (T)
def import_arinc_424_et(sql_cursor, line):
    sql_table = 'enroute_preferred_routes'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    # = line[6:13]
    sql_common_columns['route_identifier'] = line[13:23]
    sql_common_columns['preferred_route_use_ind'] = line[22:25]
    sql_common_columns['sequence_number'] = line[25:29]
    # = line[29:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['to_fix_identifier_identifier'] = line[39:44]
        sql_specific_columns['to_fix_identifier_icao_code'] = line[44:46]
        sql_specific_columns['to_fix_identifier_section_code'] = line[46:47]
        sql_specific_columns['to_fix_identifier_subsection_code'] = line[47:48]
        sql_specific_columns['via_code'] = line[48:51]
        sql_specific_columns['sid_star_awy_identifier'] = line[51:57]
        sql_specific_columns['area_code'] = line[57:60]
        sql_specific_columns['level'] = line[60:61]
        sql_specific_columns['route_type'] = line[61:62]
        sql_specific_columns['initial_airport_fix_identifier'] = line[62:67]
        sql_specific_columns['initial_airport_fix_icao_code'] = line[67:69]
        sql_specific_columns['initial_airport_fix_section_code'] = line[69:70]
        sql_specific_columns['initial_airport_fix_subsection_code'] = line[70:71]
        sql_specific_columns['terminus_airport_fix_identifier'] = line[71:76]
        sql_specific_columns['terminus_airport_fix_icao_code'] = line[76:78]
        sql_specific_columns['terminus_airport_fix_section_code'] = line[78:79]
        sql_specific_columns['terminus_airport_fix_subsection_code'] = line[79:80]
        sql_specific_columns['minimum_altitude'] = line[80:85]
        sql_specific_columns['maximum_altitude'] = line[85:90]
        sql_specific_columns['time_code'] = line[90:91]
        sql_specific_columns['aircraft_use_group'] = line[91:93]
        sql_specific_columns['direction_restriction'] = line[93:94]
        sql_specific_columns['altitude_description'] = line[94:95]
        sql_specific_columns['altitude_1'] = line[95:100]
        sql_specific_columns['altitude_2'] = line[100:105]
        sql_specific_columns['sid_star_app_awy_route_type'] = line[105:106]
        sql_specific_columns['s_s_a_route_type_qualifier_1'] = line[106:107]
        sql_specific_columns['s_s_a_route_type_qualifier_2'] = line[107:108]
        # = line[108:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'T':
            sql_specific_columns['t_time_code'] = line[40:41]
            sql_specific_columns['t_time_indicator'] = line[41:42]
            sql_specific_columns['t_time_of_operation_1'] = line[42:52]
            sql_specific_columns['t_time_of_operation_2'] = line[52:62]
            sql_specific_columns['t_time_of_operation_3'] = line[62:72]
            sql_specific_columns['t_time_of_operation_4'] = line[72:82]
            sql_specific_columns['t_time_of_operation_5'] = line[82:92]
            sql_specific_columns['t_time_of_operation_6'] = line[92:102]
            sql_specific_columns['t_time_of_operation_7'] = line[102:112]
            # = line[112:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'A':
            sql_specific_columns['a_notes'] = line[40:109]
            # = line[109:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airspace (U) - Controlled Airspace (C)
def import_arinc_424_uc(sql_cursor, line):
    sql_table = 'airspace_controlled_airspaces'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['airspace_icao_code'] = line[6:8]
    sql_common_columns['airspace_type'] = line[8:9]
    sql_common_columns['airspace_center_identifier'] = line[9:14]
    sql_common_columns['airspace_section_code'] = line[14:15]
    sql_common_columns['airspace_subsection_code'] = line[15:16]
    sql_common_columns['airspace_classification'] = line[16:17]
    # = line[17:19]
    sql_common_columns['multiple_code'] = line[19:20]
    sql_common_columns['sequence_number'] = line[20:24]
    continuation_record_no = line[24:25]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['level'] = line[25:26]
        sql_specific_columns['time_code'] = line[26:27]
        sql_specific_columns['notam'] = line[27:28]
        # = line[28:30]
        sql_specific_columns['boundary_via'] = line[30:32]
        sql_specific_columns['latitude'] = line[32:41]
        sql_specific_columns['longitude'] = line[41:51]
        sql_specific_columns['arc_origin_latitude'] = line[51:60]
        sql_specific_columns['arc_origin_longitude'] = line[60:70]
        sql_specific_columns['arc_distance'] = line[70:74]
        sql_specific_columns['arc_bearing'] = line[74:78]
        sql_specific_columns['rnp'] = line[78:81]
        sql_specific_columns['lower_limit'] = line[81:86]
        sql_specific_columns['lower_limit_unit_indicator'] = line[86:87]
        sql_specific_columns['upper_limit'] = line[87:92]
        sql_specific_columns['upper_limit_unit_indicator'] = line[92:93]
        sql_specific_columns['controlled_airspace_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[25:26]
        if application_type == 'T':
            sql_specific_columns['t_time_code'] = line[26:27]
            sql_specific_columns['t_notam'] = line[27:28]
            sql_specific_columns['t_time_indicator'] = line[28:29]
            sql_specific_columns['t_time_of_operation_1'] = line[29:39]
            sql_specific_columns['t_time_of_operation_2'] = line[39:49]
            sql_specific_columns['t_time_of_operation_3'] = line[49:59]
            sql_specific_columns['t_time_of_operation_4'] = line[59:69]
            sql_specific_columns['t_time_of_operation_5'] = line[69:79]
            sql_specific_columns['t_time_of_operation_6'] = line[79:89]
            sql_specific_columns['t_time_of_operation_7'] = line[89:99]
            # = line[99:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'E':
            sql_specific_columns['e_speed_limit_1'] = line[26:29]
            sql_specific_columns['e_speed_limit_altitude_1'] = line[29:34]
            sql_specific_columns['e_speed_limit_aircraft_category_type_1'] = line[34:35]
            sql_specific_columns['e_speed_limit_2'] = line[35:38]
            sql_specific_columns['e_speed_limit_altitude_2'] = line[38:43]
            sql_specific_columns['e_speed_limit_aircraft_category_type_2'] = line[43:44]
            # = line[44:123]
            sql_specific_columns['file_record_number'] = line[123:128]
            sql_specific_columns['cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Tables (T) - Geographical Reference (G)
def import_arinc_424_tg(sql_cursor, line):
    sql_table = 'tables_geographical_references'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['geographical_ref_table_identifier'] = line[6:8]
    sql_common_columns['sequence_number'] = line[8:9]
    sql_common_columns['geographical_entity'] = line[9:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[39:40]
        sql_specific_columns['preferred_route_ident_1'] = line[40:50]
        sql_specific_columns['preferred_route_use_ind_1'] = line[50:52]
        sql_specific_columns['preferred_route_ident_2'] = line[52:62]
        sql_specific_columns['preferred_route_use_ind_2'] = line[62:64]
        sql_specific_columns['preferred_route_ident_3'] = line[64:74]
        sql_specific_columns['preferred_route_use_ind_3'] = line[74:76]
        sql_specific_columns['preferred_route_ident_4'] = line[76:86]
        sql_specific_columns['preferred_route_use_ind_4'] = line[86:88]
        sql_specific_columns['preferred_route_ident_5'] = line[88:98]
        sql_specific_columns['preferred_route_use_ind_5'] = line[98:100]
        sql_specific_columns['preferred_route_ident_6'] = line[100:110]
        sql_specific_columns['preferred_route_use_ind_6'] = line[110:112]
        # = line[112:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[40:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Flight Planning Arrival/Departure (R)
def import_arinc_424_pr(sql_cursor, line):
    sql_table = 'airport_flight_planning_arrival_departure_data'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['sid_star_approach_identifier'] = line[13:19]
    sql_common_columns['procedure_type'] = line[19:20]
    sql_common_columns['runway_transition_identifier'] = line[20:25]
    sql_common_columns['runway_transition_fix_identifier'] = line[25:30]
    sql_common_columns['runway_transition_fix_icao_code'] = line[30:32]
    sql_common_columns['runway_transition_fix_section_code'] = line[32:33]
    sql_common_columns['runway_transition_fix_subsection_code'] = line[33:34]
    sql_common_columns['runway_transition_along_track_distance'] = line[34:37]
    sql_common_columns['common_segment_transition_fix_identifier'] = line[37:42]
    sql_common_columns['common_segment_transition_fix_icao_code'] = line[42:44]
    sql_common_columns['common_segment_transition_fix_section_code'] = line[44:45]
    sql_common_columns['common_segment_transition_fix_subsection_code'] = line[45:46]
    sql_common_columns['common_segment_along_track_distance'] = line[46:49]
    sql_common_columns['enroute_transition_identifier'] = line[49:54]
    sql_common_columns['enroute_transition_fix_identifier'] = line[54:59]
    sql_common_columns['enroute_transition_fix_icao_code'] = line[59:61]
    sql_common_columns['enroute_transition_fix_section_code'] = line[61:62]
    sql_common_columns['enroute_transition_fixsubsection_code'] = line[62:63]
    sql_common_columns['enroute_transition_along_track_distance'] = line[63:66]
    sql_common_columns['sequence_number'] = line[66:69]
    continuation_record_no = line[69:70]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['number_of_engines'] = line[70:74]
        sql_specific_columns['turboprop_jet_indicator'] = line[74:75]
        sql_specific_columns['rnav_flag'] = line[75:76]
        sql_specific_columns['atc_weight_category'] = line[76:77]
        sql_specific_columns['atc_identifier'] = line[77:84]
        sql_specific_columns['time_code'] = line[84:85]
        sql_specific_columns['procedure_description'] = line[85:100]
        sql_specific_columns['leg_type_code'] = line[100:102]
        sql_specific_columns['reporting_code'] = line[102:103]
        sql_specific_columns['initial_departure_magnetic_course'] = line[103:107]
        sql_specific_columns['altitude_description'] = line[107:108]
        sql_specific_columns['altitude_1'] = line[108:111]
        sql_specific_columns['altitude_2'] = line[111:114]
        sql_specific_columns['speed_limit'] = line[114:117]
        sql_specific_columns['initial_cruise_table'] = line[117:119]
        sql_specific_columns['speed_limit_description'] = line[119:120]
        # = line[120:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[70:71]
        if application_type == 'E':
            sql_specific_columns['e_intermediate_fix_identifier_1'] = line[71:76]
            sql_specific_columns['e_intermediate_fix_icao_code_1'] = line[76:78]
            sql_specific_columns['e_intermediate_fix_section_code_1'] = line[78:79]
            sql_specific_columns['e_intermediate_fix_subsection_code_1'] = line[79:80]
            sql_specific_columns['e_intermediate_distance_atd_1'] = line[80:83]
            sql_specific_columns['e_fix_related_transition_code_1'] = line[83:84]
            sql_specific_columns['e_intermediate_fix_identifier_2'] = line[84:89]
            sql_specific_columns['e_intermediate_fix_icao_code_2'] = line[89:91]
            sql_specific_columns['e_intermediate_fix_section_code_2'] = line[91:92]
            sql_specific_columns['e_intermediate_fix_subsection_code_2'] = line[92:93]
            sql_specific_columns['e_intermediate_distance_atd_2'] = line[93:96]
            sql_specific_columns['e_fix_related_transition_code_2'] = line[96:97]
            sql_specific_columns['e_intermediate_fix_identifier_3'] = line[97:102]
            sql_specific_columns['e_intermediate_fix_icao_code_3'] = line[102:104]
            sql_specific_columns['e_intermediate_fix_section_code_3'] = line[104:105]
            sql_specific_columns['e_intermediate_fix_subsection_code_3'] = line[105:106]
            sql_specific_columns['e_intermediate_distance_atd_3'] = line[106:109]
            sql_specific_columns['e_fix_related_transition_code_3'] = line[109:110]
            sql_specific_columns['e_intermediate_fix_identifier_4'] = line[110:115]
            sql_specific_columns['e_intermediate_fix_icao_code_4'] = line[115:117]
            sql_specific_columns['e_intermediate_fix_section_code_4'] = line[117:118]
            sql_specific_columns['e_intermediate_fix_subsection_code_4'] = line[118:119]
            sql_specific_columns['e_intermediate_distance_atd_4'] = line[119:122]
            sql_specific_columns['e_fix_related_transition_code_4'] = line[122:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'T':
            sql_specific_columns['t_time_code'] = line[71:72]
            sql_specific_columns['t_time_indicator'] = line[72:73]
            sql_specific_columns['t_time_of_operation_1'] = line[73:83]
            sql_specific_columns['t_time_of_operation_2'] = line[83:93]
            sql_specific_columns['t_time_of_operation_3'] = line[93:103]
            sql_specific_columns['t_time_of_operation_4'] = line[103:113]
            sql_specific_columns['t_time_of_operation_5'] = line[113:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - SBAS Path Point (P)
def import_arinc_424_pp(sql_cursor, line):
    sql_table = 'airport_sbas_path_points'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_procedure_identifier'] = line[13:19]
    sql_common_columns['runway_identifier_or_final_approach_course'] = line[19:24]
    sql_common_columns['operation_type'] = line[24:26]
    continuation_record_no = line[26:27]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['route_indicator'] = line[27:28]
        sql_specific_columns['sbas_service_provider_identifier'] = line[28:30]
        sql_specific_columns['reference_path_data_selector'] = line[30:32]
        sql_specific_columns['reference_path_identifier'] = line[32:36]
        sql_specific_columns['approach_performance_designator'] = line[36:37]
        sql_specific_columns['landing_threshold_point_latitude'] = line[37:48]
        sql_specific_columns['landing_threshold_point_longitude'] = line[48:60]
        sql_specific_columns['ltp_ellipsoid_height'] = line[60:66]
        sql_specific_columns['glide_path_angle'] = line[66:70]
        sql_specific_columns['flight_path_alignment_point_latitude'] = line[70:81]
        sql_specific_columns['flight_path_alignment_point_longitude'] = line[81:93]
        sql_specific_columns['course_width_at_threshold'] = line[93:98]
        sql_specific_columns['length_offset'] = line[98:102]
        sql_specific_columns['path_point_tch'] = line[102:108]
        sql_specific_columns['tch_units_indicator'] = line[108:109]
        sql_specific_columns['hal'] = line[109:112]
        sql_specific_columns['val'] = line[112:115]
        sql_specific_columns['sbas_fas_data_crc_remainder'] = line[115:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[27:28]
        if application_type == 'E':
            sql_specific_columns = {}
            sql_specific_columns['e_fpap_ellipsoid_height'] = line[28:34]
            sql_specific_columns['e_fpap_orthometric_height'] = line[34:40]
            sql_specific_columns['e_ltp_orthometric_height'] = line[40:46]
            sql_specific_columns['e_approach_type_identifier'] = line[46:56]
            sql_specific_columns['e_gnss_channel_number'] = line[56:61]
            sql_specific_columns['e_helicopter_procedure_course'] = line[61:65]
            # = line[65:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - GLS Station (T)
def import_arinc_424_pt(sql_cursor, line):
    sql_table = 'airport_gls_stations'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_or_heliport_identifier'] = line[6:10]
    sql_common_columns['airport_or_heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['gls_ref_path_identifier'] = line[13:17]
    sql_common_columns['gls_category'] = line[17:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['gbas_sbas_channel'] = line[22:27]
        sql_specific_columns['runway_identifier'] = line[27:32]
        # = line[32:51]
        sql_specific_columns['gls_approach_bearing'] = line[51:55]
        sql_specific_columns['station_latitude'] = line[55:64]
        sql_specific_columns['station_longitude'] = line[64:74]
        sql_specific_columns['gls_station_identifier'] = line[74:78]
        # = line[78:83]
        sql_specific_columns['service_volume_radius'] = line[83:85]
        sql_specific_columns['tdma_slots'] = line[85:87]
        sql_specific_columns['gls_approach_slope'] = line[87:90]
        sql_specific_columns['magnetic_variation'] = line[90:95]
        # = line[95:97]
        sql_specific_columns['station_elevation'] = line[97:102]
        sql_specific_columns['datum_code'] = line[102:105]
        sql_specific_columns['station_type'] = line[105:108]
        # = line[108:110]
        sql_specific_columns['station_elevation_wgs_84'] = line[110:115]
        sql_specific_columns['glide_path_tch'] = line[115:118]
        # = line[118:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Company Routes (R) - Alternate (A)
def import_arinc_424_ra(sql_cursor, line):
    sql_table = 'company_routes_alternates'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:3]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['alternate_related_airport_or_fix_identifier'] = line[6:11]
    sql_common_columns['alternate_related_icao_code'] = line[11:13]
    sql_common_columns['alternate_related_section_code'] = line[13:14]
    sql_common_columns['alternate_related_subsection_code'] = line[14:15]
    sql_common_columns['alternate_record_type'] = line[15:17]
    # = line[17:19]
    sql_common_columns['distance_to_alternate'] = line[19:22]
    sql_common_columns['alternate_type'] = line[22:23]
    sql_common_columns['primary_alternate_identifier'] = line[23:33]
    # = line[33:35]
    sql_common_columns['distance_to_alternate_1'] = line[35:38]
    sql_common_columns['alternate_type_1'] = line[38:39]
    sql_common_columns['additional_alternate_identifier_1'] = line[39:49]
    # = line[49:51]
    sql_common_columns['distance_to_alternate_2'] = line[51:54]
    sql_common_columns['alternate_type_2'] = line[54:55]
    sql_common_columns['additional_alternate_identifier_2'] = line[55:65]
    # = line[65:67]
    sql_common_columns['distance_to_alternate_3'] = line[67:70]
    sql_common_columns['alternate_type_3'] = line[70:71]
    sql_common_columns['additional_alternate_identifier_3'] = line[71:81]
    # = line[81:83]
    sql_common_columns['distance_to_alternate_4'] = line[83:86]
    sql_common_columns['alternate_type_4'] = line[86:87]
    sql_common_columns['additional_alternate_identifier_4'] = line[87:97]
    # = line[97:99]
    sql_common_columns['distance_to_alternate_5'] = line[99:102]
    sql_common_columns['alternate_type_5'] = line[102:103]
    sql_common_columns['additional_alternate_identifier_5'] = line[103:113]
    # = line[113:123]
    sql_common_columns['file_record_number'] = line[123:128]
    sql_common_columns['cycle_date'] = line[128:132]
    sql_primary(sql_cursor, sql_table, sql_common_columns, {})


# Airport (P) - TAA (K)
def import_arinc_424_pk(sql_cursor, line):
    sql_table = 'airport_taa'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_identifier'] = line[13:19]
    sql_common_columns['taa_waypoint_identifier'] = line[19:24]
    sql_common_columns['taa_waypoint_icao_code'] = line[24:26]
    sql_common_columns['taa_waypoint_section_code'] = line[26:27]
    sql_common_columns['taa_waypoint_subsection_code'] = line[27:28]
    sql_common_columns['taa_fix_position_indicator'] = line[28:29]
    continuation_record_no = line[29:30]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[31:32]
        sql_specific_columns['sector_bearing_1'] = line[32:38]
        sql_specific_columns['sector_minimum_altitude_1'] = line[38:41]
        sql_specific_columns['sector_radius_1'] = line[41:45]
        sql_specific_columns['procedure_turn_indicator_1'] = line[45:46]
        sql_specific_columns['sector_bearing_2'] = line[46:52]
        sql_specific_columns['sector_minimum_altitude_2'] = line[52:55]
        sql_specific_columns['sector_radius_2'] = line[55:59]
        sql_specific_columns['procedure_turn_indicator_2'] = line[59:60]
        sql_specific_columns['sector_bearing_3'] = line[60:66]
        sql_specific_columns['sector_minimum_altitude_3'] = line[66:69]
        sql_specific_columns['sector_radius_3'] = line[69:73]
        sql_specific_columns['procedure_turn_indicator_3'] = line[73:74]
        sql_specific_columns['sector_bearing_4'] = line[74:80]
        sql_specific_columns['sector_minimum_altitude_4'] = line[80:83]
        sql_specific_columns['sector_radius_4'] = line[83:87]
        sql_specific_columns['procedure_turn_indicator_4'] = line[87:88]
        sql_specific_columns['sector_bearing_5'] = line[88:94]
        sql_specific_columns['sector_minimum_altitude_5'] = line[94:97]
        sql_specific_columns['sector_radius_5'] = line[97:101]
        sql_specific_columns['procedure_turn_indicator_5'] = line[101:102]
        sql_specific_columns['sector_bearing_reference_waypoint_identifier'] = line[102:107]
        sql_specific_columns['sector_bearing_reference_waypoint_icao_code'] = line[107:109]
        sql_specific_columns['sector_bearing_reference_waypoint_section_code'] = line[109:110]
        sql_specific_columns['sector_bearing_reference_waypoint_subsection_code'] = line[110:111]
        # = line[111:116]
        sql_specific_columns['procedure_design_aircraft_category_or_type'] = line[116:117]
        sql_specific_columns['approach_route_qualifier_1'] = line[117:118]
        sql_specific_columns['approach_route_qualifier_2'] = line[118:119]
        sql_specific_columns['mag_true_indicator'] = line[119:120]
        # = line[120:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[30:31]
        if application_type == '?':         # TODO
            # = line[31:32]
            sql_specific_columns['sector_bearing_1'] = line[32:38]
            sql_specific_columns['sector_minimum_altitude_1'] = line[38:41]
            sql_specific_columns['sector_radius_1'] = line[41:45]
            sql_specific_columns['procedure_turn_indicator_1'] = line[45:46]
            sql_specific_columns['sector_bearing_2'] = line[46:52]
            sql_specific_columns['sector_minimum_altitude_2'] = line[52:55]
            sql_specific_columns['sector_radius_2'] = line[55:59]
            sql_specific_columns['procedure_turn_indicator_2'] = line[59:60]
            sql_specific_columns['sector_bearing_3'] = line[60:66]
            sql_specific_columns['sector_minimum_altitude_3'] = line[66:69]
            sql_specific_columns['sector_radius_3'] = line[69:73]
            sql_specific_columns['procedure_turn_indicator_3'] = line[73:74]
            sql_specific_columns['sector_bearing_4'] = line[74:80]
            sql_specific_columns['sector_minimum_altitude_4'] = line[80:83]
            sql_specific_columns['sector_radius_4'] = line[83:87]
            sql_specific_columns['procedure_turn_indicator_4'] = line[87:88]
            sql_specific_columns['notes'] = line[88:109]
            # = line[109:116]
            sql_specific_columns['procedure_design_aircraft_category_or_type'] = line[116:117]
            sql_specific_columns['approach_route_qualifier_1'] = line[117:118]
            sql_specific_columns['approach_route_qualifier_2'] = line[118:119]
            # = line[119:123]
            sql_specific_columns['file_record_number'] = line[123:128]
            sql_specific_columns['cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Navaid (D) - TACAN-Only NAVAID (T)
def import_arinc_424_dt(sql_cursor, line):
    sql_table = 'navaid_tacan_only_navaids'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    # = line[12:13]
    sql_common_columns['vor_identifier'] = line[13:17]
    # = line[17:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['vor_frequency'] = line[22:27]
        sql_specific_columns['navaid_class'] = line[27:32]
        # = line[32:51]
        sql_specific_columns['tacan_identifier'] = line[51:55]
        sql_specific_columns['tacan_latitude'] = line[55:64]
        sql_specific_columns['tacan_longitude'] = line[64:74]
        sql_specific_columns['station_declination'] = line[74:79]
        sql_specific_columns['tacan_elevation'] = line[79:84]
        sql_specific_columns['navaid_useable_range'] = line[84:85]
        # = line[85:87]
        sql_specific_columns['frequency_protection'] = line[87:90]
        sql_specific_columns['datum_code'] = line[90:93]
        sql_specific_columns['tacan_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
        elif application_type == 'S':
            # = line[23:27]
            sql_specific_columns['s_facility_characteristics'] = line[27:32]
            # = line[32:74
            sql_specific_columns['s_magnetic_variation'] = line[74:79]
            sql_specific_columns['s_facility_elevation'] = line[79:84]
            # = line[84:123]
            sql_specific_columns['s_file_record_number'] = line[123:128]
            sql_specific_columns['s_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            # = line[43:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'L':
            sql_specific_columns['l_navaid_limitation_code'] = line[23:24]
            sql_specific_columns['l_component_affected_indicator'] = line[24:25]
            sql_specific_columns['l_sequence_number'] = line[25:27]
            sql_specific_columns['l_sector_from_sector_to_1'] = line[27:29]
            sql_specific_columns['l_distance_description_1'] = line[29:30]
            sql_specific_columns['l_distance_limitation_1'] = line[30:36]
            sql_specific_columns['l_altitude_description_1'] = line[36:37]
            sql_specific_columns['l_altitude_limitation_1'] = line[37:43]
            sql_specific_columns['l_sector_from_sector_to_2'] = line[43:45]
            sql_specific_columns['l_distance_description_2'] = line[45:46]
            sql_specific_columns['l_distance_limitation_2'] = line[46:52]
            sql_specific_columns['l_altitude_description_2'] = line[52:53]
            sql_specific_columns['l_altitude_limitation_2'] = line[53:59]
            sql_specific_columns['l_sector_from_sector_to_3'] = line[59:61]
            sql_specific_columns['l_distance_description_3'] = line[61:62]
            sql_specific_columns['l_distance_limitation_3'] = line[62:68]
            sql_specific_columns['l_altitude_description_3'] = line[68:69]
            sql_specific_columns['l_altitude_limitation_3'] = line[69:75]
            sql_specific_columns['l_sector_from_sector_to_4'] = line[75:77]
            sql_specific_columns['l_distance_description_4'] = line[77:78]
            sql_specific_columns['l_distance_limitation_4'] = line[78:84]
            sql_specific_columns['l_altitude_description_4'] = line[84:85]
            sql_specific_columns['l_altitude_limitation_4'] = line[85:91]
            sql_specific_columns['l_sector_from_sector_to_5'] = line[91:93]
            sql_specific_columns['l_distance_description_5'] = line[93:94]
            sql_specific_columns['l_distance_limitation_5'] = line[94:100]
            sql_specific_columns['l_altitude_description_5'] = line[100:101]
            sql_specific_columns['l_altitude_limitation_5'] = line[101:107]
            sql_specific_columns['l_sequence_end_indicator'] = line[107:108]
            # = line[108:123]
            sql_specific_columns['l_file_record_number'] = line[123:128]
            sql_specific_columns['l_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Enroute (E) - Special Activity Area (S)
def import_arinc_424_es(sql_cursor, line):
    sql_table = 'enroute_special_activity_areas'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['saa_type'] = line[6:7]
    sql_common_columns['saa_identifier'] = line[7:13]
    sql_common_columns['saa_icao_code'] = line[13:15]
    sql_common_columns['airport_identifier'] = line[15:19]
    sql_common_columns['airport_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[22:23]
        sql_specific_columns['latitude'] = line[23:32]
        sql_specific_columns['longitude'] = line[32:42]
        sql_specific_columns['saa_size'] = line[42:45]
        sql_specific_columns['upper_limit'] = line[45:51]
        sql_specific_columns['upper_limit_unit_indicator'] = line[51:52]
        sql_specific_columns['saa_volume'] = line[52:53]
        sql_specific_columns['operating_times'] = line[53:56]
        sql_specific_columns['public_or_military'] = line[56:57]
        # = line[57:58]
        sql_specific_columns['controlling_agency'] = line[58:83]
        sql_specific_columns['communication_type'] = line[83:86]
        sql_specific_columns['communication_frequency'] = line[86:93]
        sql_specific_columns['special_activity_area_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Tables (T) - Communication Type Translation (V)
def import_arinc_424_tv(sql_cursor, line):
    sql_table = 'tables_communication_type_translations'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    # = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['communication_type'] = line[6:9]
    sql_common_columns['type_recognized_by'] = line[9:10]
    sql_common_columns['translation'] = line[10:90]
    sql_common_columns['used_on'] = line[90:91]
    sql_common_columns['communication_class'] = line[91:95]
    # = line[95:123]
    sql_common_columns['file_record_number'] = line[123:128]
    sql_common_columns['cycle_date'] = line[128:132]
    sql_primary(sql_cursor, sql_table, sql_common_columns, {})


# Airport (P) - GBAS Path Point (Q)
def import_arinc_424_pq(sql_cursor, line):
    sql_table = 'airport_gbas_path_points'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_procedure_identifier'] = line[13:19]
    sql_common_columns['runway_or_helipad_identifier'] = line[19:24]
    sql_common_columns['operation_type'] = line[24:26]
    continuation_record_no = line[26:27]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['route_indicator'] = line[27:28]
        sql_specific_columns['sbas_service_provider_identifier'] = line[28:30]
        sql_specific_columns['reference_path_data_selector'] = line[30:32]
        sql_specific_columns['reference_path_identifier'] = line[32:36]
        sql_specific_columns['approach_performance_designator'] = line[36:37]
        sql_specific_columns['landing_threshold_point_latitude'] = line[37:48]
        sql_specific_columns['landing_threshold_point_longitude'] = line[48:60]
        sql_specific_columns['ltp_ellipsoid_height'] = line[60:66]
        sql_specific_columns['glide_path_angle'] = line[66:70]
        sql_specific_columns['flight_path_alignment_point_latitude'] = line[70:81]
        sql_specific_columns['flight_path_alignment_point_longitude'] = line[81:93]
        sql_specific_columns['course_width_at_threshold'] = line[93:98]
        sql_specific_columns['length_offset'] = line[98:102]
        sql_specific_columns['path_point_tch'] = line[102:108]
        sql_specific_columns['tch_units_indicator'] = line[108:109]
        # = line[109:115]
        sql_specific_columns['gbas_fas_data_crc_remainder'] = line[115:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[27:28]
        if application_type == '?':                             # TODO
            sql_specific_columns = {}
            sql_specific_columns['fpap_ellipsoid_height'] = line[28:34]
            sql_specific_columns['fpap_orthometric_height'] = line[34:40]
            sql_specific_columns['ltp_orthometric_height'] = line[40:46]
            sql_specific_columns['approach_type_identifier'] = line[46:56]
            sql_specific_columns['gnss_channel_number'] = line[56:61]
            # = line[61:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Airport (P) - Helipad (H)
def import_arinc_424_ph(sql_cursor, line):
    sql_table = 'airport_helipads'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_or_heliport_identifier'] = line[6:10]
    sql_common_columns['airport_or_heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['helipad_identifier'] = line[13:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['helipad_shape'] = line[22:23]
        sql_specific_columns['helipad_tlof_dimension'] = line[23:31]
        # = line[31:32]
        sql_specific_columns['helipad_latitude'] = line[32:41]
        sql_specific_columns['helipad_longitude'] = line[41:51]
        sql_specific_columns['helipad_surface_code'] = line[51:52]
        sql_specific_columns['helipad_surface_type'] = line[52:56]
        sql_specific_columns['max_allowable_helicopter_weight'] = line[56:59]
        sql_specific_columns['helicopter_performance_requirement'] = line[59:60]
        sql_specific_columns['helipad_maximum_rotor_diameter'] = line[60:63]
        sql_specific_columns['helipad_type'] = line[63:64]
        # = line[64:65]
        sql_specific_columns['helipad_elevation'] = line[65:70]
        sql_specific_columns['helipad_fato_dimension'] = line[70:78]
        sql_specific_columns['safety_area_dimension'] = line[78:86]
        sql_specific_columns['helipad_orientation'] = line[86:91]
        sql_specific_columns['helipad_identifier_orientation'] = line[91:96]
        sql_specific_columns['preferred_approach_bearing_1'] = line[96:100]
        sql_specific_columns['preferred_approach_bearing_2'] = line[100:104]
        # = line[104:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Tables (T) - ATN Data (ATN NSAP) (L)
def import_aring_424_tl(sql_cursor, line):
    sql_table = 'tables_atn_data'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    # = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['ground_facility_identifier'] = line[6:14]
    # = line[14:18]
    continuation_record_noc = line[18:19]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['authority_and_format_identifier'] = line[19:21]
        sql_specific_columns['initial_domain_identifier'] = line[21:25]
        sql_specific_columns['version_identifier'] = line[25:27]
        sql_specific_columns['administrative_identifier'] = line[27:33]
        sql_specific_columns['routing_domain_format'] = line[33:35]
        sql_specific_columns['administrative_region_selector'] = line[35:41]
        sql_specific_columns['location_identifier'] = line[41:45]
        sql_specific_columns['system_identifier'] = line[45:57]
        sql_specific_columns['nsap_selector'] = line[57:59]
        sql_specific_columns['cm_transport_selector'] = line[59:63]
        sql_specific_columns['use_indicator'] = line[63:64]
        sql_specific_columns['fir_uir_name'] = line[64:89]
        # = line[89:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - Reference Point (A)
def import_arinc_424_ha(sql_cursor, line):
    sql_table = 'heliport_reference_points'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['ata_iata_designator'] = line[13:16]
    sql_common_columns['pad_identifier'] = line[16:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['speed_limit_altitude'] = line[22:27]
        sql_specific_columns['datum_code'] = line[27:30]
        sql_specific_columns['ifr_capability'] = line[30:31]
        sql_specific_columns['heliport_type'] = line[31:32]
        sql_specific_columns['heliport_reference_point_latitude'] = line[32:41]
        sql_specific_columns['heliport_reference_point_longitude'] = line[41:51]
        sql_specific_columns['magnetic_variation'] = line[51:56]
        sql_specific_columns['heliport_elevation'] = line[56:61]
        sql_specific_columns['speed_limit'] = line[61:64]
        sql_specific_columns['recommended_navaid_identifier'] = line[64:68]
        sql_specific_columns['recommended_navaid_icao_code'] = line[68:70]
        sql_specific_columns['transition_altitude'] = line[70:75]
        sql_specific_columns['transition_level'] = line[75:80]
        sql_specific_columns['public_military_indicator'] = line[80:81]
        sql_specific_columns['time_zone'] = line[81:84]
        sql_specific_columns['daylight_indicator'] = line[84:85]
        sql_specific_columns['pad_dimensions'] = line[85:91]
        sql_specific_columns['magnetic_true_indicator'] = line[91:92]
        # = line[92:93]
        sql_specific_columns['heliport_name'] = line[93:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            sql_specific_columns['a_notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:66]
            sql_specific_columns['p_controlled_airspace_indicator'] = line[66:67]
            sql_specific_columns['p_controlled_airspace_airport_identifier'] = line[67:71]
            sql_specific_columns['p_controlled_airspace_airport_icao_code'] = line[71:73]
            # = line[73:123]
            sql_specific_columns['file_record_number'] = line[123:128]
            sql_specific_columns['cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - Terminal Waypoint (C)
def import_arinc_424_hc(sql_cursor, line):
    sql_table = 'heliport_terminal_waypoints'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['waypoint_identifier'] = line[13:18]
    # = line[18:19]
    sql_common_columns['waypoint_icao_code'] = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[22:26]
        sql_specific_columns['waypoint_type'] = line[26:29]
        # = line[29:30]
        sql_specific_columns['waypoint_usage'] = line[30:31]
        # = line[31:32]
        sql_specific_columns['waypoint_latitude'] = line[32:41]
        sql_specific_columns['waypoint_longitude'] = line[41:51]
        # = line[51:74]
        sql_specific_columns['dynamic_magnetic_variation'] = line[74:79]
        # = line[79:84]
        sql_specific_columns['datum_code'] = line[84:87]
        # = line[87:95]
        sql_specific_columns['name_format_indicator'] = line[95:98]
        sql_specific_columns['waypoint_name_description'] = line[98:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[22:23]
        if application_type == 'A':
            # sql_specific_columns['notes'] = line[23:92]
            # = line[92:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            sql_specific_columns['p_fir_identifier'] = line[23:27]
            sql_specific_columns['p_uir_identifier'] = line[27:31]
            # = line[31:43]
            # = line[43:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - SID (D)
def import_arinc_424_hd(sql_cursor, line):
    sql_table = 'heliport_sids'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['sid_identifier'] = line[13:19]
    sql_common_columns['route_type'] = line[19:20]
    sql_common_columns['transition_identifier'] = line[20:25]
    sql_common_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
    sql_common_columns['sequence_number'] = line[26:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['rnp'] = line[44:47]
        sql_specific_columns['path_and_termination'] = line[47:49]
        sql_specific_columns['turn_direction_valid'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['arc_radius'] = line[56:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_holding_distance_or_time'] = line[74:78]
        sql_specific_columns['recommended_navaid_section_code'] = line[78:79]
        sql_specific_columns['recommended_navaid_subsection_code'] = line[79:80]
        sql_specific_columns['inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_specific_columns['altitude_description'] = line[82:83]
        sql_specific_columns['atc_indicator'] = line[83:84]
        sql_specific_columns['altitude_1'] = line[84:89]
        sql_specific_columns['altitude_2'] = line[89:94]
        sql_specific_columns['transition_altitude'] = line[94:99]
        sql_specific_columns['speed_limit'] = line[99:102]
        sql_specific_columns['vertical_angle'] = line[102:106]
        sql_specific_columns['center_fix_or_taa_procedure_turn_indicator'] = line[106:111]
        sql_specific_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_specific_columns['taa_icao_code'] = line[112:114]
        sql_specific_columns['taa_section_code'] = line[114:115]
        sql_specific_columns['taa_subsection_code'] = line[115:116]
        sql_specific_columns['gnss_fms_indication'] = line[116:117]
        sql_specific_columns['speed_limit_description'] = line[117:118]
        sql_specific_columns['route_qualifier_1'] = line[118:119]
        sql_specific_columns['route_qualifier_2'] = line[119:120]
        sql_specific_columns['route_qualifier_3'] = line[120:121]
        sql_specific_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            sql_specific_columns['e_procedure_tch'] = line[40:43]
            # = line[43:60]
            sql_specific_columns['e_procedure_design_mag_var'] = line[60:65]
            sql_specific_columns['e_procedure_design_mag_var_indicator'] = line[65:66]
            sql_specific_columns['e_procedure_referenced_fix_identifier_1'] = line[66:71]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_1'] = line[71:73]
            sql_specific_columns['e_procedure_referenced_fix_section_code_1'] = line[73:74]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_1'] = line[74:75]
            sql_specific_columns['e_procedure_referenced_fix_identifier_2'] = line[75:80]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_2'] = line[80:82]
            sql_specific_columns['e_procedure_referenced_fix_section_code_2'] = line[82:83]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_2'] = line[83:84]
            sql_specific_columns['e_procedure_referenced_fix_identifier_3'] = line[84:89]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_3'] = line[89:91]
            sql_specific_columns['e_procedure_referenced_fix_section_code_3'] = line[91:92]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_3'] = line[92:93]
            sql_specific_columns['e_procedure_referenced_fix_identifier_4'] = line[93:98]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_4'] = line[98:100]
            sql_specific_columns['e_procedure_referenced_fix_section_code_4'] = line[100:101]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_4'] = line[101:102]
            sql_specific_columns['e_cat_a_radii'] = line[102:104]
            # = line[104:110]
            sql_specific_columns['e_special_indicator'] = line[110:111]
            # = line[111:115]
            sql_specific_columns['e_vertical_scale_factor'] = line[115:118]
            sql_specific_columns['e_route_qualifier_1'] = line[118:119]
            sql_specific_columns['e_route_qualifier_2'] = line[119:120]
            sql_specific_columns['e_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:74]
            sql_specific_columns['p_leg_distance'] = line[74:78]
            # = line[78:118]
            sql_specific_columns['p_route_qualifier_1'] = line[118:119]
            sql_specific_columns['p_route_qualifier_2'] = line[119:120]
            sql_specific_columns['p_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'W':
            sql_specific_columns['w_fas_block_provided'] = line[40:41]
            sql_specific_columns['w_fas_block_provided_lev_of_service_name'] = line[41:51]
            sql_specific_columns['w_lnav_vnav_authorized'] = line[51:52]
            sql_specific_columns['w_lnav_vnav_level_of_service_name'] = line[52:62]
            sql_specific_columns['w_lnav_authorized'] = line[62:63]
            sql_specific_columns['w_lnav_level_of_service_name'] = line[63:73]
            sql_specific_columns['w_remote_altimeter_flag'] = line[73:74]
            # = line[74:88]
            sql_specific_columns['w_rnp_authorized_1'] = line[88:89]
            sql_specific_columns['w_rnp_level_of_service_value_1'] = line[89:92]
            sql_specific_columns['w_rnp_authorized_2'] = line[92:93]
            sql_specific_columns['w_rnp_level_of_service_value_2'] = line[93:96]
            sql_specific_columns['w_rnp_authorized_3'] = line[96:97]
            sql_specific_columns['w_rnp_level_of_service_value_3'] = line[97:100]
            sql_specific_columns['w_rnp_authorized_4'] = line[100:101]
            sql_specific_columns['w_rnp_level_of_service_value_4'] = line[101:104]
            # = line[104:118]
            sql_specific_columns['w_route_qualifier_1'] = line[118:119]
            sql_specific_columns['w_route_qualifier_2'] = line[119:120]
            sql_specific_columns['w_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['w_file_record_number'] = line[123:128]
            sql_specific_columns['w_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - STAR (E)
def import_arinc_424_he(sql_cursor, line):
    sql_table = 'heliport_stars'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['star_identifier'] = line[13:19]
    sql_common_columns['route_type'] = line[19:20]
    sql_common_columns['transition_identifier'] = line[20:25]
    sql_common_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
    sql_common_columns['sequence_number'] = line[26:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['rnp'] = line[44:47]
        sql_specific_columns['path_and_termination'] = line[47:49]
        sql_specific_columns['turn_direction_valid'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['arc_radius'] = line[56:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_holding_distance_or_time'] = line[74:78]
        sql_specific_columns['recommended_navaid_section_code'] = line[78:79]
        sql_specific_columns['recommended_navaid_subsection_code'] = line[79:80]
        sql_specific_columns['inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_specific_columns['altitude_description'] = line[82:83]
        sql_specific_columns['atc_indicator'] = line[83:84]
        sql_specific_columns['altitude_1'] = line[84:89]
        sql_specific_columns['altitude_2'] = line[89:94]
        sql_specific_columns['transition_altitude'] = line[94:99]
        sql_specific_columns['speed_limit'] = line[99:102]
        sql_specific_columns['vertical_angle'] = line[102:106]
        sql_specific_columns['center_fix_or_taa_procedure_turn_indicator'] = line[106:111]
        sql_specific_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_specific_columns['taa_icao_code'] = line[112:114]
        sql_specific_columns['taa_section_code'] = line[114:115]
        sql_specific_columns['taa_subsection_code'] = line[115:116]
        sql_specific_columns['gnss_fms_indication'] = line[116:117]
        sql_specific_columns['speed_limit_description'] = line[117:118]
        sql_specific_columns['route_qualifier_1'] = line[118:119]
        sql_specific_columns['route_qualifier_2'] = line[119:120]
        sql_specific_columns['route_qualifier_3'] = line[120:121]
        sql_specific_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            sql_specific_columns['e_procedure_tch'] = line[40:43]
            # = line[43:60]
            sql_specific_columns['e_procedure_design_mag_var'] = line[60:65]
            sql_specific_columns['e_procedure_design_mag_var_indicator'] = line[65:66]
            sql_specific_columns['e_procedure_referenced_fix_identifier_1'] = line[66:71]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_1'] = line[71:73]
            sql_specific_columns['e_procedure_referenced_fix_section_code_1'] = line[73:74]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_1'] = line[74:75]
            sql_specific_columns['e_procedure_referenced_fix_identifier_2'] = line[75:80]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_2'] = line[80:82]
            sql_specific_columns['e_procedure_referenced_fix_section_code_2'] = line[82:83]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_2'] = line[83:84]
            sql_specific_columns['e_procedure_referenced_fix_identifier_3'] = line[84:89]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_3'] = line[89:91]
            sql_specific_columns['e_procedure_referenced_fix_section_code_3'] = line[91:92]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_3'] = line[92:93]
            sql_specific_columns['e_procedure_referenced_fix_identifier_4'] = line[93:98]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_4'] = line[98:100]
            sql_specific_columns['e_procedure_referenced_fix_section_code_4'] = line[100:101]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_4'] = line[101:102]
            sql_specific_columns['e_cat_a_radii'] = line[102:104]
            # = line[104:110]
            sql_specific_columns['e_special_indicator'] = line[110:111]
            # = line[111:115]
            sql_specific_columns['e_vertical_scale_factor'] = line[115:118]
            sql_specific_columns['e_route_qualifier_1'] = line[118:119]
            sql_specific_columns['e_route_qualifier_2'] = line[119:120]
            sql_specific_columns['e_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:74]
            sql_specific_columns['p_leg_distance'] = line[74:78]
            # = line[78:118]
            sql_specific_columns['p_route_qualifier_1'] = line[118:119]
            sql_specific_columns['p_route_qualifier_2'] = line[119:120]
            sql_specific_columns['p_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'W':
            sql_specific_columns['w_fas_block_provided'] = line[40:41]
            sql_specific_columns['w_fas_block_provided_lev_of_service_name'] = line[41:51]
            sql_specific_columns['w_lnav_vnav_authorized'] = line[51:52]
            sql_specific_columns['w_lnav_vnav_level_of_service_name'] = line[52:62]
            sql_specific_columns['w_lnav_authorized'] = line[62:63]
            sql_specific_columns['w_lnav_level_of_service_name'] = line[63:73]
            sql_specific_columns['w_remote_altimeter_flag'] = line[73:74]
            # = line[74:88]
            sql_specific_columns['w_rnp_authorized_1'] = line[88:89]
            sql_specific_columns['w_rnp_level_of_service_value_1'] = line[89:92]
            sql_specific_columns['w_rnp_authorized_2'] = line[92:93]
            sql_specific_columns['w_rnp_level_of_service_value_2'] = line[93:96]
            sql_specific_columns['w_rnp_authorized_3'] = line[96:97]
            sql_specific_columns['w_rnp_level_of_service_value_3'] = line[97:100]
            sql_specific_columns['w_rnp_authorized_4'] = line[100:101]
            sql_specific_columns['w_rnp_level_of_service_value_4'] = line[101:104]
            # = line[104:118]
            sql_specific_columns['w_route_qualifier_1'] = line[118:119]
            sql_specific_columns['w_route_qualifier_2'] = line[119:120]
            sql_specific_columns['w_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['w_file_record_number'] = line[123:128]
            sql_specific_columns['w_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - Approach Procedure (F)
def import_arinc_424_hf(sql_cursor, line):
    sql_table = 'heliport_approach_procedures'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_identifier'] = line[13:19]
    sql_common_columns['route_type'] = line[19:20]
    sql_common_columns['transition_identifier'] = line[20:25]
    sql_common_columns['procedure_design_aircraft_category_or_type'] = line[25:26]
    sql_common_columns['sequence_number'] = line[26:29]
    sql_common_columns['fix_identifier'] = line[29:34]
    sql_common_columns['fix_icao_code'] = line[34:36]
    sql_common_columns['fix_section_code'] = line[36:37]
    sql_common_columns['fix_subsection_code'] = line[37:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['waypoint_description_code'] = line[39:43]
        sql_specific_columns['turn_direction'] = line[43:44]
        sql_specific_columns['rnp'] = line[44:47]
        sql_specific_columns['path_and_termination'] = line[47:49]
        sql_specific_columns['turn_direction_valid'] = line[49:50]
        sql_specific_columns['recommended_navaid_identifier'] = line[50:54]
        sql_specific_columns['recommended_navaid_icao_code'] = line[54:56]
        sql_specific_columns['arc_radius'] = line[56:62]
        sql_specific_columns['theta'] = line[62:66]
        sql_specific_columns['rho'] = line[66:70]
        sql_specific_columns['magnetic_course'] = line[70:74]
        sql_specific_columns['route_distance_holding_distance_or_time'] = line[74:78]
        sql_specific_columns['recommended_navaid_section_code'] = line[78:79]
        sql_specific_columns['recommended_navaid_subsection_code'] = line[79:80]
        sql_specific_columns['inbound_outbound_indicator'] = line[80:81]
        # = line[81:82]
        sql_specific_columns['altitude_description'] = line[82:83]
        sql_specific_columns['atc_indicator'] = line[83:84]
        sql_specific_columns['altitude_1'] = line[84:89]
        sql_specific_columns['altitude_2'] = line[89:94]
        sql_specific_columns['transition_altitude'] = line[94:99]
        sql_specific_columns['speed_limit'] = line[99:102]
        sql_specific_columns['vertical_angle'] = line[102:106]
        sql_specific_columns['center_fix_or_taa_procedure_turn_indicator'] = line[106:111]
        sql_specific_columns['multiple_code_or_taa_sector_identifier'] = line[111:112]
        sql_specific_columns['taa_icao_code'] = line[112:114]
        sql_specific_columns['taa_section_code'] = line[114:115]
        sql_specific_columns['taa_subsection_code'] = line[115:116]
        sql_specific_columns['gnss_fms_indication'] = line[116:117]
        sql_specific_columns['speed_limit_description'] = line[117:118]
        sql_specific_columns['route_qualifier_1'] = line[118:119]
        sql_specific_columns['route_qualifier_2'] = line[119:120]
        sql_specific_columns['route_qualifier_3'] = line[120:121]
        sql_specific_columns['preferred_multiple_approach_indicator'] = line[121:122]
        # = line[122:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            sql_specific_columns['e_procedure_tch'] = line[40:43]
            # = line[43:60]
            sql_specific_columns['e_procedure_design_mag_var'] = line[60:65]
            sql_specific_columns['e_procedure_design_mag_var_indicator'] = line[65:66]
            sql_specific_columns['e_procedure_referenced_fix_identifier_1'] = line[66:71]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_1'] = line[71:73]
            sql_specific_columns['e_procedure_referenced_fix_section_code_1'] = line[73:74]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_1'] = line[74:75]
            sql_specific_columns['e_procedure_referenced_fix_identifier_2'] = line[75:80]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_2'] = line[80:82]
            sql_specific_columns['e_procedure_referenced_fix_section_code_2'] = line[82:83]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_2'] = line[83:84]
            sql_specific_columns['e_procedure_referenced_fix_identifier_3'] = line[84:89]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_3'] = line[89:91]
            sql_specific_columns['e_procedure_referenced_fix_section_code_3'] = line[91:92]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_3'] = line[92:93]
            sql_specific_columns['e_procedure_referenced_fix_identifier_4'] = line[93:98]
            sql_specific_columns['e_procedure_referenced_fix_icao_code_4'] = line[98:100]
            sql_specific_columns['e_procedure_referenced_fix_section_code_4'] = line[100:101]
            sql_specific_columns['e_procedure_referenced_fix_subsection_code_4'] = line[101:102]
            sql_specific_columns['e_cat_a_radii'] = line[102:104]
            # = line[104:110]
            sql_specific_columns['e_special_indicator'] = line[110:111]
            # = line[111:115]
            sql_specific_columns['e_vertical_scale_factor'] = line[115:118]
            sql_specific_columns['e_route_qualifier_1'] = line[118:119]
            sql_specific_columns['e_route_qualifier_2'] = line[119:120]
            sql_specific_columns['e_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'P':
            # = line[40:74]
            sql_specific_columns['p_leg_distance'] = line[74:78]
            # = line[78:118]
            sql_specific_columns['p_route_qualifier_1'] = line[118:119]
            sql_specific_columns['p_route_qualifier_2'] = line[119:120]
            sql_specific_columns['p_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['p_file_record_number'] = line[123:128]
            sql_specific_columns['p_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'W':
            sql_specific_columns['w_fas_block_provided'] = line[40:41]
            sql_specific_columns['w_fas_block_provided_lev_of_service_name'] = line[41:51]
            sql_specific_columns['w_lnav_vnav_authorized'] = line[51:52]
            sql_specific_columns['w_lnav_vnav_level_of_service_name'] = line[52:62]
            sql_specific_columns['w_lnav_authorized'] = line[62:63]
            sql_specific_columns['w_lnav_level_of_service_name'] = line[63:73]
            sql_specific_columns['w_remote_altimeter_flag'] = line[73:74]
            # = line[74:88]
            sql_specific_columns['w_rnp_authorized_1'] = line[88:89]
            sql_specific_columns['w_rnp_level_of_service_value_1'] = line[89:92]
            sql_specific_columns['w_rnp_authorized_2'] = line[92:93]
            sql_specific_columns['w_rnp_level_of_service_value_2'] = line[93:96]
            sql_specific_columns['w_rnp_authorized_3'] = line[96:97]
            sql_specific_columns['w_rnp_level_of_service_value_3'] = line[97:100]
            sql_specific_columns['w_rnp_authorized_4'] = line[100:101]
            sql_specific_columns['w_rnp_level_of_service_value_4'] = line[101:104]
            # = line[104:118]
            sql_specific_columns['w_route_qualifier_1'] = line[118:119]
            sql_specific_columns['w_route_qualifier_2'] = line[119:120]
            sql_specific_columns['w_route_qualifier_3'] = line[120:121]
            # = line[121:123]
            sql_specific_columns['w_file_record_number'] = line[123:128]
            sql_specific_columns['w_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - MSA (S)
def import_arinc_424_hs(sql_cursor, line):
    sql_table = 'heliport_msa'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['msa_center_identifier'] = line[13:18]
    sql_common_columns['msa_center_icao_code'] = line[18:20]
    sql_common_columns['msa_center_section_code'] = line[20:21]
    sql_common_columns['msa_center_subsection_code'] = line[21:22]
    sql_common_columns['multiple_code'] = line[22:23]
    # = line[23:38]
    continuation_record_no = line[38:39]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        # = line[39:42]
        sql_specific_columns['sector_bearing_1'] = line[42:48]
        sql_specific_columns['sector_altitude_1'] = line[48:51]
        sql_specific_columns['sector_radius_1'] = line[51:53]
        sql_specific_columns['sector_bearing_2'] = line[53:59]
        sql_specific_columns['sector_altitude_2'] = line[59:62]
        sql_specific_columns['sector_radius_2'] = line[62:64]
        sql_specific_columns['sector_bearing_3'] = line[64:70]
        sql_specific_columns['sector_altitude_3'] = line[70:73]
        sql_specific_columns['sector_radius_3'] = line[73:75]
        sql_specific_columns['sector_bearing_4'] = line[75:81]
        sql_specific_columns['sector_altitude_4'] = line[81:84]
        sql_specific_columns['sector_radius_4'] = line[84:86]
        sql_specific_columns['sector_bearing_5'] = line[86:92]
        sql_specific_columns['sector_altitude_5'] = line[92:95]
        sql_specific_columns['sector_radius_5'] = line[95:97]
        sql_specific_columns['sector_bearing_6'] = line[97:103]
        sql_specific_columns['sector_altitude_6'] = line[103:106]
        sql_specific_columns['sector_radius_6'] = line[106:108]
        sql_specific_columns['sector_bearing_7'] = line[108:114]
        sql_specific_columns['sector_altitude_7'] = line[114:117]
        sql_specific_columns['sector_radius_7'] = line[117:119]
        sql_specific_columns['magnetic_true_indicator'] = line[119:120]
        # = line[120:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[39:40]
        if application_type == 'E':
            # = line[40:42]
            sql_specific_columns['e_sector_bearing_1'] = line[42:48]
            sql_specific_columns['e_sector_altitude_1'] = line[48:51]
            sql_specific_columns['e_sector_radius_1'] = line[51:53]
            sql_specific_columns['e_sector_bearing_2'] = line[53:59]
            sql_specific_columns['e_sector_altitude_2'] = line[59:62]
            sql_specific_columns['e_sector_radius_2'] = line[62:64]
            sql_specific_columns['e_sector_bearing_3'] = line[64:70]
            sql_specific_columns['e_sector_altitude_3'] = line[70:73]
            sql_specific_columns['e_sector_radius_3'] = line[73:75]
            sql_specific_columns['e_sector_bearing_4'] = line[75:81]
            sql_specific_columns['e_sector_altitude_4'] = line[81:84]
            sql_specific_columns['e_sector_radius_4'] = line[84:86]
            sql_specific_columns['e_sector_bearing_5'] = line[86:92]
            sql_specific_columns['e_sector_altitude_5'] = line[92:95]
            sql_specific_columns['e_sector_radius_5'] = line[95:97]
            sql_specific_columns['e_sector_bearing_6'] = line[97:103]
            sql_specific_columns['e_sector_altitude_6'] = line[103:106]
            sql_specific_columns['e_sector_radius_6'] = line[106:108]
            sql_specific_columns['e_sector_bearing_7'] = line[108:114]
            sql_specific_columns['e_sector_altitude_7'] = line[114:117]
            sql_specific_columns['e_sector_radius_7'] = line[117:119]
            sql_specific_columns['e_magnetic_true_indicator'] = line[119:120]
            # = line[120:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'A':
            sql_specific_columns['a_notes'] = line[40:109]
            # = line[109:123]
            sql_specific_columns['a_file_record_number'] = line[123:128]
            sql_specific_columns['a_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - Communications (V)
def import_arinc_424_hv(sql_cursor, line):
    sql_table = 'heliport_communications'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    # = line[13:15]
    # = line[15:19]
    # = line[19:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['communication_types'] = line[22:25]
        sql_specific_columns['transmit_frequency'] = line[25:32]
        sql_specific_columns['receive_frequency'] = line[32:39]
        sql_specific_columns['frequency_units'] = line[39:40]
        sql_specific_columns['radar_units'] = line[40:41]
        sql_specific_columns['h24_indicator'] = line[41:42]
        sql_specific_columns['call_signs'] = line[42:67]
        sql_specific_columns['multi_sector_indicator'] = line[67:68]
        sql_specific_columns['sectorization'] = line[68:74]
        sql_specific_columns['sector_facility'] = line[74:78]
        sql_specific_columns['icao_code'] = line[78:80]
        sql_specific_columns['section_code'] = line[80:81]
        sql_specific_columns['subsection_code'] = line[81:82]
        sql_specific_columns['altitude_description_code'] = line[82:83]
        sql_specific_columns['communication_altitude_1'] = line[83:86]
        sql_specific_columns['communication_altitude_2'] = line[86:89]
        sql_specific_columns['distance_description_code'] = line[89:90]
        sql_specific_columns['communication_distance'] = line[90:92]
        sql_specific_columns['transmitter_'] = line[92:101]
        sql_specific_columns['transmitter_'] = line[101:111]
        sql_specific_columns['service_indicator'] = line[111:114]
        sql_specific_columns['modulation'] = line[114:115]
        sql_specific_columns['signal_emission'] = line[115:116]
        # = line[116:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[26:27]
        if application_type == 'E':
            sql_specific_columns['e_remote_facility_identifier'] = line[23:27]
            sql_specific_columns['e_remote_facility_icao_code'] = line[27:29]
            sql_specific_columns['e_remote_facility_section_code'] = line[29:30]
            sql_specific_columns['e_remote_facility_subsection_code'] = line[30:31]
            sql_specific_columns['e_transmitter_site_mag_var'] = line[31:36]
            sql_specific_columns['e_transmitter_site_elevation'] = line[36:41]
            sql_specific_columns['e_additional_sectorization_1'] = line[41:47]
            sql_specific_columns['e_additional_sectorization_1_altitude_description'] = line[47:48]
            sql_specific_columns['e_additional_sectorization_1_altitude_1'] = line[48:51]
            sql_specific_columns['e_additional_sectorization_1_altitude_2'] = line[51:54]
            sql_specific_columns['e_additional_sectorization_2'] = line[54:60]
            sql_specific_columns['e_additional_sectorization_2_altitude_description'] = line[60:61]
            sql_specific_columns['e_additional_sectorization_2_altitude_1'] = line[61:64]
            sql_specific_columns['e_additional_sectorization_2_altitude_2'] = line[64:67]
            # = line[67:123]
            sql_specific_columns['file_record_number'] = line[123:128]
            sql_specific_columns['cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'N':
            sql_specific_columns['n_sectorization_narrative'] = line[23:83]
            # = line[83:123]
            sql_specific_columns['n_file_record_number'] = line[123:128]
            sql_specific_columns['n_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'T':
            # = line[23:69]
            sql_specific_columns['t_time_of_operation_1'] = line[69:79]
            sql_specific_columns['t_time_of_operation_2'] = line[69:79]
            sql_specific_columns['t_time_of_operation_3'] = line[79:89]
            sql_specific_columns['t_time_of_operation_4'] = line[89:99]
            sql_specific_columns['t_time_of_operation_5'] = line[99:109]
            # = line[109:123]
            sql_specific_columns['t_file_record_number'] = line[123:128]
            sql_specific_columns['t_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        elif application_type == 'U':
            sql_specific_columns['u_time_narrative'] = line[23:123]
            sql_specific_columns['u_file_record_number'] = line[123:128]
            sql_specific_columns['u_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - TAA (K)
def import_arinc_424_hk(sql_cursor, line):
    sql_table = 'heliport_taa'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_identifier'] = line[13:19]
    sql_common_columns['taa_waypoint_identifier'] = line[19:24]
    sql_common_columns['taa_waypoint_icao_code'] = line[24:26]
    sql_common_columns['taa_waypoint_section_code'] = line[26:27]
    sql_common_columns['taa_waypoint_subsection_code'] = line[27:28]
    sql_common_columns['taa_fix_position_indicator'] = line[28:29]
    continuation_record_no = line[29:30]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['sector_bearing_1'] = line[32:38]
        sql_specific_columns['sector_minimum_altitude_1'] = line[38:41]
        sql_specific_columns['sector_radius_1'] = line[41:45]
        sql_specific_columns['procedure_turn_indicator_1'] = line[45:46]
        sql_specific_columns['sector_bearing_2'] = line[46:52]
        sql_specific_columns['sector_minimum_altitude_2'] = line[52:55]
        sql_specific_columns['sector_radius_2'] = line[55:59]
        sql_specific_columns['procedure_turn_indicator_2'] = line[59:60]
        sql_specific_columns['sector_bearing_3'] = line[60:66]
        sql_specific_columns['sector_minimum_altitude_3'] = line[66:69]
        sql_specific_columns['sector_radius_3'] = line[69:73]
        sql_specific_columns['procedure_turn_indicator_3'] = line[73:74]
        sql_specific_columns['sector_bearing_4'] = line[74:80]
        sql_specific_columns['sector_minimum_altitude_4'] = line[80:83]
        sql_specific_columns['sector_radius_4'] = line[83:87]
        sql_specific_columns['procedure_turn_indicator_4'] = line[87:88]
        sql_specific_columns['sector_bearing_5'] = line[88:94]
        sql_specific_columns['sector_minimum_altitude_5'] = line[94:97]
        sql_specific_columns['sector_radius_5'] = line[97:101]
        sql_specific_columns['procedure_turn_indicator_5'] = line[101:102]
        sql_specific_columns['sector_bearing_reference_waypoint_identifier'] = line[102:107]
        sql_specific_columns['sector_bearing_reference_waypoint_icao_code'] = line[107:109]
        sql_specific_columns['sector_bearing_reference_waypoint_section_code'] = line[109:110]
        sql_specific_columns['sector_bearing_reference_waypoint_subsection_code'] = line[110:111]
        # = line[111:116]
        sql_specific_columns['procedure_design_aircraft_category_or_type'] = line[116:117]
        sql_specific_columns['approach_route_qualifier_1'] = line[117:118]
        sql_specific_columns['approach_route_qualifier_2'] = line[118:119]
        sql_specific_columns['mag_true_indicator'] = line[119:120]
        # = line[120:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[30:31]
        if application_type == '?':         # TODO
            # = line[31:32]
            sql_specific_columns['sector_bearing_1'] = line[32:38]
            sql_specific_columns['sector_minimum_altitude_1'] = line[38:41]
            sql_specific_columns['sector_radius_1'] = line[41:45]
            sql_specific_columns['procedure_turn_indicator_1'] = line[45:46]
            sql_specific_columns['sector_bearing_2'] = line[46:52]
            sql_specific_columns['sector_minimum_altitude_2'] = line[52:55]
            sql_specific_columns['sector_radius_2'] = line[55:59]
            sql_specific_columns['procedure_turn_indicator_2'] = line[59:60]
            sql_specific_columns['sector_bearing_3'] = line[60:66]
            sql_specific_columns['sector_minimum_altitude_3'] = line[66:69]
            sql_specific_columns['sector_radius_3'] = line[69:73]
            sql_specific_columns['procedure_turn_indicator_3'] = line[73:74]
            sql_specific_columns['sector_bearing_4'] = line[74:80]
            sql_specific_columns['sector_minimum_altitude_4'] = line[80:83]
            sql_specific_columns['sector_radius_4'] = line[83:87]
            sql_specific_columns['procedure_turn_indicator_4'] = line[87:88]
            sql_specific_columns['notes'] = line[88:109]
            # = line[109:116]
            sql_specific_columns['procedure_design_aircraft_category_or_type'] = line[116:117]
            sql_specific_columns['approach_route_qualifier_1'] = line[117:118]
            sql_specific_columns['approach_route_qualifier_2'] = line[118:119]
            # = line[119:123]
            sql_specific_columns['file_record_number'] = line[123:128]
            sql_specific_columns['cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Company Routes (R) - Helicopter operation Route (H)
def import_arinc_424_rh(sql_cursor, line):
    sql_table = 'company_routes_helicopter_operations'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    sql_common_columns['subsection_code'] = line[5:6]
    sql_common_columns['from_airport_heliport_fix_identifier'] = line[6:11]
    sql_common_columns['from_helipad_identifier'] = line[11:16]
    # = line[16:17]
    sql_common_columns['from_heliport_fix_icao_code'] = line[17:19]
    sql_common_columns['from_heliport_fix_section_code'] = line[19:20]
    sql_common_columns['from_heliport_fix_subsection_code'] = line[20:21]
    sql_common_columns['to_airport_helipad_fix_identifier'] = line[21:26]
    sql_common_columns['to_helipad_identifier'] = line[26:31]
    # = line[31:32]
    sql_common_columns['to_airport_fix_icao_code'] = line[32:34]
    sql_common_columns['to_airport_fix_section_code'] = line[34:35]
    sql_common_columns['to_airport_fix_subsection_code'] = line[35:36]
    sql_common_columns['company_route_identifier'] = line[36:46]
    sql_common_columns['sequence_number'] = line[46:49]
    sql_common_columns['via_code'] = line[49:52]
    sql_common_columns['sid_star_app_awy_identifier'] = line[52:58]
    sql_common_columns['sid_star_app_awy_section_code'] = line[58:59]
    sql_common_columns['sid_star_app_awy_subsection_code'] = line[59:60]
    sql_common_columns['sid_star_app_awy_route_type'] = line[60:61]
    sql_common_columns['ssa_route_type_qualifier_1'] = line[61:62]
    sql_common_columns['ssa_route_type_qualifier_2'] = line[62:63]
    sql_common_columns['area_code'] = line[63:66]
    sql_common_columns['to_fix_identifier'] = line[66:72]
    sql_common_columns['to_fix_icao_code'] = line[72:74]
    sql_common_columns['to_fix_section_code'] = line[74:75]
    sql_common_columns['to_fix_subsection_code'] = line[75:76]
    sql_common_columns['runway_trans'] = line[76:81]
    sql_common_columns['enrt_trans'] = line[81:86]
    # = line[86:87]
    sql_common_columns['cruise_altitude'] = line[87:92]
    sql_common_columns['terminal_alternate_airport_identifier'] = line[92:96]
    sql_common_columns['terminal_alternate_airport_icao_code'] = line[96:98]
    sql_common_columns['terminal_alternate_airport_section_code'] = line[98:99]
    sql_common_columns['terminal_alternate_airport_subsection_code'] = line[99:100]
    sql_common_columns['alternate_distance'] = line[100:104]
    sql_common_columns['cost_index'] = line[104:107]
    sql_common_columns['enroute_alternate_airport'] = line[107:111]
    # = line[111:123]
    sql_common_columns['file_record_number'] = line[123:128]
    sql_common_columns['cycle_date'] = line[128:132]
    sql_primary(sql_cursor, sql_table, sql_common_columns, {})


# Heliport (H) - SBAS Path Point (P)
def import_arinc_424_hp(sql_cursor, line):
    sql_table = 'heliport_sbas_path_points'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['airport_identifier'] = line[6:10]
    sql_common_columns['airport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['approach_procedure_identifier'] = line[13:19]
    sql_common_columns['runway_or_helipad_identifier'] = line[19:24]
    sql_common_columns['operation_type'] = line[24:26]
    continuation_record_no = line[26:27]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['route_indicator'] = line[27:28]
        sql_specific_columns['sbas_service_provider_identifier'] = line[28:30]
        sql_specific_columns['reference_path_data_selector'] = line[30:32]
        sql_specific_columns['reference_path_identifier'] = line[32:36]
        sql_specific_columns['approach_performance_designator'] = line[36:37]
        sql_specific_columns['landing_threshold_point_latitude'] = line[37:48]
        sql_specific_columns['landing_threshold_point_longitude'] = line[48:60]
        sql_specific_columns['ltp_ellipsoid_height'] = line[60:66]
        sql_specific_columns['glide_path_angle'] = line[66:70]
        sql_specific_columns['flight_path_alignment_point_latitude'] = line[70:81]
        sql_specific_columns['flight_path_alignment_point_longitude'] = line[81:93]
        sql_specific_columns['course_width_at_threshold'] = line[93:98]
        sql_specific_columns['length_offset'] = line[98:102]
        sql_specific_columns['path_point_tch'] = line[102:108]
        sql_specific_columns['tch_units_indicator'] = line[108:109]
        sql_specific_columns['hal'] = line[109:112]
        sql_specific_columns['val'] = line[112:115]
        sql_specific_columns['sbas_fas_data_crc_remainder'] = line[115:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    elif ((continuation_record_no >= '2') and (continuation_record_no <= '9')) or ((continuation_record_no >= 'A') and (continuation_record_no <= 'Z')):
        application_type = line[27:28]
        if application_type == 'E':
            sql_specific_columns = {}
            sql_specific_columns['e_fpap_ellipsoid_height'] = line[28:34]
            sql_specific_columns['e_fpap_orthometric_height'] = line[34:40]
            sql_specific_columns['e_ltp_orthometric_height'] = line[40:46]
            sql_specific_columns['e_approach_type_identifier'] = line[46:56]
            sql_specific_columns['e_gbas_sbas_channel_number'] = line[56:61]
            # = line[61:71]
            sql_specific_columns['e_helicopter_procedure_course'] = line[71:74]
            # = line[74:123]
            sql_specific_columns['e_file_record_number'] = line[123:128]
            sql_specific_columns['e_cycle_date'] = line[128:132]
            sql_continuation(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
        else:
            print('Unknown application type ({0}{1}{2}{3}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no, application_type))
            print(line)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Heliport (H) - Helipad (H)
def import_arinc_424_hh(sql_cursor, line):
    sql_table = 'heliport_helipads'

    sql_common_columns = {}
    sql_common_columns['record_type'] = line[0:1]
    sql_common_columns['customer_area_code'] = line[1:4]
    sql_common_columns['section_code'] = line[4:5]
    # = line[5:6]
    sql_common_columns['heliport_identifier'] = line[6:10]
    sql_common_columns['heliport_icao_code'] = line[10:12]
    sql_common_columns['subsection_code'] = line[12:13]
    sql_common_columns['helipad_identifier'] = line[13:18]
    # = line[18:21]
    continuation_record_no = line[21:22]
    sql_specific_columns = {}
    if (continuation_record_no >= '0') and (continuation_record_no <= '1'):
        sql_specific_columns['helipad_shape'] = line[22:23]
        sql_specific_columns['helipad_tlof_dimension'] = line[23:31]
        # = line[31:32]
        sql_specific_columns['helipad_latitude'] = line[32:41]
        sql_specific_columns['helipad_longitude'] = line[41:51]
        sql_specific_columns['helipad_surface_code'] = line[51:52]
        sql_specific_columns['helipad_surface_type'] = line[52:56]
        sql_specific_columns['max_allowable_helicopter_weight'] = line[56:59]
        sql_specific_columns['helicopter_performance_requirement'] = line[59:60]
        sql_specific_columns['helipad_maximum_rotor_diameter'] = line[60:63]
        sql_specific_columns['helipad_type'] = line[63:64]
        # = line[64:65]
        sql_specific_columns['helipad_elevation'] = line[65:70]
        sql_specific_columns['helipad_fato_dimension'] = line[70:78]
        sql_specific_columns['safety_area_dimension'] = line[78:86]
        sql_specific_columns['helipad_orientation'] = line[86:91]
        sql_specific_columns['helipad_identifier_orientation'] = line[91:96]
        sql_specific_columns['preferred_approach_bearing_1'] = line[96:100]
        sql_specific_columns['preferred_approach_bearing_2'] = line[100:104]
        # = line[104:123]
        sql_specific_columns['file_record_number'] = line[123:128]
        sql_specific_columns['cycle_date'] = line[128:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    else:
        print('Unknown continuation record no ({0}{1}{2}) in following line:'.format(sql_common_columns['section_code'], sql_common_columns['subsection_code'], continuation_record_no))
        print(line)


# Header (HDR)
def import_arinc_424_hdr(sql_cursor, line):
    sql_table = 'header'

    sql_common_columns = {}
    sql_common_columns['header_identifier'] = line[0:3]
    sql_common_columns['header_number'] = line[3:5]
    sql_specific_columns = {}
    if sql_common_columns['header_number'] == '01':
        sql_specific_columns['file_name'] = line[5:20]
        sql_specific_columns['version_number'] = line[20:23]
        sql_specific_columns['production_test_flag'] = line[23:24]
        sql_specific_columns['record_length'] = line[24:28]
        sql_specific_columns['record_count'] = line[28:35]
        sql_specific_columns['cycle_date'] = line[35:39]
        # = line[39:41]
        sql_specific_columns['creation_date'] = line[41:52]
        sql_specific_columns['creation_time'] = line[52:60]
        # = line[60:61]
        sql_specific_columns['data_supplier_identifier'] = line[61:77]
        sql_specific_columns['target_customer_identifier'] = line[77:93]
        sql_specific_columns['database_part_number'] = line[93:113]
        # = line[113:124]
        sql_specific_columns['file_crc'] = line[124:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)
    else:
        sql_specific_columns['effective_date'] = line[5:16]
        sql_specific_columns['expiration_date'] = line[16:27]
        # = line[27:28]
        sql_specific_columns['supplier_text_field'] = line[28:58]
        sql_specific_columns['descriptive_text'] = line[58:88]
        # = line[88:132]
        sql_primary(sql_cursor, sql_table, sql_common_columns, sql_specific_columns)


# Import ARINC424
def import_arinc_424(sql_cursor, input_file):
    import os
    from tqdm import tqdm
    with tqdm(total=round(os.path.getsize(input_file) / 134), unit=' lines') as pbar:
        with open(input_file) as f:
            for line in f:
                line = line.rstrip('\r\n')
                pbar.update()
                if len(line) != 132:
                    print('Wrong length of following line:')
                    print(line)
                    continue

                # Header (HDR)
                if line.startswith('HDR'):
                    # Header
                    import_arinc_424_hdr(sql_cursor, line)
                    continue

                # Navigation Data
                section_code = line[4:5]
                if section_code == 'A':
                    # MORA (A)
                    subsection_code = line[5:6]
                    if subsection_code == 'S':
                        # Grid MORA (S)
                        import_arinc_424_as(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'D':
                    # Navaid (D)
                    subsection_code = line[5:6]
                    if subsection_code == ' ':
                        # VHF Navaid ( )
                        import_arinc_424_d_(sql_cursor, line)
                    elif subsection_code == 'B':
                        # NDB Navaid (B)
                        import_arinc_424_db(sql_cursor, line)
                    elif subsection_code == 'T':
                        # TACAN-Only NAVAID (T)
                        import_arinc_424_dt(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'E':
                    # Enroute (E)
                    subsection_code = line[5:6]
                    if subsection_code == 'A':
                        # Waypoints (A)
                        import_arinc_424_ea(sql_cursor, line)
                    elif subsection_code == 'M':
                        # Airway Markers (M)
                        import_arinc_424_em(sql_cursor, line)
                    elif subsection_code == 'P':
                        # Holding Patterns (P)
                        import_arinc_424_ep(sql_cursor, line)
                    elif subsection_code == 'R':
                        # Airways and Routes (R)
                        import_arinc_424_er(sql_cursor, line)
                    elif subsection_code == 'T':
                        # Preferred Routes (T)
                        import_arinc_424_et(sql_cursor, line)
                    elif subsection_code == 'U':
                        # Airway Restrictions (U)
                        import_arinc_424_eu(sql_cursor, line)
                    elif subsection_code == 'V':
                        # Communications (V)
                        import_arinc_424_ev(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'H':
                    # Heliport
                    subsection_code = line[12:13]
                    if subsection_code == 'A':
                        # Pads (A)
                        import_arinc_424_ha(sql_cursor, line)
                    elif subsection_code == 'C':
                        # Terminal Waypoints (C)
                        import_arinc_424_hc(sql_cursor, line)
                    elif subsection_code == 'D':
                        # SIDs (D)
                        import_arinc_424_hd(sql_cursor, line)
                    elif subsection_code == 'E':
                        # STARs (E)
                        import_arinc_424_he(sql_cursor, line)
                    elif subsection_code == 'F':
                        # Approach Procedures (F)
                        import_arinc_424_hf(sql_cursor, line)
                    elif subsection_code == 'H':
                        # Helipad (H)
                        import_arinc_424_hh(sql_cursor, line)
                    elif subsection_code == 'K':
                        # TAA (K)
                        import_arinc_424_hk(sql_cursor, line)
                    elif subsection_code == 'P':
                        # SBAS Path Point (P)
                        import_arinc_424_hp(sql_cursor, line)
                    elif subsection_code == 'S':
                        # MSA (S)
                        import_arinc_424_hs(sql_cursor, line)
                    elif subsection_code == 'V':
                        # Communications (V)
                        import_arinc_424_hv(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'P':
                    # Airport (P)
                    subsection_code = line[12:13]
                    if line[5:6] == 'N':
                        # Terminal NDB (N)
                        import_arinc_424_pn(sql_cursor, line)
                    elif subsection_code == 'A':
                        # Reference Points (A)
                        import_arinc_424_pa(sql_cursor, line)
                    elif subsection_code == 'B':
                        # Gates (B)
                        import_arinc_424_pb(sql_cursor, line)
                    elif subsection_code == 'C':
                        # Terminal Waypoints (C)
                        import_arinc_424_pc(sql_cursor, line)
                    elif subsection_code == 'D':
                        # SIDs (D)
                        import_arinc_424_pd(sql_cursor, line)
                    elif subsection_code == 'E':
                        # STARs (E)
                        import_arinc_424_pe(sql_cursor, line)
                    elif subsection_code == 'F':
                        # Approach Procedures (F)
                        import_arinc_424_pf(sql_cursor, line)
                    elif subsection_code == 'G':
                        # Runways (G)
                        import_arinc_424_pg(sql_cursor, line)
                    elif subsection_code == 'H':
                        # Helipad (H)
                        import_arinc_424_ph(sql_cursor, line)
                    elif subsection_code == 'I':
                        # Localizer/Glide Slope (I)
                        import_arinc_424_pi(sql_cursor, line)
                    elif subsection_code == 'K':
                        # TAA (K)
                        import_arinc_424_pk(sql_cursor, line)
                    elif subsection_code == 'L':
                        # MLS (L)
                        import_arinc_424_pl(sql_cursor, line)
                    elif subsection_code == 'M':
                        # Localizer Marker (M)
                        import_arinc_424_pm(sql_cursor, line)
                    elif subsection_code == 'P':
                        # Path Point (P)
                        import_arinc_424_pp(sql_cursor, line)
                    elif subsection_code == 'Q':
                        # GBAS Path Point (Q)
                        import_arinc_424_pq(sql_cursor, line)
                    elif subsection_code == 'R':
                        # Flt Planning ARR/DEP (R)
                        import_arinc_424_pr(sql_cursor, line)
                    elif subsection_code == 'S':
                        # MSA (S)
                        import_arinc_424_ps(sql_cursor, line)
                    elif subsection_code == 'T':
                        # GLS Station (T)
                        import_arinc_424_pt(sql_cursor, line)
                    elif subsection_code == 'V':
                        # Communications (V)
                        import_arinc_424_pv(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'R':
                    # Company Routes (R)
                    subsection_code = line[5:6]
                    if subsection_code == ' ':
                        # Company Routes ( )
                        import_arinc_424_r_(sql_cursor, line)
                    elif subsection_code == 'A':
                        # Alternate Records (A)
                        import_arinc_424_ra(sql_cursor, line)
                    elif subsection_code == 'H':
                        # Helicopter Operations Company Routes (H)
                        import_arinc_424_rh(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'T':
                    # Tables (T)
                    subsection_code = line[5:6]
                    if subsection_code == 'C':
                        # Cruising Tables (C)
                        import_arinc_424_tc(sql_cursor, line)
                    elif subsection_code == 'G':
                        # Geographical Reference (G)
                        import_arinc_424_tg(sql_cursor, line)
                    elif subsection_code == 'L':
                        # ATN Data (ATN NSAP) (L)
                        import_aring_424_tl(sql_cursor, line)
                    elif subsection_code == 'V':
                        # Communication Type Translation (V)
                        import_arinc_424_tv(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                elif section_code == 'U':
                    # Airspace (U)
                    subsection_code = line[5:6]
                    if subsection_code == 'C':
                        # Controlled Airspace (C)
                        import_arinc_424_uc(sql_cursor, line)
                    elif subsection_code == 'F':
                        # FIR/UIR (F)
                        import_arinc_424_uf(sql_cursor, line)
                    elif subsection_code == 'R':
                        # Restrictive Airspace (R)
                        import_arinc_424_ur(sql_cursor, line)
                    else:
                        # warning
                        print('Unknown subsection code ({0}{1}) in following line:'.format(section_code, subsection_code))
                        print(line)
                else:
                    # warning
                    print('Unknown section code ({0}) in following line:'.format(section_code))
                    print(line)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='ARINC424 input file path', default='FAACIFP18')
    parser.add_argument('-o', '--output', help='Navigation database file', default='navdata.sqlite3')
    args = parser.parse_args()

    # open database connection
    sql_connection = sqlite3.connect(args.output)
    sql_cursor = sql_connection.cursor()

    # import data
    import_arinc_424(sql_cursor, args.input)

    # close database connection
    sql_connection.commit()
    sql_connection.close()


if __name__ == '__main__':
    # execute only if run as a script
    main()
